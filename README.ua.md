# NeutrinoParticles GPU

�� �������������� ��� ��� `npm` �������, ���'������ � �������� [**NeutrinoParticles**](https://neutrinoparticles.com/) ������ � ������ GPU ������������.

## ��� ������� ���

```bash
npm install
npm build
```

## �������

* `gpu.js` �� `gpu.wasm` - ����������� ��������� NeutrinoParticles ������ � ������ GPU ������������, �� �������������� WebGL ��� ��������� �� ������� ������. ���� ����������� ������ JavaScript, ������ - Web Assembly. �������� ���� �������� ���������� ��� ���������� � ���� �����. �������� � ����� WebGL. ��������� ������������ � ������������� �������� ��� ������� WebGL `gpu.reference`, ��� ���� �� ���� ���������� � �����.
* `gpu.reference` - ������������� �������� ������ ��� ������� WebGL ����������. ����������� `gpu.js` �� `gpu.wasm`. ��������� ������ ������� ������������ ���������� �������, ��������� ����� �������� �� ���� ������� ��� ������������ API.
* `gpu.reference.samples` - ������� ������ ������������ `gpu.reference`.
* [gpu.pixi7](./packages/gpu.pixi7/README.ua.md) - ���������� NeutrinoParticles GPU � [PIXI.js](https://pixijs.com/) ���� 7.
* [gpu.pixi7.samples](./packages/gpu.pixi7.samples/README.ua.md) - �������� ������������ `gpu.pixi7`. �������� ����������� ������� ������������ NeutrinoParticles � PIXI.

## �������� ����������
��� ChatGPT o1-mini:
> �������� ������� ���������� �����, ����� � ���������� ����� README.ua.md �� README.md. ��������� ����� � ������ ��������� .md ����, ��� ����� ���� ���������