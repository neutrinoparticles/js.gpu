# NeutrinoParticles GPU

This is a monorepository for all `npm` packages related to rendering [**NeutrinoParticles**](https://neutrinoparticles.com/) effects with full GPU acceleration.

## To build everything

```bash
npm install
npm build
```

## Packages

* `gpu.js` and `gpu.wasm` - Low-level renderers for NeutrinoParticles effects with full GPU acceleration, using WebGL for simulating and rendering effects. One uses pure JavaScript, the other - Web Assembly. Created to be basic libraries for integration into other engines. Work with raw WebGL. An example of usage is the high-level renderer for pure WebGL `gpu.reference`, or any other engine integrations.
* `gpu.reference` - High-level effects renderer for pure WebGL environments. Uses `gpu.js` and `gpu.wasm`. Additionally performs functions of loading necessary textures, setting up basic libraries, and provides an easy-to-use API.
* `gpu.reference.samples` - Several samples of using `gpu.reference`.
* [gpu.pixi7](./packages/gpu.pixi7/README.md) - Integration of NeutrinoParticles GPU into [PIXI.js](https://pixijs.com/) version 7.
* [gpu.pixi7.samples](./packages/gpu.pixi7.samples/README.md) - Examples of using `gpu.pixi7`. Highlights the most important aspects of using NeutrinoParticles in PIXI.
