// TypeScript bindings for emscripten-generated code.  Automatically generated at compile time.
declare namespace RuntimeExports {
    /**
     * @param {string|null=} returnType
     * @param {Array=} argTypes
     * @param {Arguments|Array=} args
     * @param {Object=} opts
     */
    function ccall(ident: any, returnType?: (string | null) | undefined, argTypes?: any[] | undefined, args?: (Arguments | any[]) | undefined, opts?: any | undefined): any;
    /**
     * @param {string=} returnType
     * @param {Array=} argTypes
     * @param {Object=} opts
     */
    function cwrap(ident: any, returnType?: string | undefined, argTypes?: any[] | undefined, opts?: any | undefined): any;
    namespace GL {
        let counter: number;
        let buffers: any[];
        let programs: any[];
        let framebuffers: any[];
        let renderbuffers: any[];
        let textures: any[];
        let shaders: any[];
        let vaos: any[];
        let contexts: any[];
        let offscreenCanvases: {};
        let queries: any[];
        let samplers: any[];
        let transformFeedbacks: any[];
        let syncs: any[];
        let stringCache: {};
        let stringiCache: {};
        let unpackAlignment: number;
        let unpackRowLength: number;
        function recordError(errorCode: any): void;
        function getNewId(table: any): number;
        function genObject(n: any, buffers: any, createFunction: any, objectTable: any): void;
        function getSource(shader: any, count: any, string: any, length: any): string;
        function createContext(canvas: HTMLCanvasElement, webGLContextAttributes: any): any;
        function registerContext(ctx: any, webGLContextAttributes: any): any;
        function makeContextCurrent(contextHandle: any): boolean;
        function getContext(contextHandle: any): any;
        function deleteContext(contextHandle: any): void;
        function initExtensions(context: any): void;
    }
    let HEAPF32: any;
    let HEAPF64: any;
    let HEAP_DATA_VIEW: any;
    let HEAP8: any;
    let HEAPU8: any;
    let HEAP16: any;
    let HEAPU16: any;
    let HEAP32: any;
    let HEAPU32: any;
    let HEAP64: any;
    let HEAPU64: any;
}
interface WasmModule {
  _main(_0: number, _1: number): number;
}

type EmbindString = ArrayBuffer|Uint8Array|Uint8ClampedArray|Int8Array|string;
export interface ClassHandle {
  isAliasOf(other: ClassHandle): boolean;
  delete(): void;
  deleteLater(): this;
  isDeleted(): boolean;
  clone(): this;
}
export type Vec2 = [ number, number ];

export type Vec3 = [ number, number, number ];

export type Quat = [ number, number, number, number ];

export type Mat4 = [ number, number, number, number, number, number, number, number, number, number, number, number, number, number, number, number ];

export interface StringList extends ClassHandle {
  push_back(_0: EmbindString): void;
  resize(_0: number, _1: EmbindString): void;
  size(): number;
  get(_0: number): any;
  set(_0: number, _1: EmbindString): boolean;
}

export interface BlendFuncValue<T extends number> {
  value: T;
}
export type BlendFunc = BlendFuncValue<0>|BlendFuncValue<1>|BlendFuncValue<2>|BlendFuncValue<3>|BlendFuncValue<4>;

export interface CullFaceValue<T extends number> {
  value: T;
}
export type CullFace = CullFaceValue<0>|CullFaceValue<1>;

export interface DepthFuncValue<T extends number> {
  value: T;
}
export type DepthFunc = DepthFuncValue<0>|DepthFuncValue<1>;

export interface TextureTypeValue<T extends number> {
  value: T;
}
export type TextureType = TextureTypeValue<0>|TextureTypeValue<1>;

export interface RenderStateChanger extends ClassHandle {
  enableDepthTest(value: boolean): void;
  enableBlend(value: boolean): void;
  enableProgramPointSize(value: boolean): void;
  blendFunc(value: BlendFunc): void;
  cullFace(value: CullFace): void;
  depthFunc(value: DepthFunc): void;
  activeTexture(index: number): void;
  notifyBeforeTextureBound(type: TextureType, id: number): void;
  notifyBeforeRender(): void;
}

export interface RenderStateChangerWrapper extends RenderStateChanger {
  notifyOnDestruction(): void;
}

export interface Context extends ClassHandle {
  defaultColorMultiplier: number;
  releaseGL(): void;
  beginUpdate(): void;
  endUpdate(): void;
  beginPreRender(cameraPos: Vec3, cameraDir: Vec3, cameraUp: Vec3, cameraRight: Vec3, vpMatrix: Mat4, windowWidth: number, windowHeight: number): void;
  endPreRender(): void;
  beginRender(): void;
  endRender(): void;
}

export interface EffectSource extends ClassHandle {
  readonly textures: StringList;
}

export interface EffectModel extends ClassHandle {
  releaseGL(): void;
}

export interface EffectInst extends ClassHandle {
  releaseGL(): void;
  initialize(position: Vec3, rotation: Quat, velocity: Vec3): void;
  update(dt: number, position: Vec3, rotation: Quat): void;
  preRender(modelMatrix: Mat4): void;
  render(): void;
  isEmpty(): boolean;
  setProp1(propName: EmbindString, value: number): void;
  setProp2(propName: EmbindString, value: Vec2): void;
  setProp3(propName: EmbindString, value: Vec3): void;
  setPropQ(propName: EmbindString, value: Quat): void;
}

export interface Bloom extends ClassHandle {
  processBloom(): void;
  renderResult(): void;
  resize(windowWidth: number, windowHeight: number): void;
}

export interface BloomNonFloat extends Bloom {
  resize(windowWidth: number, windowHeight: number): void;
  setupForRenderScene(): void;
}

export interface BloomFloat extends Bloom {
  resize(windowWidth: number, windowHeight: number): void;
  setupForRenderScene(): void;
}

export interface BloomFloatExternal extends Bloom {
}

export interface ViewportSettings extends ClassHandle {
}

interface EmbindModule {
  NANVEC3: Vec3;
  NANQUAT: Quat;
  StringList: {
    new(): StringList;
  };
  BlendFunc: {ONE__ONE: BlendFuncValue<0>, SRC_ALPHA__ONE_MINUS_SRC_ALPHA: BlendFuncValue<1>, ONE__ONE_MINUS_SRC_ALPHA: BlendFuncValue<2>, SRC_ALPHA__ONE: BlendFuncValue<3>, ZERO__SRC_COLOR: BlendFuncValue<4>};
  CullFace: {FRONT_AND_BACK: CullFaceValue<0>, BACK: CullFaceValue<1>};
  DepthFunc: {LESS: DepthFuncValue<0>, GREATER: DepthFuncValue<1>};
  TextureType: {TEXTURE_2D: TextureTypeValue<0>, TEXTURE_3D: TextureTypeValue<1>};
  RenderStateChanger: {
    implement(_0: any): RenderStateChangerWrapper | null;
    extend(_0: EmbindString, _1: any): any;
  };
  RenderStateChangerWrapper: {};
  Context: {
    create(texPremultipliedAlpha: boolean): Context | null;
    createWithStateChanger(stateChanger: RenderStateChanger | null, texPremultipliedAlpha: boolean): Context | null;
  };
  EffectSource: {
    create(shaderSource: EmbindString): EffectSource | null;
  };
  EffectModel: {
    create(context: Context | null, effectSource: EffectSource | null, textureMapping: any, numActualTextures: any, textureReindexing: any): EffectModel | null;
  };
  EffectInst: {
    create(effectModel: EffectModel | null): EffectInst | null;
  };
  Bloom: {};
  BloomNonFloat: {
    create(maxColor: number): BloomNonFloat | null;
  };
  BloomFloat: {
    create(maxColor: number): BloomFloat | null;
  };
  BloomFloatExternal: {
    create(textureId: number, flipY: boolean, maxColor: number): BloomFloatExternal | null;
  };
  ViewportSettings: {
    create(): ViewportSettings | null;
  };
  saveViewportSettings(viewportSettings: ViewportSettings): void;
  restoreViewportSettings(viewportSettings: ViewportSettings): void;
  errorMessage(): string;
}

export type MainModule = WasmModule & typeof RuntimeExports & EmbindModule;
export default function MainModuleFactory (options?: unknown): Promise<MainModule>;
