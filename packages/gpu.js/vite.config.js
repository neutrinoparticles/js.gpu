import { defineConfig } from 'vite';

export default defineConfig({
  build: {
    lib: {
      entry: './neutrinoparticles.renderer.gpu.js',
      name: 'NPGPU',
      formats: ['umd'],
      fileName: 'neutrinoparticles.renderer.gpu',
    },
    rollupOptions: {
      output: {
        globals: {
        },
      },
    },
  },
});
