/**
 * Texture loader interface used by {@link EffectModel} to load necessary textures.
 */
export abstract class AbstractTexturesLoader
{
    constructor() {
    }

    /**
	 * Requests loading of a texture.
	 * 
	 * @param {string} texturePath Path to texture.
	 * @param {function(texture)} callback Should be called when texture is loaded.
	 */
    abstract load(texturePath: string): Promise<WebGLTexture>;
}

export class RequestTexturesLoader extends AbstractTexturesLoader
{
	public gl: WebGL2RenderingContext;
	public basePath: string;

	constructor(gl: WebGL2RenderingContext, basePath: string) {
		super();
		this.gl = gl;
		this.basePath = basePath;
	}
	
	load(texturePath: string): Promise<WebGLTexture> {
		return new Promise<WebGLTexture>((resolve) => {
			let image = new Image();
			image.onload = () => {
				const gl = this.gl;
				const texture = gl.createTexture();
				gl.bindTexture(gl.TEXTURE_2D, texture);
				//gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
				gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
				gl.generateMipmap(gl.TEXTURE_2D);
				
				resolve(texture!);
			};
			
			image.src = this.basePath + texturePath;
		});
	}

};