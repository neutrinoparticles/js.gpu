import * as NPGPU from "@neutrinoparticles/gpu.types";

export abstract class Camera
{
    protected _cameraPos: NPGPU.Vec3 = [0, 0, 0];
    public get cameraPos() { return this._cameraPos; }

    protected _cameraDir: NPGPU.Vec3 = [0, 0, 0];
    public get cameraDir() { return this._cameraDir; }

    protected _cameraUp: NPGPU.Vec3 = [0, 0, 0];
    public get cameraUp() { return this._cameraUp; }

    protected _cameraRight: NPGPU.Vec3 = [0, 0, 0];
    public get cameraRight() { return this._cameraRight; }

    protected _viewProjMatrix: NPGPU.Mat4 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    public get viewProjMatrix() { return this._viewProjMatrix; }

    constructor() {
    }

    public abstract resize(width: number, height: number) : void;
}