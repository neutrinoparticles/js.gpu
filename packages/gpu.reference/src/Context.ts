import * as NPGPU from "@neutrinoparticles/gpu.types";
import { Camera } from "./Camera";
import { AbstractTexturesLoader } from "./TexturesLoader";

export interface ContextOptions {
    npgpuLoader: (options?: unknown) => Promise<NPGPU.MainModule>,
    texturesLoader: AbstractTexturesLoader;
    camera: Camera;
}

export class Context
{
    public npgpu?: NPGPU.MainModule;
    public readonly gl: WebGL2RenderingContext;
    public loaded: boolean = false;
    public readonly loadedPromise: Promise<void>;

    public readonly texturesLoader: AbstractTexturesLoader;

    private _camera: Camera;
    get camera() { return this._camera; }
    set camera(value: Camera) {
        this._camera = value;
    }

    private static _instance?: Context;

    private _viewportSettings: Array<NPGPU.ViewportSettings> = [];
    private _viewportIndex = 0;

    private gpu33Context_?: NPGPU.Context | null = null;
    get gpu33Context(): NPGPU.Context {
        return this.gpu33Context_!;
    }

    public constructor(gl: WebGL2RenderingContext, options: ContextOptions) {
        this.gl = gl;

        this.texturesLoader = options.texturesLoader;
        
        this._camera = options.camera;

        this.loadedPromise = new Promise<void>(async (resolve) => {

            const npgpu = await options.npgpuLoader();
            this.npgpu = npgpu;

            const emGLCtx = npgpu.GL.registerContext(this.gl, {});      
            this.npgpu.GL.makeContextCurrent(emGLCtx);

            this.gpu33Context_ = npgpu.Context.create(false);

            if (!this.gpu33Context_) {
                throw new Error(npgpu.errorMessage());
            }

            this.loaded = true;
            resolve();
        });
    }

    public static initialize(gl: WebGL2RenderingContext, options: ContextOptions) : Context {
        if (Context._instance)
            throw new Error("Context is already initialized");

        Context._instance = new Context(gl, options);
        return Context._instance!;
    }

    public static isInitialized() : boolean {
        return !!Context._instance;
    }

    public static instance() : Context {
        return Context._instance!;
    }

    public static shutdown() : void {
        Context._instance!._shutdown();
        Context._instance = undefined;
    }

    public pushViewportSettings() {
        const npgpu = this.npgpu!;

        if (this._viewportSettings.length <= this._viewportIndex) {
            this._viewportSettings.push(npgpu.ViewportSettings.create()!);
        }

        npgpu.saveViewportSettings(this._viewportSettings[this._viewportIndex]);
        this._viewportIndex++;
    }

    public popViewportSettings() {
        if (this._viewportIndex < 1)
            return;

        const npgpu = this.npgpu!;

        this._viewportIndex--;
        npgpu.restoreViewportSettings(this._viewportSettings[this._viewportIndex]);
    }

    public beginPreRender(width: number, height: number) {
        this.pushViewportSettings();

        this.gpu33Context.beginPreRender(this.camera.cameraPos, this.camera.cameraDir,
            this.camera.cameraUp, this.camera.cameraRight, this.camera.viewProjMatrix,
            width, height);
    }

    public endPreRender() {
        this.gpu33Context.endPreRender();

        this.popViewportSettings();
    }

    private _shutdown() {
        this.gpu33Context_?.delete();
    }
}