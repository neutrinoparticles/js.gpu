import * as glMatrix from 'gl-matrix';
import { Context } from './Context';
import { EffectModel } from './EffectModel';
import * as NPGPU from "@neutrinoparticles/gpu.types";
import { EventEmitter } from 'events';

const _identityMatrix: NPGPU.Mat4 = glMatrix.mat4.create() as NPGPU.Mat4;
glMatrix.mat4.identity(_identityMatrix);

export interface EffectOptions {
	position?: NPGPU.Vec3
};

export class Effect extends EventEmitter {
	public readonly context: Context;
	public readonly effectModel: EffectModel;

	public position: NPGPU.Vec3;

	private _npgpuEffect: NPGPU.EffectInst | null = null;
	get gpu33Effect() {
		return this._npgpuEffect;
	}

	get ready() {
		return this.gpu33Effect != null;
	}

	private lastUpdatePosition: NPGPU.Vec3 = [0, 0, 0];

	constructor(effectModel: EffectModel, options?: EffectOptions) {
		super();

		if (options && options!.position) {
			this.position = [...options!.position];
		} else {
			this.position = [0, 0, 0];
		}

		this.context = effectModel.context;
		this.effectModel = effectModel;

		if (effectModel.ready) {
			this._onEffectReady();
		} else {
			effectModel.once('ready', () => {
				this._onEffectReady();
			});
		}
	}

	destroy() {
		if (this._npgpuEffect) {
			this._npgpuEffect!.delete();
		}
	}

	update(seconds: number) {
		if (!this.ready)
			return;

		const npgpu = this.context.npgpu!;

		let position: NPGPU.Vec3;

		if (this.lastUpdatePosition[0] != this.position[0] ||
			this.lastUpdatePosition[1] != this.position[1] ||
			this.lastUpdatePosition[2] != this.position[2]) 
		{
			Object.assign(this.lastUpdatePosition, this.position);
			position = this.lastUpdatePosition;
		}
		else {
			position = npgpu.NANVEC3;
		}

        this._npgpuEffect!.update(seconds, position, npgpu.NANQUAT);
	}

	preRender()
	{
		if (!this._npgpuEffect)
			return;

		this._npgpuEffect!.preRender(_identityMatrix);
	}

	render()
    {
        if (!this.ready)
			return;
		
        this.effectModel.setupTextures();
        this._npgpuEffect!.render();      
    }

	private _onEffectReady() {
		const npgpu = this.context.npgpu!;

		this.context.pushViewportSettings();

		Object.assign(this.lastUpdatePosition, this.position);

		this._npgpuEffect = npgpu.EffectInst.create(this.effectModel.npgpuEffectModel!);
		this._npgpuEffect!.initialize(this.lastUpdatePosition, npgpu.NANQUAT, npgpu.NANVEC3);

		this.context.popViewportSettings();

		this.emit('ready', this);
	}
}
