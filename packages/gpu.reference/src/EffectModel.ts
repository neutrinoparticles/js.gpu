import { Context } from "./Context";
import { AbstractTexturesLoader } from "./TexturesLoader";
import { EventEmitter } from 'events';
import * as NPGPU from "@neutrinoparticles/gpu.types";

export class EffectModel extends EventEmitter
{
    public readonly gl: WebGL2RenderingContext;
    public readonly context: Context;
    public readonly textures: Array<WebGLTexture>;

    public npgpuEffectSource: NPGPU.EffectSource;
    public npgpuEffectModel: NPGPU.EffectModel | null = null;

    private _numTexturesToLoadLeft: number = 0;

    constructor(context: Context, effectSource: string, texturesLoader?: AbstractTexturesLoader) {
		super();
 
        this.gl = context.gl;
        this.context = context;

		if (!texturesLoader)
			texturesLoader = this.context.texturesLoader;

		const npgpu = this.context.npgpu!;

        this.npgpuEffectSource = npgpu.EffectSource.create(effectSource)!;
        
        this._numTexturesToLoadLeft = this.npgpuEffectSource.textures.size();
        this.textures = new Array<WebGLTexture>();
        this._loadTextures(this._numTexturesToLoadLeft, texturesLoader);
    }

    destroy(): void {
        this.npgpuEffectModel?.delete();
        this.npgpuEffectSource?.delete();
    }

    get ready(): boolean {
		return this._numTexturesToLoadLeft === 0;
	}

    public setupTextures(): void {
        const gl = this.gl;

        this.textures.forEach((texture, index) => {
            gl.activeTexture(gl.TEXTURE0 + index);
			gl.bindTexture(gl.TEXTURE_2D, texture);
        });
    }

    private _loadTextures(numTextures: number, texturesLoader: AbstractTexturesLoader): void
    {
		for (let texIndex = 0; texIndex < numTextures; ++texIndex) 
		{
			let texturePath = this.npgpuEffectSource.textures.get(texIndex);
			
			texturesLoader.load(texturePath).then(
				((self, imageIndex: number) => {
					return function(texture) 
					{
						self._onTextureLoaded(imageIndex, texture);
					}
				}) (this, texIndex));
		}

    }

    private _onTextureLoaded(index: number, texture: WebGLTexture) : void
	{
		this.textures[index] = texture;

		if (--this._numTexturesToLoadLeft === 0) {
			this._onAllTexturesLoaded();
		}
	}

	private _onAllTexturesLoaded() : void
	{
		const npgpu = this.context.npgpu!;

		this.npgpuEffectModel = npgpu.EffectModel.create(this.context.gpu33Context
            , this.npgpuEffectSource, null, this.textures.length, null);
			
        if (!this.npgpuEffectModel) {
            throw new Error(npgpu.errorMessage());
		}

		this.emit('ready', this);
	}
}