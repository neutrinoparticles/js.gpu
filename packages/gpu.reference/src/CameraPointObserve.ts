import * as NPGPU from "@neutrinoparticles/gpu.types";
import * as glMatrix from 'gl-matrix';
import { Camera } from './Camera';

const cameraXMatrix: NPGPU.Mat4 = glMatrix.mat4.create() as NPGPU.Mat4;
const cameraMatrix: NPGPU.Mat4 = glMatrix.mat4.create() as NPGPU.Mat4;
const projMatrix: NPGPU.Mat4 = glMatrix.mat4.create() as NPGPU.Mat4;
const viewMatrix: NPGPU.Mat4 = glMatrix.mat4.create() as NPGPU.Mat4;
const up: NPGPU.Vec3 = [0, 1, 0];

export interface CameraPointObserveOptions
{
    lookAtPoint?: NPGPU.Vec3,
    nearPlaneFromDistPercentage?: number,
    farPlaneFromDistPercentage?: number,
}

export class CameraPointObserve extends Camera
{
    private distance: number;
    private viewAngleDegree: number;
    private lookAtPoint: NPGPU.Vec3; 
    private nearPlaneFromDistPercentage: number;
    private farPlaneFromDistPercentage: number;
    private viewAspect: number;

    private angleXRad: number = 0;
    private angleYRad: number = 0;

    constructor(distance: number, viewAngleDegree: number, width: number, height: number, options?: CameraPointObserveOptions) {
        super();

        this.distance = distance;
        this.viewAngleDegree = viewAngleDegree;
        
        if (options && options.lookAtPoint) {
            this.lookAtPoint = [...options.lookAtPoint!];
        } else {
            this.lookAtPoint = [0, 0, 0];
        }

        this.nearPlaneFromDistPercentage = (options && options.nearPlaneFromDistPercentage) || 0.01;
        this.farPlaneFromDistPercentage = (options && options.farPlaneFromDistPercentage) || 500;

        this.viewAspect = width / height;
        this._updateMatrices();
    }

    public resize(width: number, height: number) : void
    {
        this.viewAspect = width / height;
        this._updateMatrices();
    }

    public rotate(angleXDegree: number, angleYDegree: number) {
        const angleXRad =  Math.PI / 180.0 * angleXDegree;
        this.angleXRad += angleXRad;
        this.angleXRad = this.angleXRad % (Math.PI / 180.0 * 360);

        const angleYRad =  Math.PI / 180.0 * angleYDegree;
        this.angleYRad += angleYRad;

        const maxYAngleRad = Math.PI / 180.0 * 85;
        if (this.angleYRad > maxYAngleRad)
            this.angleYRad = maxYAngleRad;
        else if (this.angleYRad < -maxYAngleRad)
            this.angleYRad = -maxYAngleRad;

        this._updateMatrices();
    }

    public rotateY(angleDegree: number) {
        const angleRad =  Math.PI / 180.0 * angleDegree;
        this.angleXRad += angleRad;
    }

    private _updateMatrices() {
        glMatrix.mat4.fromYRotation(cameraXMatrix, this.angleXRad);
        glMatrix.mat4.fromXRotation(cameraMatrix, this.angleYRad);
        glMatrix.mat4.multiply(cameraMatrix, cameraXMatrix, cameraMatrix);
        glMatrix.mat4.translate(cameraMatrix, cameraMatrix, this.lookAtPoint);
        glMatrix.vec3.transformMat4(this._cameraPos, [0, 0, this.distance], cameraMatrix);

        glMatrix.vec3.subtract(this._cameraDir, this.lookAtPoint, this._cameraPos);
        glMatrix.vec3.normalize(this._cameraDir, this._cameraDir);

        glMatrix.vec3.cross(this._cameraRight, this._cameraDir, up);
        glMatrix.vec3.normalize(this._cameraRight,this._cameraRight);

        glMatrix.vec3.cross(this._cameraUp, this._cameraRight, this._cameraDir);
        glMatrix.vec3.normalize(this._cameraUp, this._cameraUp);

        glMatrix.mat4.lookAt(viewMatrix, this._cameraPos, this.lookAtPoint,
            this._cameraUp);

        const viewAngleRad = Math.PI / 180.0 * this.viewAngleDegree;

        glMatrix.mat4.perspective(projMatrix, viewAngleRad, this.viewAspect, 
            this.distance * this.nearPlaneFromDistPercentage, 
            this.distance * this.farPlaneFromDistPercentage);
        glMatrix.mat4.multiply(this._viewProjMatrix, projMatrix, viewMatrix);
    }
}