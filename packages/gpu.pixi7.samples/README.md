# Examples for NeutrinoParticles GPU Renderer in PIXI.js 7

Here are examples of using [**neutrinoparticles.gpu.pixi7**](../gpu.pixi7/README.md)

To run the examples:

```bash
cd ../..
npm install 
npm run build 
cd packages/gpu.pixi7.samples 
npm run dev
```

Then, open the `Demo` link for each sample in your browser.

## Basic Usage
![basic_usage](./img/basic_usage.gif)

* Demo: [http://localhost:5173/basic_usage.html](http://localhost:5173/basic_usage.html)
* Source: [basic_usage.ts](./src/basic_usage.ts)

A minimal sample for rendering and updating an effect. Simply a sprite in the background and an effect positioned at the center of the screen.

## Rotation
![rotation](./img/rotation.gif)

* Demo: [http://localhost:5173/rotation.html](http://localhost:5173/rotation.html)
* Source: [rotation.ts](./src/rotation.ts)

Setting the rotation of the effect. Note that the effect is part of the scene, and if the parent object of the effect is rotated, the effect itself will also rotate according to the hierarchy.

For the rotation to affect the effect, it needs to account for it somehow. In the editor, in the Emitter Guide, there are checkboxes that enable rotation consideration:
![Rotation checkboxes](./img/rotation_editor_checkboxes.png)

## World Display Object
![World Display Object](./img/world_display_object.gif)

* Demo: [http://localhost:5173/world_display_object.html](http://localhost:5173/world_display_object.html)
* Source: [world_display_object.ts](./src/world_display_object.ts)

> The effect generates particles in world space. This means that if you move the effect, the particles will stay in their places, and new particles will be generated considering the new position of the effect.
>
> However, the effect can specify an object that defines its world coordinate system. Any transformation of this object will instantly transform all particles of the effect.

This sample shows how to organize a scene with an effect and a background image that maintains proportions and scales to fit entirely within the new window frame when the browser window is resized.

## Render to Texture

![Render to texture](./img/render_to_texture.gif)

* Demo: [http://localhost:5173/render_to_texture.html](http://localhost:5173/render_to_texture.html)
* Source: [render_to_texture.ts](./src/render_to_texture.ts)

Rendering an effect with a sprite in the background to a texture. This texture is then used to render four identical sprites on the main scene.

A `RenderTexture` is created, then the main scene, which has four sprites with this texture. An additional scene is also created, which has a rabbit sprite in the background and an effect. During the update stage of the main scene, the additional scene is updated and rendered to the `RenderTexture`.

## Texture Atlas

![Texture atlas](./img/texture_atlas.gif)

* Demo: [http://localhost:5173/texture_atlas.html](http://localhost:5173/texture_atlas.html)
* Source: [texture_atlas.ts](./src/texture_atlas.ts)

> Textures from loaded texture atlases are used automatically when loading the effect. It is important that the texture name on the atlas matches the one used by the effect.
>
> It is important that the atlas is already loaded before loading the effect.

This sample loads a texture atlas, then loads an effect that uses the same two textures that were used to create the atlas. As a result, the effect uses the already loaded textures from the atlas.

## Bloom

![Bloom](./img/bloom.gif)

* Demo: [http://localhost:5173/bloom.html](http://localhost:5173/bloom.html)
* Source: [bloom.ts](./src/bloom.ts)

> The bloom effect adds a glow to pixels and parts of the scene that have a color value greater than 1 (in the normal range from 0 to 1).
>
> For this effect, the PIXI.js scene must be rendered to a `FLOAT` texture, and the particles in the effect must have colors greater than 1. After that, the texture with the Bloom effect is rendered to the screen. The `neutrinoparticles.gpu.pixi7` library provides everything needed to do this.

In this sample, a `FLOAT` `RenderTexture` is created, and the scene is rendered to it. Then, a `Bloom` object is used to render this texture with the bloom effect to the screen.

## UMD + Pure HTML5

* Demo: [http://localhost:5173/umd.html](http://localhost:5173/umd.html)
* Source: [umd.html](./umd.html)

Using effect rendering in a pure browser environment (without ES6 or TypeScript). Essentially, this is the [basic_usage](#basic-usage) sample, but written in pure JavaScript directly inside an HTML page.
