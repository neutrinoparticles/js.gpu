# Приклади для NeutrinoParticles GPU рендереру в PIXI.js 7

Тут описані приклади використання [**neutrinoparticles.gpu.pixi7**](../gpu.pixi7/README.ua.md)

Для запуску прикладів:

```bash
cd ../..
npm install
npm run build
cd packages/gpu.pixi7.samples
npm run dev
```

Далі в браузері відкриваємо посилання `Demo` для кожного з семплів.

## Basic Usage
![basic_usage](./img/basic_usage.gif)

* Demo: [http://localhost:5173/basic_usage.html](http://localhost:5173/basic_usage.html)
* Source: [basic_usage.ts](./src/basic_usage.ts)

Мінімальний семпл для рендеру та апдейту ефекта. Просто спрайт на фоні та ефект з позицією в центрі екрану.

## Rotation
![rotation](./img/rotation.gif)

* Demo: [http://localhost:5173/rotation.html](http://localhost:5173/rotation.html)
* Source: [rotation.ts](./src/rotation.ts)

Задавання повертання ефекту. Зверніть увагу, що ефект - це частина сцени, і якщо парент об'єкт ефекту повернений, сам ефект теж стає повернений відповідно до ієрархії.

Для того, щоб повертання мало вплив на ефект, він має якось це враховувати. В редакторі, в Emitter Guide є чекбокси, які включають враховування повороту:
![Rotation checkboes](./img/rotation_editor_checkboxes.png)


## World Display Object
![World Display Object](./img/world_display_object.gif)

* Demo: [http://localhost:5173/world_display_object.html](http://localhost:5173/world_display_object.html)
* Source: [world_display_object.ts](./src/world_display_object.ts)

> Ефект генерує партікли в світовий простір. Це значить, що якщо ви здвинете ефект, партікли залишаться на своїх місцях, а нові сгенеровані партікли вже будуть генеруватися враховуючи нову позицію ефекту.
>
> Але для ефекту є можливість вказати об'єкт, який визначає світову систему координат для нього. Будь яка трансформація цього об'єкту буде миттєво трансформувати всі партікли ефекту.

Цей семпл показує, як організувати сцену з ефектом та малюнком на тлі, яка при зміні розміру вікна браузера буде зберігати пропорції та масштабуватися, щоб повністю вміститися в нову рамку.

## Render to texture

![Render to texture](./img/render_to_texture.gif)

* Demo: [http://localhost:5173/render_to_texture.html](http://localhost:5173/render_to_texture.html)
* Source: [render_to_texture.ts](./src/render_to_texture.ts)

Рендеринг ефекту зі спрайтом на тлі в тексту. Ця текстура потім використовується для рендеру чотирьох однакових спрайтів на головній сцені.

Створюється `RenderTexture`, потім головна сцена, яка має чотири спрайти з цією текстурою. Створюється також додаткова сцена, яка має спрайт кролика на тлі та ефект. На етапі апдейту основної сцени, додаткова сцена апдейтиться та рендериться в `RenderTexture`.

## Texture atlas

![Texture atlas](./img/texture_atlas.gif)

* Demo: [http://localhost:5173/texture_atlas.html](http://localhost:5173/texture_atlas.html)
* Source: [texture_atlas.ts](./src/texture_atlas.ts)

> Текстури з завантажені текстурних атласві використовуються автоматично при завантаженні ефекту. Важливо, щоб назва текстури на атласі співпадала з тією, що використовує ефект.

> Важливо, щоб атлас вже був завантажений до завантаження ефекту.

Цей семпл завантажує текстурний атлас, потім завантажує ефект, в якому використовуються ті самі дві текстури, які використовувалися для створення атласу. Як результат, ефект використовує вже завантажені текстури з атласу.

## Bloom

![Bloom](./img/bloom.gif)

* Demo: [http://localhost:5173/bloom.html](http://localhost:5173/bloom.html)
* Source: [bloom.ts](./src/bloom.ts)

> Ефект блуму додає сяйво для пікселів та частин сцени, які мають колір більше 1 (в звичайному діапазоні від 0 до 1).
>
> Для цього ефекту сцена в PIXI.js має рендеритися у `FLOAT` текстуру, а партікли в ефекті мають бути з кольорами більше 1. Після цього текстура з Bloom ефектом рендериться на екран. Бібліотека `neutrinoparticles.gpu.pixi7` дає все необхідне, щоб це зробити.

У цьому семплі створюється `FLOAT` текстура `RenderTexture`, на яку рендериться сцена. Потім використовується `Bloom` об'єкт, який рендерить цю текстуру з блум ефектом на екран.

## UMD + Чистий HTML5

* Demo: [http://localhost:5173/umd.html](http://localhost:5173/umd.html)
* Source: [umd.html](./umd.html)

Використання рендеру ефекті у чистому браузерному середовищі (без ES6 або TypeScript). Фактично, це [basic_usage](#basic-usage) семпл, але на чистому JavaScript написаному прямо всередині HTML сторінки.

## Переклад англійською
Для ChatGPT o1-mini:
> Перклади простою англійською мовою, тільки в посиланнях заміни README.ua.md на README.md. Результат видай у вигляді похідного .md коду, щоб можна було скопіювати