//import { default as npgpuLoader } from '@neutrinoparticles/gpu.js';
import { default as npgpuLoader } from '@neutrinoparticles/gpu.wasm';

import { Effect, EffectsUpdateGroup, registerPlugins } from '@neutrinoparticles/gpu.pixi7';
import * as PIXI from 'pixi.js';

registerPlugins();

const app = new PIXI.Application({ background: '#1099bb', resizeTo: window,
    neutrinoGPU: { // Additional options for NeutrinoParticles Context
        npgpuLoader, // Mandatory. NeutrinoParticles GPU library loader
		texturesBasePath: 'textures/', // Base path for textures. Will be prefixed to exported texture paths in effects
	}
 });

// Wait for GPU library loaded
await app.neutrinoGPU!.loadedPromise;

document.body.appendChild(app.view as HTMLCanvasElement);

PIXI.Assets.addBundle('resources', [
    // Request loading effect in the bundle. 'data' is neccessary to mark it as an NP effect
	{ name: 'testEffect', srcs: 'src/shaders/TextureAtlas.shader', data: app.neutrinoGPU!.loadData},
]);

const updateGroup = new EffectsUpdateGroup(app.neutrinoGPU!);

// Load texture atlas
PIXI.Assets.load('textures/atlas.json').then(() => {
    // Load resource bundle
    PIXI.Assets.loadBundle('resources').then((resources) => {
        {
            // Create instance of the effect
            const effect = new Effect(resources.testEffect);

            // Add the effect to the scene graph
            app.stage.addChild(effect);

            // Start the effect. Should be called after it is added to the scene graph
            // to calculate world position correctly.
            effect.start({
                position: [app.screen.width / 2, app.screen.height / 2, 0]
            })

            // Add it to the update group to update all effects at once
            updateGroup.add(effect);
        }

        // Listen for animate update
        app.ticker.add((_delta) =>
        {
            updateGroup.update(app.ticker.elapsedMS / 1000.0);
        });
    });
});




