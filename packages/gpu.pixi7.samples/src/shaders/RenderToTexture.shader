//DESC 1;-1;0.0;10.0;1;0;0;0;13;15;2;1;1;1;4;1;20;0;-1;-1;-1;6;0;5;Position;3;flags;1;Lifetime;1;Velocity;3;Angle;1;0;0;
//TEXTURES default.png

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;
precision highp sampler3D;
#define _6A0EA0315208361E 13u
#define _2CE42649D807BFC4 1u

const uint a = 0u;
const uint b = 1u;
const uint c = 2u;
const uint d = 3u;

uniform sampler3D _5CABCD2CE500BEBB;

struct e
{
	uint f; 
	uint g; 
	uint h; 
	uint i; 
};

#if defined(_9CBA6082CFE05FA3) || defined(_B970C3D2AD57F959) || defined(_8CFBC1D9BA8D6516)

uint j = 0u;
uint k = 0u;
uint l = 0u;

void m(uint n)
{
	n &= 0xFFFFFFFFu;

	j = n < 2u ? n + 2u : n;
	k = n < 8u ? n + 8u : n;
	l = n < 16u ? n + 16u : n;
}

uint o()
{
	uint p = (((j << 13u) ^ j) & 0xFFFFFFFFu) >> 19u;
	j = ((j & 0xFFFFFFFEu) << 12u) ^ p;

	p = (((k << 2u) ^ k) & 0xFFFFFFFFu) >> 25u;
	k = ((k & 0xFFFFFFF8u) << 4u) ^ p;

	p = (((l << 3u) ^ l) & 0xFFFFFFFFu) >> 11u;
	l = ((l & 0xFFFFFFF0u) << 17u) ^ p;

	return (j ^ k) ^ l;
}

float q()
{
	return float(o()) / 4294967296.0;
}

#define _9D02A6055A27F4CF 3.1415926

uint r(uint s, uint p)
{
    return s - p * (s / p);
}

float t(float degrees)
{
	return degrees / 180.0 * _9D02A6055A27F4CF;
}

uniform usampler2D _E6B127475CD1AF15;
uniform usampler2D _1AAFF6BB1AC02A82;

uint u = 0u; 

float v(uint w)
{
	return uintBitsToFloat(texelFetch(_E6B127475CD1AF15,
		ivec2(w % _6A0EA0315208361E, w / _6A0EA0315208361E), 0).r);
}

uint x(uint w)
{
	return texelFetch(_E6B127475CD1AF15,
		ivec2(w % _6A0EA0315208361E, w / _6A0EA0315208361E), 0).r;
}

float y(uint w)
{
	return uintBitsToFloat(texelFetch(_1AAFF6BB1AC02A82,
		ivec2(w % _6A0EA0315208361E, w / _6A0EA0315208361E), 0).r);
}

uint z(uint w)
{
	return texelFetch(_1AAFF6BB1AC02A82,
		ivec2(w % _6A0EA0315208361E, w / _6A0EA0315208361E), 0).r;
}

vec3 A(uint w)
{
	return vec3(v(w), v(w + 1u), v(w + 2u));
}

vec3 B(uint w)
{
	return vec3(y(w), y(w + 1u), y(w + 2u));
}

uvec2 C(uint D)
{
	u = 13u + D * 9u;
	return uvec2(0u, 3u); 
}

e E(uint F)
{
	return e(0u, 4u, 3u, r(F - 0u, 20u)); 
}

#endif

#ifdef _9CBA6082CFE05FA3

uniform sampler2D _17DCC16C9305C31F;
uniform float _0D6ECD3D4CD5F8C7;
uniform float _B29D96F4CC68440F;
uniform uint _600D80823B393136;
uniform uint _A62B4010E834E4BC;
uniform uint _8B5E47AC853BBD76;

float G;

void H()
{
	gl_Position = vec4(-2.0, -2.0, 0, 1);
}

uint I(uvec2 J)
{
	return uint(texelFetch(_17DCC16C9305C31F, ivec2(J), 0).r * 1023.0);
}

uint I(uint K)
{
	return I(uvec2(K % 2u, K / 2u));
}

uvec2 L;

#if (_53986AB86EF3B727 == 0) 

#define _83AB904282DA95AA 0u

void M()
{
	uint K;
	uint F = uint(gl_VertexID);

	K = 0u + (F) / 20u * 1u + (F) % 20u % 1u;

	L = uvec2(K % 2u, K / 2u);
	gl_Position = vec4((float(L.x) + 0.5) / float(2u) * 2.0 - 1.0, (float(L.y) + 0.5) / float(1u) * 2.0 - 1.0,  0, 1);
}

uint N()
{
	return c;
}

vec3 O;
uint P;
float Q;
vec3 R;
float S;

flat out vec3 _C09D6F3C5A9B9FBA;
flat out uint _98DBFB22A3024CC1;
flat out float _AF547B67E173F200;
flat out vec3 _BA00525347033428;
flat out float _D95F711B69033EF4;

uint D()
{
	return uint(gl_VertexID);
}

void T(uint D)
{
	m((0xFFFFFFFFu / 20u * D) ^ _600D80823B393136 ^ P);
}

bool U(e V, float W, out float _93AC3C19AEE8CE0B)
{
	uint X = V.f;
	uint Y = V.f + V.g;

	uint Z = I(L);
	uint _ = 20u + ((uint(gl_VertexID) % 1u) < 0u ? 1u : 0u); 
	float ab = float(1u) * (float(_) - float(Z));
	float bb = W / ab;
	float cb = q();

	if (cb < (bb * (50.0 * 0.2))) { _93AC3C19AEE8CE0B = q(); return true; } 
	return false;
}

void db(uint eb)
{
	P = (P & 0xFFFFFFFCu) | eb;
}

e fb()
{
	return E(uint(gl_VertexID));
}

void gb(e V, float _93AC3C19AEE8CE0B)
{
	uint X = V.f;
	uint Y = V.f + V.g;

	Q = 0.0;
	vec3  hb = vec3(0.0, 0.0, 0.0);
	O = hb;
	vec3 ib = mix(
		B(X + 0u), 
		A(X + 0u),
		_93AC3C19AEE8CE0B);
	O += ib;
	float jb = 0.0 + q() * (1.0 - 0.0);
	vec2[2] kb = vec2[2](vec2(-95.0, -203.0),vec2(105.0, -203.0));
	vec2[2] lb = vec2[2](vec2(1.0, 0.0),vec2(1.0, 0.0));
	float mb = clamp(jb, 0.0, 1.0);
	float nb = 0.0+(mb - 0.0) * 1.0;
	vec2 ob = mix(kb[int(nb)], kb[int(nb) + 1], fract(nb));
	vec2 pb = mix(lb[int(nb)], lb[int(nb) + 1], fract(nb));
	vec3 qb = vec3(ob, 0.0);
	R = qb;
	S = 0.0;
}

void rb()
{
	G = _B29D96F4CC68440F;

	O = B(u + 0u);
	Q = y(u + 4u);
	R = B(u + 5u);
	S = y(u + 8u);
}

void sb(e V)
{
	uint X = V.f;
	uint Y = V.f + V.g;

	_AF547B67E173F200 = Q + G; 
	_C09D6F3C5A9B9FBA = O;
	_BA00525347033428 = R;
	vec3 tb = _C09D6F3C5A9B9FBA + _BA00525347033428 * G;
	_C09D6F3C5A9B9FBA = tb;
	_D95F711B69033EF4 = S;
	float hb = 2.0;
	if (_AF547B67E173F200 >= hb)
		db(a);
}

void ub()
{
	_C09D6F3C5A9B9FBA = O; 
	_98DBFB22A3024CC1 = P; 
	_AF547B67E173F200 = Q; 
	_BA00525347033428 = R; 
	_D95F711B69033EF4 = S; 
}

#endif

void main() {
	uint D = _83AB904282DA95AA + uint(gl_VertexID);
	uint h = C(D).y;

	P = z(u + h);
	uint eb = P & 3u;

	M();

	T(D);

	uint vb = 0u;
	uint wb;

	e xb = fb();

	switch (eb)
	{
	case a:
	{
		uint yb = x(xb.f + xb.h);
		uint zb = (yb & 3u);
		uint Ab = ((P >> 2u) & 3u); 

		float Bb = (Ab != 0u ? 
			_B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - float((P >> 4u) + 1u) / float(0xFFFFFFFu)) : 
			_B29D96F4CC68440F
		);

float _93AC3C19AEE8CE0B = 0.f;

		if (zb == N() && U(xb, Bb, _93AC3C19AEE8CE0B))
		{
			G = Bb * (1.0 - _93AC3C19AEE8CE0B);
			gb(xb, _93AC3C19AEE8CE0B);

#ifdef _73599E697EB8FFF2
			db(b);
			wb = uint(_93AC3C19AEE8CE0B * float(0xFFFFFFFu)) << 4u;
			ub();
#else
			db(c);	
			sb(xb);
			wb = o();
#endif
		}
		else
		{
			H();

			if (zb == b)
			{
				wb = yb; 
				vb = 1u; 
			}
			else
			{
				wb = o();
			}
		}
				
		break;
	}

#ifdef _73599E697EB8FFF2
	case b:
	{
		rb();
		float _93AC3C19AEE8CE0B = float(P >> 4u) / float(0xFFFFFFFu);
		G = _B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - _93AC3C19AEE8CE0B);
		sb(xb);

		if ((P & 3u) == a)
		{
			H();
		}
		else
		{
			db(c);
			vb = min(((P >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		wb = P;

		break;
	}
#endif

	case c:
	{
		rb();
		G = _B29D96F4CC68440F;
		sb(xb);

		if ((P & 3u) == a)
		{
			H();
		}
		else
		{
			vb = min(((P >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		wb = o();
					
		break;
	}

#ifdef _73599E697EB8FFF2
	case d:
	{
		rb();
		Cb();

		if ((P & 3u) == a)
		{
			H();
		}
		else
		{
			vb = min(((P >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		wb = o();
		break;
	}
#endif
	}

	P = (wb & 0xFFFFFFF0u) | (vb << 2u) | (P & 3u);

	_98DBFB22A3024CC1 = P;
	gl_PointSize = 1.0;
}

#endif 
#ifdef _CC5A472CC9FE17BA

out vec4 _4CCF54A3B6372A66;

void main()
{
	_4CCF54A3B6372A66 = vec4(1.01 / 1024.0, 0, 0, 0);
}

#endif

#ifdef _B970C3D2AD57F959

layout (std140) uniform _F52D3B1AA302F088
{
    vec3 Db;
    float Eb;
    vec3 Fb;
    uint Gb;
    vec3 Hb;
    uint Ib;
    vec3 Jb;
    float Kb;
    mat4 Lb;
};

uniform float _618E8A543B8FC66E;

uniform mat4 _4F2DACEFFBDAB8D1;

vec4 Mb(vec4 Nb)
{
	return _618E8A543B8FC66E < 0.0 ? Nb : vec4(Nb.rgb * _618E8A543B8FC66E, Nb.a);
}

smooth out vec4 _203C8330B731DD85;
flat out uint _94B3AA5995C3B5A1;

vec4 Ob = vec4(0, 0, 1, 1);

uvec2 Pb()
{
	return uvec2(uint(gl_VertexID) / 6u, uint(gl_VertexID) % 6u);
}

smooth out vec2 _BEAD44E72AA5013D;

float[6] Qb = float[6](-1.0,   -1.0,   -1.0,   0.0,   0.0,    0.0);
float[6] Rb = float[6](0.0,    0.0,    -1.0,    0.0,   -1.0,   -1.0);

float[6] Sb = float[6](0.0, 0.0,    0.0,    1.0,    1.0,    1.0);
float[6] Tb = float[6](1.0, 1.0,    0.0,    1.0,    0.0,    0.0);

vec2[6] Ub = vec2[6](
	vec2(0.0, 1.0), vec2(0.0, 1.0), vec2(0.0, 0.0), vec2(1.0, 1.0), vec2(1.0, 0.0), vec2(1.0, 0.0)
	);

void Vb(uint Wb, vec3 Xb, vec2 Yb, vec3 Zb, vec3 _b, 
    vec4 Nb, vec4 ac)
{
	_203C8330B731DD85 = Mb(Nb);

	vec2 bc = vec2(1, 1) - Yb;

	vec2 cc = vec2(Qb[Wb], Rb[Wb]) * Yb
		+ vec2(Sb[Wb], Tb[Wb]) * bc;

	vec3 dc = cc.x * Zb + cc.y * _b;

    mat4 ec = Lb * _4F2DACEFFBDAB8D1;

	gl_Position = ec * vec4(Xb + dc, 1.0);
	
	_BEAD44E72AA5013D = Ub[Wb] * ac.zw + ac.xy;
}

void fc(uint Wb, vec3 Xb, vec2 Yb, float gc, vec2 hc, vec4 Nb, vec4 ac)
{
    vec3 ic = Jb;
    vec3 jc = Fb;

    float s = t(gc);
    float kc = -sin(s);
    float lc = cos(s);

    vec3 Zb = vec3(
        ic.x * lc + jc.x * kc,
        ic.y * lc + jc.y * kc,
        ic.z * lc + jc.z * kc);

    vec3 _b = vec3(
        -ic.x * kc + jc.x * lc,
        -ic.y * kc + jc.y * lc,
        -ic.z * kc + jc.z * lc);

    Vb(Wb, Xb, Yb, Zb * hc.x, _b * hc.y, Nb, ac);
}

void mc()
{
	gl_Position = vec4(-2, -2, 0, 1);
}

void nc(uint oc, uint Wb)
{
	e V = E(oc - 0u);
	uint X = V.f;
	uint Y = V.f + V.g;

	vec3 Xb = A(u + 0u);
	float S = v(u + 8u);
	float hb = 30.0;
	_94B3AA5995C3B5A1 = 0u;
	fc(Wb, A(u + 0u), vec2(0.5, 0.5), S, vec2(hb, hb), vec4(vec3(1.0, 1.0, 1.0), 1.0), Ob);
}

void main() {
	uvec2 pc = Pb();
	uint D = pc.x;
	uint Wb = pc.y;

	uvec2 qc = C(D);
	uint rc = qc.x;
	uint h = qc.y;
	uint sc = x(u + h);
	uint eb = sc & 3u;

	if (eb != c)
	{
		mc();
		return;
	}
	switch (rc)
	{
	case 0u: nc(D, Wb); break;
	}
}

#endif 

#ifdef _DC66F57E96C6C75C

uniform float _618E8A543B8FC66E;

flat in uint _94B3AA5995C3B5A1;
smooth in vec4 _203C8330B731DD85;

out vec4 _EB400D689750048A;

smooth in vec2 _BEAD44E72AA5013D;

vec4 tc(sampler2D uc, mat3x2 vc)
{
	return texture(uc, vc * vec3(_BEAD44E72AA5013D, 1))* _203C8330B731DD85;
}

void makeFragColor(sampler2D uc, mat3x2 vc)
{
	vec4 wc = tc(uc, vc);

#ifdef _6335F91D31E2CC83
	wc.rgb = wc.rgb * _203C8330B731DD85.a;
#endif

	_EB400D689750048A = _618E8A543B8FC66E < 0.0 ?
		vec4(mix(vec3(1, 1, 1), wc.rgb, wc.a), 1) :
		wc;
}

in makeFragColor()

#endif

