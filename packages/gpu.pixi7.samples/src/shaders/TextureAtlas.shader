//DESC 1;-1;0.0;10.0;1;0;0;0;19;21;2;1;2;1;4;1;20;0;-1;-1;-1;6;0;5;Position;3;flags;1;Lifetime;1;Velocity;3;Angle;1;0;11;4;1;20;0;-1;-1;-1;6;0;5;Position;3;flags;1;Lifetime;1;Velocity;3;Angle;1;0;0;
//TEXTURES default.png;default_inv.png

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;
precision highp sampler3D;
#define _6A0EA0315208361E 19u
#define _2CE42649D807BFC4 2u

const uint a = 0u;
const uint b = 1u;
const uint c = 2u;
const uint d = 3u;

uniform sampler3D _5CABCD2CE500BEBB;

struct e
{
	uint f; 
	uint g; 
	uint h; 
	uint i; 
};

#if defined(_9CBA6082CFE05FA3) || defined(_B970C3D2AD57F959) || defined(_8CFBC1D9BA8D6516)

uint j = 0u;
uint k = 0u;
uint l = 0u;

void m(uint n)
{
	n &= 0xFFFFFFFFu;

	j = n < 2u ? n + 2u : n;
	k = n < 8u ? n + 8u : n;
	l = n < 16u ? n + 16u : n;
}

uint o()
{
	uint p = (((j << 13u) ^ j) & 0xFFFFFFFFu) >> 19u;
	j = ((j & 0xFFFFFFFEu) << 12u) ^ p;

	p = (((k << 2u) ^ k) & 0xFFFFFFFFu) >> 25u;
	k = ((k & 0xFFFFFFF8u) << 4u) ^ p;

	p = (((l << 3u) ^ l) & 0xFFFFFFFFu) >> 11u;
	l = ((l & 0xFFFFFFF0u) << 17u) ^ p;

	return (j ^ k) ^ l;
}

float q()
{
	return float(o()) / 4294967296.0;
}

#define _9D02A6055A27F4CF 3.1415926

uint r(uint s, uint p)
{
    return s - p * (s / p);
}

float t(float degrees)
{
	return degrees / 180.0 * _9D02A6055A27F4CF;
}

uniform usampler2D _E6B127475CD1AF15;
uniform usampler2D _1AAFF6BB1AC02A82;

uint u = 0u; 

float v(uint w)
{
	return uintBitsToFloat(texelFetch(_E6B127475CD1AF15,
		ivec2(w % _6A0EA0315208361E, w / _6A0EA0315208361E), 0).r);
}

uint x(uint w)
{
	return texelFetch(_E6B127475CD1AF15,
		ivec2(w % _6A0EA0315208361E, w / _6A0EA0315208361E), 0).r;
}

float y(uint w)
{
	return uintBitsToFloat(texelFetch(_1AAFF6BB1AC02A82,
		ivec2(w % _6A0EA0315208361E, w / _6A0EA0315208361E), 0).r);
}

uint z(uint w)
{
	return texelFetch(_1AAFF6BB1AC02A82,
		ivec2(w % _6A0EA0315208361E, w / _6A0EA0315208361E), 0).r;
}

vec3 A(uint w)
{
	return vec3(v(w), v(w + 1u), v(w + 2u));
}

vec3 B(uint w)
{
	return vec3(y(w), y(w + 1u), y(w + 2u));
}

uvec2 C(uint D)
{
	if (D < 20u) { 
		u = 19u + (D - 0u) * 9u; 
		return uvec2(0u, 3u); 
	} 
	else { 
		u = 209u + (D - 20u) * 9u; 
		return uvec2(1u, 3u); 
	} 
}

e E(uint F)
{
	return e(0u, 4u, 3u, r(F - 0u, 20u)); 
}

e G(uint F)
{
	return e(0u, 4u, 3u, r(F - 0u, 20u)); 
}

#endif

#ifdef _9CBA6082CFE05FA3

uniform sampler2D _17DCC16C9305C31F;
uniform float _0D6ECD3D4CD5F8C7;
uniform float _B29D96F4CC68440F;
uniform uint _600D80823B393136;
uniform uint _A62B4010E834E4BC;
uniform uint _8B5E47AC853BBD76;

float H;

void I()
{
	gl_Position = vec4(-2.0, -2.0, 0, 1);
}

uint J(uvec2 K)
{
	return uint(texelFetch(_17DCC16C9305C31F, ivec2(K), 0).r * 1023.0);
}

uint J(uint L)
{
	return J(uvec2(L % 2u, L / 2u));
}

uvec2 M;

#if (_53986AB86EF3B727 == 0) 

#define _83AB904282DA95AA 0u

void N()
{
	uint L;
	uint F = uint(gl_VertexID);

	L = 0u + (F) / 20u * 1u + (F) % 20u % 1u;

	M = uvec2(L % 2u, L / 2u);
	gl_Position = vec4((float(M.x) + 0.5) / float(2u) * 2.0 - 1.0, (float(M.y) + 0.5) / float(1u) * 2.0 - 1.0,  0, 1);
}

uint O()
{
	return c;
}

vec3 P;
uint Q;
float R;
vec3 S;
float T;

flat out vec3 _C09D6F3C5A9B9FBA;
flat out uint _98DBFB22A3024CC1;
flat out float _AF547B67E173F200;
flat out vec3 _BA00525347033428;
flat out float _D95F711B69033EF4;

uint D()
{
	return uint(gl_VertexID);
}

void U(uint D)
{
	m((0xFFFFFFFFu / 40u * D) ^ _600D80823B393136 ^ Q);
}

bool V(e W, float X, out float _93AC3C19AEE8CE0B)
{
	uint Y = W.f;
	uint Z = W.f + W.g;

	uint _ = J(M);
	uint ab = 20u + ((uint(gl_VertexID) % 1u) < 0u ? 1u : 0u); 
	float bb = float(1u) * (float(ab) - float(_));
	float cb = X / bb;
	float db = q();

	if (db < (cb * (50.0 * 0.2))) { _93AC3C19AEE8CE0B = q(); return true; } 
	return false;
}

void eb(uint fb)
{
	Q = (Q & 0xFFFFFFFCu) | fb;
}

e gb()
{
	return E(uint(gl_VertexID));
}

void hb(e W, float _93AC3C19AEE8CE0B)
{
	uint Y = W.f;
	uint Z = W.f + W.g;

	R = 0.0;
	vec3  ib = vec3(0.0, 0.0, 0.0);
	P = ib;
	vec3 jb = mix(
		B(Y + 0u), 
		A(Y + 0u),
		_93AC3C19AEE8CE0B);
	P += jb;
	float kb = 0.0 + q() * (1.0 - 0.0);
	vec2[2] lb = vec2[2](vec2(-103.0, -140.0),vec2(97.0, -140.0));
	vec2[2] mb = vec2[2](vec2(1.0, 0.0),vec2(1.0, 0.0));
	float nb = clamp(kb, 0.0, 1.0);
	float ob = 0.0+(nb - 0.0) * 1.0;
	vec2 pb = mix(lb[int(ob)], lb[int(ob) + 1], fract(ob));
	vec2 qb = mix(mb[int(ob)], mb[int(ob) + 1], fract(ob));
	vec3 rb = vec3(pb, 0.0);
	S = rb;
	T = 0.0;
}

void sb()
{
	H = _B29D96F4CC68440F;

	P = B(u + 0u);
	R = y(u + 4u);
	S = B(u + 5u);
	T = y(u + 8u);
}

void tb(e W)
{
	uint Y = W.f;
	uint Z = W.f + W.g;

	_AF547B67E173F200 = R + H; 
	_C09D6F3C5A9B9FBA = P;
	_BA00525347033428 = S;
	vec3 ub = _C09D6F3C5A9B9FBA + _BA00525347033428 * H;
	_C09D6F3C5A9B9FBA = ub;
	_D95F711B69033EF4 = T;
	float ib = 2.0;
	if (_AF547B67E173F200 >= ib)
		eb(a);
}

void vb()
{
	_C09D6F3C5A9B9FBA = P; 
	_98DBFB22A3024CC1 = Q; 
	_AF547B67E173F200 = R; 
	_BA00525347033428 = S; 
	_D95F711B69033EF4 = T; 
}

#elif (_53986AB86EF3B727 == 1) 

#define _83AB904282DA95AA 20u

void N()
{
	uint L;
	uint F = uint(gl_VertexID);

	L = 1u + (F) / 20u * 1u + (F) % 20u % 1u;

	M = uvec2(L % 2u, L / 2u);
	gl_Position = vec4((float(M.x) + 0.5) / float(2u) * 2.0 - 1.0, (float(M.y) + 0.5) / float(1u) * 2.0 - 1.0,  0, 1);
}

uint O()
{
	return c;
}

vec3 P;
uint Q;
float R;
vec3 S;
float T;

flat out vec3 _C09D6F3C5A9B9FBA;
flat out uint _98DBFB22A3024CC1;
flat out float _AF547B67E173F200;
flat out vec3 _BA00525347033428;
flat out float _D95F711B69033EF4;

uint D()
{
	return uint(gl_VertexID) + 20u;
}

void U(uint D)
{
	m((0xFFFFFFFFu / 40u * D) ^ _600D80823B393136 ^ Q);
}

bool V(e W, float X, out float _93AC3C19AEE8CE0B)
{
	uint Y = W.f;
	uint Z = W.f + W.g;

	uint _ = J(M);
	uint ab = 20u + ((uint(gl_VertexID) % 1u) < 0u ? 1u : 0u); 
	float bb = float(1u) * (float(ab) - float(_));
	float cb = X / bb;
	float db = q();

	if (db < (cb * (50.0 * 0.2))) { _93AC3C19AEE8CE0B = q(); return true; } 
	return false;
}

void eb(uint fb)
{
	Q = (Q & 0xFFFFFFFCu) | fb;
}

e gb()
{
	return G(uint(gl_VertexID));
}

void hb(e W, float _93AC3C19AEE8CE0B)
{
	uint Y = W.f;
	uint Z = W.f + W.g;

	R = 0.0;
	vec3  ib = vec3(0.0, 0.0, 0.0);
	P = ib;
	vec3 jb = mix(
		B(Y + 0u), 
		A(Y + 0u),
		_93AC3C19AEE8CE0B);
	P += jb;
	float kb = 0.0 + q() * (1.0 - 0.0);
	vec2[2] lb = vec2[2](vec2(-101.0, 121.0),vec2(99.0, 121.0));
	vec2[2] mb = vec2[2](vec2(1.0, 0.0),vec2(1.0, 0.0));
	float nb = clamp(kb, 0.0, 1.0);
	float ob = 0.0+(nb - 0.0) * 1.0;
	vec2 pb = mix(lb[int(ob)], lb[int(ob) + 1], fract(ob));
	vec2 qb = mix(mb[int(ob)], mb[int(ob) + 1], fract(ob));
	vec3 rb = vec3(pb, 0.0);
	S = rb;
	T = 0.0;
}

void sb()
{
	H = _B29D96F4CC68440F;

	P = B(u + 0u);
	R = y(u + 4u);
	S = B(u + 5u);
	T = y(u + 8u);
}

void tb(e W)
{
	uint Y = W.f;
	uint Z = W.f + W.g;

	_AF547B67E173F200 = R + H; 
	_C09D6F3C5A9B9FBA = P;
	_BA00525347033428 = S;
	vec3 ub = _C09D6F3C5A9B9FBA + _BA00525347033428 * H;
	_C09D6F3C5A9B9FBA = ub;
	_D95F711B69033EF4 = T;
	float ib = 2.0;
	if (_AF547B67E173F200 >= ib)
		eb(a);
}

void vb()
{
	_C09D6F3C5A9B9FBA = P; 
	_98DBFB22A3024CC1 = Q; 
	_AF547B67E173F200 = R; 
	_BA00525347033428 = S; 
	_D95F711B69033EF4 = T; 
}

#endif

void main() {
	uint D = _83AB904282DA95AA + uint(gl_VertexID);
	uint h = C(D).y;

	Q = z(u + h);
	uint fb = Q & 3u;

	N();

	U(D);

	uint wb = 0u;
	uint xb;

	e yb = gb();

	switch (fb)
	{
	case a:
	{
		uint zb = x(yb.f + yb.h);
		uint Ab = (zb & 3u);
		uint Bb = ((Q >> 2u) & 3u); 

		float Cb = (Bb != 0u ? 
			_B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - float((Q >> 4u) + 1u) / float(0xFFFFFFFu)) : 
			_B29D96F4CC68440F
		);

float _93AC3C19AEE8CE0B = 0.f;

		if (Ab == O() && V(yb, Cb, _93AC3C19AEE8CE0B))
		{
			H = Cb * (1.0 - _93AC3C19AEE8CE0B);
			hb(yb, _93AC3C19AEE8CE0B);

#ifdef _73599E697EB8FFF2
			eb(b);
			xb = uint(_93AC3C19AEE8CE0B * float(0xFFFFFFFu)) << 4u;
			vb();
#else
			eb(c);	
			tb(yb);
			xb = o();
#endif
		}
		else
		{
			I();

			if (Ab == b)
			{
				xb = zb; 
				wb = 1u; 
			}
			else
			{
				xb = o();
			}
		}
				
		break;
	}

#ifdef _73599E697EB8FFF2
	case b:
	{
		sb();
		float _93AC3C19AEE8CE0B = float(Q >> 4u) / float(0xFFFFFFFu);
		H = _B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - _93AC3C19AEE8CE0B);
		tb(yb);

		if ((Q & 3u) == a)
		{
			I();
		}
		else
		{
			eb(c);
			wb = min(((Q >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		xb = Q;

		break;
	}
#endif

	case c:
	{
		sb();
		H = _B29D96F4CC68440F;
		tb(yb);

		if ((Q & 3u) == a)
		{
			I();
		}
		else
		{
			wb = min(((Q >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		xb = o();
					
		break;
	}

#ifdef _73599E697EB8FFF2
	case d:
	{
		sb();
		Db();

		if ((Q & 3u) == a)
		{
			I();
		}
		else
		{
			wb = min(((Q >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		xb = o();
		break;
	}
#endif
	}

	Q = (xb & 0xFFFFFFF0u) | (wb << 2u) | (Q & 3u);

	_98DBFB22A3024CC1 = Q;
	gl_PointSize = 1.0;
}

#endif 
#ifdef _CC5A472CC9FE17BA

out vec4 _4CCF54A3B6372A66;

void main()
{
	_4CCF54A3B6372A66 = vec4(1.01 / 1024.0, 0, 0, 0);
}

#endif

#ifdef _B970C3D2AD57F959

layout (std140) uniform _F52D3B1AA302F088
{
    vec3 Eb;
    float Fb;
    vec3 Gb;
    uint Hb;
    vec3 Ib;
    uint Jb;
    vec3 Kb;
    float Lb;
    mat4 Mb;
};

uniform float _618E8A543B8FC66E;

uniform mat4 _4F2DACEFFBDAB8D1;

vec4 Nb(vec4 Ob)
{
	return _618E8A543B8FC66E < 0.0 ? Ob : vec4(Ob.rgb * _618E8A543B8FC66E, Ob.a);
}

smooth out vec4 _203C8330B731DD85;
flat out uint _94B3AA5995C3B5A1;

vec4 Pb = vec4(0, 0, 1, 1);

uvec2 Qb()
{
	return uvec2(uint(gl_VertexID) / 6u, uint(gl_VertexID) % 6u);
}

smooth out vec2 _BEAD44E72AA5013D;

float[6] Rb = float[6](-1.0,   -1.0,   -1.0,   0.0,   0.0,    0.0);
float[6] Sb = float[6](0.0,    0.0,    -1.0,    0.0,   -1.0,   -1.0);

float[6] Tb = float[6](0.0, 0.0,    0.0,    1.0,    1.0,    1.0);
float[6] Ub = float[6](1.0, 1.0,    0.0,    1.0,    0.0,    0.0);

vec2[6] Vb = vec2[6](
	vec2(0.0, 1.0), vec2(0.0, 1.0), vec2(0.0, 0.0), vec2(1.0, 1.0), vec2(1.0, 0.0), vec2(1.0, 0.0)
	);

void Wb(uint Xb, vec3 Yb, vec2 Zb, vec3 _b, vec3 ac, 
    vec4 Ob, vec4 bc)
{
	_203C8330B731DD85 = Nb(Ob);

	vec2 cc = vec2(1, 1) - Zb;

	vec2 dc = vec2(Rb[Xb], Sb[Xb]) * Zb
		+ vec2(Tb[Xb], Ub[Xb]) * cc;

	vec3 ec = dc.x * _b + dc.y * ac;

    mat4 fc = Mb * _4F2DACEFFBDAB8D1;

	gl_Position = fc * vec4(Yb + ec, 1.0);
	
	_BEAD44E72AA5013D = Vb[Xb] * bc.zw + bc.xy;
}

void gc(uint Xb, vec3 Yb, vec2 Zb, float hc, vec2 ic, vec4 Ob, vec4 bc)
{
    vec3 jc = Kb;
    vec3 kc = Gb;

    float s = t(hc);
    float lc = -sin(s);
    float mc = cos(s);

    vec3 _b = vec3(
        jc.x * mc + kc.x * lc,
        jc.y * mc + kc.y * lc,
        jc.z * mc + kc.z * lc);

    vec3 ac = vec3(
        -jc.x * lc + kc.x * mc,
        -jc.y * lc + kc.y * mc,
        -jc.z * lc + kc.z * mc);

    Wb(Xb, Yb, Zb, _b * ic.x, ac * ic.y, Ob, bc);
}

void nc()
{
	gl_Position = vec4(-2, -2, 0, 1);
}

void oc(uint pc, uint Xb)
{
	e W = E(pc - 0u);
	uint Y = W.f;
	uint Z = W.f + W.g;

	vec3 Yb = A(u + 0u);
	float T = v(u + 8u);
	float ib = 30.0;
	float qc = v(u + 4u);
	float rc = 2.0;
	float sc = (qc / rc);
	float tc;
	float[6] uc = float[6](1.0,1.0,1.0,1.0,0.0,0.0);
	float vc = (sc < 0.0 ? 0.0:(sc>1.0?1.0:sc));
	float wc = vc<0.9?0.0+(vc - 0.0) * 1.11111:3.0+(vc - 0.9) * 10.0;
	tc = mix(uc[int(wc)], uc[int(wc) + 1], fract(wc));
	_94B3AA5995C3B5A1 = 0u;
	gc(Xb, A(u + 0u), vec2(0.5, 0.5), T, vec2(ib, ib), vec4(vec3(1.0, 1.0, 1.0), tc), Pb);
}

void xc(uint pc, uint Xb)
{
	e W = G(pc - 20u);
	uint Y = W.f;
	uint Z = W.f + W.g;

	vec3 Yb = A(u + 0u);
	float T = v(u + 8u);
	float ib = 30.0;
	float qc = v(u + 4u);
	float rc = 2.0;
	float sc = (qc / rc);
	float tc;
	float[6] uc = float[6](1.0,1.0,1.0,1.0,0.0,0.0);
	float vc = (sc < 0.0 ? 0.0:(sc>1.0?1.0:sc));
	float wc = vc<0.9?0.0+(vc - 0.0) * 1.11111:3.0+(vc - 0.9) * 10.0;
	tc = mix(uc[int(wc)], uc[int(wc) + 1], fract(wc));
	_94B3AA5995C3B5A1 = 1u;
	gc(Xb, A(u + 0u), vec2(0.5, 0.5), T, vec2(ib, ib), vec4(vec3(1.0, 1.0, 1.0), tc), Pb);
}

void main() {
	uvec2 yc = Qb();
	uint D = yc.x;
	uint Xb = yc.y;

	uvec2 zc = C(D);
	uint Ac = zc.x;
	uint h = zc.y;
	uint Bc = x(u + h);
	uint fb = Bc & 3u;

	if (fb != c)
	{
		nc();
		return;
	}
	switch (Ac)
	{
	case 0u: oc(D, Xb); break;
	case 1u: xc(D, Xb); break;
	}
}

#endif 

#ifdef _DC66F57E96C6C75C

uniform float _618E8A543B8FC66E;

flat in uint _94B3AA5995C3B5A1;
smooth in vec4 _203C8330B731DD85;

out vec4 _EB400D689750048A;

smooth in vec2 _BEAD44E72AA5013D;

vec4 Cc(sampler2D Dc, mat3x2 Ec)
{
	return texture(Dc, Ec * vec3(_BEAD44E72AA5013D, 1))* _203C8330B731DD85;
}

void makeFragColor(sampler2D Dc, mat3x2 Ec)
{
	vec4 Fc = Cc(Dc, Ec);

#ifdef _6335F91D31E2CC83
	Fc.rgb = Fc.rgb * _203C8330B731DD85.a;
#endif

	_EB400D689750048A = _618E8A543B8FC66E < 0.0 ?
		vec4(mix(vec3(1, 1, 1), Fc.rgb, Fc.a), 1) :
		Fc;
}

in makeFragColor()

#endif

