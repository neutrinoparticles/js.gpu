//DESC 1;-1;0.0;10.0;1;0;0;1;31;34;2;1;1;1;8;1;100;0;-1;-1;-1;6;0;6;Position;3;flags;1;Lifetime;1;Velocity;3;Angle;1;Rot00speed;1;0;0;
//TEXTURES default.png

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;
precision highp sampler3D;
#define _6A0EA0315208361E 31u
#define _2CE42649D807BFC4 1u

const uint a = 0u;
const uint b = 1u;
const uint c = 2u;
const uint d = 3u;

uniform sampler3D _5CABCD2CE500BEBB;

struct e
{
	uint f; 
	uint g; 
	uint h; 
	uint i; 
};

#if defined(_9CBA6082CFE05FA3) || defined(_B970C3D2AD57F959) || defined(_8CFBC1D9BA8D6516)

uint j = 0u;
uint k = 0u;
uint l = 0u;

void m(uint n)
{
	n &= 0xFFFFFFFFu;

	j = n < 2u ? n + 2u : n;
	k = n < 8u ? n + 8u : n;
	l = n < 16u ? n + 16u : n;
}

uint o()
{
	uint p = (((j << 13u) ^ j) & 0xFFFFFFFFu) >> 19u;
	j = ((j & 0xFFFFFFFEu) << 12u) ^ p;

	p = (((k << 2u) ^ k) & 0xFFFFFFFFu) >> 25u;
	k = ((k & 0xFFFFFFF8u) << 4u) ^ p;

	p = (((l << 3u) ^ l) & 0xFFFFFFFFu) >> 11u;
	l = ((l & 0xFFFFFFF0u) << 17u) ^ p;

	return (j ^ k) ^ l;
}

float q()
{
	return float(o()) / 4294967296.0;
}

#define _9D02A6055A27F4CF 3.1415926

uint r(uint s, uint p)
{
    return s - p * (s / p);
}

float t(float degrees)
{
	return degrees / 180.0 * _9D02A6055A27F4CF;
}

vec4 u(vec4 v, vec4 w, float s)
{
    float x = dot(v, w);

    vec4 y;
    vec4 z;

    if (x < 0.0)
    {
        z = -w;
        x = -x;
    }
    else
    {
        z = w;
    }

    if ((1.0 - x) < 0.000001)
    {
        return vec4(
            mix(v.x, w.x, s),
            mix(v.y, w.y, s),
            mix(v.z, w.z, s),
            mix(v.w, w.w, s));
    }
    else
    {
        float A = acos(x);
        return (sin((1.0 - s) * A) * v + sin(s * A) * z) / sin(A);
    }
}

vec3 B(vec3 C, vec4 D)
{
    float v = C.x;
    float w = C.y;
    float z = C.z;

    float E = D.x;
    float F = D.y;
    float G = D.z;
    float H = D.w;

    float I = H * v + F * z - G * w;
    float J = H * w + G * v - E * z;
    float K = H * z + E * w - F * v;
    float L = -E * v - F * w - G * z;
    
    return vec3(
        I * H + L * -E + J * -G - K * -F,
        J * H + L * -F + K * -E - I * -G,
        K * H + L * -G + I * -F - J * -E);
}

uniform usampler2D _E6B127475CD1AF15;
uniform usampler2D _1AAFF6BB1AC02A82;

uint M = 0u; 

float N(uint O)
{
	return uintBitsToFloat(texelFetch(_E6B127475CD1AF15,
		ivec2(O % _6A0EA0315208361E, O / _6A0EA0315208361E), 0).r);
}

uint P(uint O)
{
	return texelFetch(_E6B127475CD1AF15,
		ivec2(O % _6A0EA0315208361E, O / _6A0EA0315208361E), 0).r;
}

float Q(uint O)
{
	return uintBitsToFloat(texelFetch(_1AAFF6BB1AC02A82,
		ivec2(O % _6A0EA0315208361E, O / _6A0EA0315208361E), 0).r);
}

uint R(uint O)
{
	return texelFetch(_1AAFF6BB1AC02A82,
		ivec2(O % _6A0EA0315208361E, O / _6A0EA0315208361E), 0).r;
}

vec3 S(uint O)
{
	return vec3(N(O), N(O + 1u), N(O + 2u));
}

vec3 T(uint O)
{
	return vec3(Q(O), Q(O + 1u), Q(O + 2u));
}

vec4 U(uint O)
{
	return vec4(N(O), N(O + 1u), N(O + 2u), N(O + 3u));
}

vec4 V(uint O)
{
	return vec4(Q(O), Q(O + 1u), Q(O + 2u), Q(O + 3u));
}

uvec2 W(uint X)
{
	M = 31u + X * 10u;
	return uvec2(0u, 3u); 
}

e Y(uint Z)
{
	return e(0u, 8u, 7u, r(Z - 0u, 100u)); 
}

#endif

#ifdef _9CBA6082CFE05FA3

uniform sampler2D _17DCC16C9305C31F;
uniform float _0D6ECD3D4CD5F8C7;
uniform float _B29D96F4CC68440F;
uniform uint _600D80823B393136;
uniform uint _A62B4010E834E4BC;
uniform uint _8B5E47AC853BBD76;

float _;

void ab()
{
	gl_Position = vec4(-2.0, -2.0, 0, 1);
}

uint bb(uvec2 cb)
{
	return uint(texelFetch(_17DCC16C9305C31F, ivec2(cb), 0).r * 1023.0);
}

uint bb(uint db)
{
	return bb(uvec2(db % 2u, db / 2u));
}

uvec2 eb;

#if (_53986AB86EF3B727 == 0) 

#define _83AB904282DA95AA 0u

void fb()
{
	uint db;
	uint Z = uint(gl_VertexID);

	db = 0u + (Z) / 100u * 1u + (Z) % 100u % 1u;

	eb = uvec2(db % 2u, db / 2u);
	gl_Position = vec4((float(eb.x) + 0.5) / float(2u) * 2.0 - 1.0, (float(eb.y) + 0.5) / float(1u) * 2.0 - 1.0,  0, 1);
}

uint gb()
{
	return c;
}

vec3 hb;
uint ib;
float jb;
vec3 kb;
float lb;
float mb;

flat out vec3 _C09D6F3C5A9B9FBA;
flat out uint _98DBFB22A3024CC1;
flat out float _AF547B67E173F200;
flat out vec3 _BA00525347033428;
flat out float _D95F711B69033EF4;
flat out float _051111BB14C03F73;

uint X()
{
	return uint(gl_VertexID);
}

void nb(uint X)
{
	m((0xFFFFFFFFu / 100u * X) ^ _600D80823B393136 ^ ib);
}

bool ob(e pb, float qb, out float _93AC3C19AEE8CE0B)
{
	uint rb = pb.f;
	uint sb = pb.f + pb.g;

	uint tb = bb(eb);
	uint ub = 100u + ((uint(gl_VertexID) % 1u) < 0u ? 1u : 0u); 
	float vb = float(1u) * (float(ub) - float(tb));
	float wb = qb / vb;
	float xb = q();

	if (xb < (wb * (66.6667 * 1.0))) { _93AC3C19AEE8CE0B = q(); return true; } 
	return false;
}

void yb(uint zb)
{
	ib = (ib & 0xFFFFFFFCu) | zb;
}

e Ab()
{
	return Y(uint(gl_VertexID));
}

void Bb(e pb, float _93AC3C19AEE8CE0B)
{
	uint rb = pb.f;
	uint sb = pb.f + pb.g;

	jb = 0.0;
	vec3  Cb = vec3(0.0, 0.0, 0.0);
	hb = Cb;
	vec3 Db = mix(
		T(rb + 0u), 
		S(rb + 0u),
		_93AC3C19AEE8CE0B);
	hb += Db;
	float Eb = 0.0 + q() * (1.0 - 0.0);
	vec2[2] Fb = vec2[2](vec2(400.0, -17.0),vec2(400.0, 22.0));
	vec2[2] Gb = vec2[2](vec2(0.0, 1.0),vec2(0.0, 1.0));
	float Hb = clamp(Eb, 0.0, 1.0);
	float Ib = 0.0+(Hb - 0.0) * 1.0;
	vec2 Jb = mix(Fb[int(Ib)], Fb[int(Ib) + 1], fract(Ib));
	vec2 Kb = mix(Gb[int(Ib)], Gb[int(Ib) + 1], fract(Ib));
	vec3 Lb = vec3(Jb, 0.0);
	vec4 Mb = u(
		V(rb + 3u), 
		U(rb + 3u),
		_93AC3C19AEE8CE0B);
	kb = B(Lb, Mb);
	float Nb = 0.0 + q() * (360.0 - 0.0);
	lb = Nb;
	float Ob = -360.0 + q() * (360.0 - -360.0);
	mb = Ob;
}

void Pb()
{
	_ = _B29D96F4CC68440F;

	hb = T(M + 0u);
	jb = Q(M + 4u);
	kb = T(M + 5u);
	lb = Q(M + 8u);
	mb = Q(M + 9u);
}

void Qb(e pb)
{
	uint rb = pb.f;
	uint sb = pb.f + pb.g;

	_AF547B67E173F200 = jb + _; 
	_C09D6F3C5A9B9FBA = hb;
	_BA00525347033428 = kb;
	vec3  Cb = vec3(0.0, 500.0, 0.0);
	vec3 Rb = Cb;
	vec3 Sb = vec3(0.0, 0.0, 0.0);
	float Tb = _;
	vec3 Ub = _BA00525347033428;
	vec3 Vb = _C09D6F3C5A9B9FBA;
	while (Tb > 0.0001F) {
		float Wb = Tb;
		vec3 Xb = Rb;
		vec3 Yb = Sb - Ub;
		float Zb = dot(Yb, Yb);
		if (Zb > 0.000001) {
			Zb = sqrt(Zb);
			vec3 _b = Yb / Zb;
			float ac = 0.01 * 0.3 * Zb;
			if (ac * Wb > 0.2)
				Wb = 0.2 / ac;
			Xb = Xb + (_b * (Zb * ac));
		}
		Ub = Ub + (Xb * Wb);
		Vb = Vb + (Ub * Wb);
		Tb -= Wb;
	}
	_C09D6F3C5A9B9FBA = Vb;
	_BA00525347033428 = Ub;
	_D95F711B69033EF4 = lb;
	_051111BB14C03F73 = mb;
	float bc = _D95F711B69033EF4 + _051111BB14C03F73 * _;
	_D95F711B69033EF4 = bc;
	float cc = 1.5;
	if (_AF547B67E173F200 >= cc)
		yb(a);
}

void dc()
{
	_C09D6F3C5A9B9FBA = hb; 
	_98DBFB22A3024CC1 = ib; 
	_AF547B67E173F200 = jb; 
	_BA00525347033428 = kb; 
	_D95F711B69033EF4 = lb; 
	_051111BB14C03F73 = mb; 
}

#endif

void main() {
	uint X = _83AB904282DA95AA + uint(gl_VertexID);
	uint h = W(X).y;

	ib = R(M + h);
	uint zb = ib & 3u;

	fb();

	nb(X);

	uint ec = 0u;
	uint fc;

	e gc = Ab();

	switch (zb)
	{
	case a:
	{
		uint hc = P(gc.f + gc.h);
		uint ic = (hc & 3u);
		uint jc = ((ib >> 2u) & 3u); 

		float kc = (jc != 0u ? 
			_B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - float((ib >> 4u) + 1u) / float(0xFFFFFFFu)) : 
			_B29D96F4CC68440F
		);

float _93AC3C19AEE8CE0B = 0.f;

		if (ic == gb() && ob(gc, kc, _93AC3C19AEE8CE0B))
		{
			_ = kc * (1.0 - _93AC3C19AEE8CE0B);
			Bb(gc, _93AC3C19AEE8CE0B);

#ifdef _73599E697EB8FFF2
			yb(b);
			fc = uint(_93AC3C19AEE8CE0B * float(0xFFFFFFFu)) << 4u;
			dc();
#else
			yb(c);	
			Qb(gc);
			fc = o();
#endif
		}
		else
		{
			ab();

			if (ic == b)
			{
				fc = hc; 
				ec = 1u; 
			}
			else
			{
				fc = o();
			}
		}
				
		break;
	}

#ifdef _73599E697EB8FFF2
	case b:
	{
		Pb();
		float _93AC3C19AEE8CE0B = float(ib >> 4u) / float(0xFFFFFFFu);
		_ = _B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - _93AC3C19AEE8CE0B);
		Qb(gc);

		if ((ib & 3u) == a)
		{
			ab();
		}
		else
		{
			yb(c);
			ec = min(((ib >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		fc = ib;

		break;
	}
#endif

	case c:
	{
		Pb();
		_ = _B29D96F4CC68440F;
		Qb(gc);

		if ((ib & 3u) == a)
		{
			ab();
		}
		else
		{
			ec = min(((ib >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		fc = o();
					
		break;
	}

#ifdef _73599E697EB8FFF2
	case d:
	{
		Pb();
		lc();

		if ((ib & 3u) == a)
		{
			ab();
		}
		else
		{
			ec = min(((ib >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		fc = o();
		break;
	}
#endif
	}

	ib = (fc & 0xFFFFFFF0u) | (ec << 2u) | (ib & 3u);

	_98DBFB22A3024CC1 = ib;
	gl_PointSize = 1.0;
}

#endif 
#ifdef _CC5A472CC9FE17BA

out vec4 _4CCF54A3B6372A66;

void main()
{
	_4CCF54A3B6372A66 = vec4(1.01 / 1024.0, 0, 0, 0);
}

#endif

#ifdef _B970C3D2AD57F959

layout (std140) uniform _F52D3B1AA302F088
{
    vec3 mc;
    float nc;
    vec3 oc;
    uint pc;
    vec3 qc;
    uint rc;
    vec3 sc;
    float tc;
    mat4 uc;
};

uniform float _618E8A543B8FC66E;

uniform mat4 _4F2DACEFFBDAB8D1;

vec4 vc(vec4 wc)
{
	return _618E8A543B8FC66E < 0.0 ? wc : vec4(wc.rgb * _618E8A543B8FC66E, wc.a);
}

smooth out vec4 _203C8330B731DD85;
flat out uint _94B3AA5995C3B5A1;

vec4 xc = vec4(0, 0, 1, 1);

uvec2 yc()
{
	return uvec2(uint(gl_VertexID) / 6u, uint(gl_VertexID) % 6u);
}

smooth out vec2 _BEAD44E72AA5013D;

float[6] zc = float[6](-1.0,   -1.0,   -1.0,   0.0,   0.0,    0.0);
float[6] Ac = float[6](0.0,    0.0,    -1.0,    0.0,   -1.0,   -1.0);

float[6] Bc = float[6](0.0, 0.0,    0.0,    1.0,    1.0,    1.0);
float[6] Cc = float[6](1.0, 1.0,    0.0,    1.0,    0.0,    0.0);

vec2[6] Dc = vec2[6](
	vec2(0.0, 1.0), vec2(0.0, 1.0), vec2(0.0, 0.0), vec2(1.0, 1.0), vec2(1.0, 0.0), vec2(1.0, 0.0)
	);

void Ec(uint Fc, vec3 Gc, vec2 Hc, vec3 Ic, vec3 Jc, 
    vec4 wc, vec4 Kc)
{
	_203C8330B731DD85 = vc(wc);

	vec2 Lc = vec2(1, 1) - Hc;

	vec2 Mc = vec2(zc[Fc], Ac[Fc]) * Hc
		+ vec2(Bc[Fc], Cc[Fc]) * Lc;

	vec3 Nc = Mc.x * Ic + Mc.y * Jc;

    mat4 Oc = uc * _4F2DACEFFBDAB8D1;

	gl_Position = Oc * vec4(Gc + Nc, 1.0);
	
	_BEAD44E72AA5013D = Dc[Fc] * Kc.zw + Kc.xy;
}

void Pc(uint Fc, vec3 Gc, vec2 Hc, float A, vec2 Qc, vec4 wc, vec4 Kc)
{
    vec3 Rc = sc;
    vec3 Sc = oc;

    float s = t(A);
    float Tc = -sin(s);
    float Uc = cos(s);

    vec3 Ic = vec3(
        Rc.x * Uc + Sc.x * Tc,
        Rc.y * Uc + Sc.y * Tc,
        Rc.z * Uc + Sc.z * Tc);

    vec3 Jc = vec3(
        -Rc.x * Tc + Sc.x * Uc,
        -Rc.y * Tc + Sc.y * Uc,
        -Rc.z * Tc + Sc.z * Uc);

    Ec(Fc, Gc, Hc, Ic * Qc.x, Jc * Qc.y, wc, Kc);
}

void Vc()
{
	gl_Position = vec4(-2, -2, 0, 1);
}

void Wc(uint Xc, uint Fc)
{
	e pb = Y(Xc - 0u);
	uint rb = pb.f;
	uint sb = pb.f + pb.g;

	vec3 Gc = S(M + 0u);
	float lb = N(M + 8u);
	float Cb = 5.0;
	float Yc = N(M + 4u);
	float cc = 1.5;
	float Zc = (Yc / cc);
	float _c;
	float[3] ad = float[3](1.0,4.0,4.0);
	float bd = (Zc < 0.0 ? 0.0:(Zc>1.0?1.0:Zc));
	float cd = 0.0+(bd - 0.0) * 1.0;
	_c = mix(ad[int(cd)], ad[int(cd) + 1], fract(cd));
	float dd = (Cb * _c);
	_94B3AA5995C3B5A1 = 0u;
	Pc(Fc, S(M + 0u), vec2(0.5, 0.5), lb, vec2(dd, dd), vec4(vec3(1.0, 1.0, 1.0), 1.0), xc);
}

void main() {
	uvec2 ed = yc();
	uint X = ed.x;
	uint Fc = ed.y;

	uvec2 fd = W(X);
	uint gd = fd.x;
	uint h = fd.y;
	uint hd = P(M + h);
	uint zb = hd & 3u;

	if (zb != c)
	{
		Vc();
		return;
	}
	switch (gd)
	{
	case 0u: Wc(X, Fc); break;
	}
}

#endif 

#ifdef _DC66F57E96C6C75C

uniform float _618E8A543B8FC66E;

flat in uint _94B3AA5995C3B5A1;
smooth in vec4 _203C8330B731DD85;

out vec4 _EB400D689750048A;

smooth in vec2 _BEAD44E72AA5013D;

vec4 id(sampler2D jd, mat3x2 kd)
{
	return texture(jd, kd * vec3(_BEAD44E72AA5013D, 1))* _203C8330B731DD85;
}

void makeFragColor(sampler2D jd, mat3x2 kd)
{
	vec4 ld = id(jd, kd);

#ifdef _6335F91D31E2CC83
	ld.rgb = ld.rgb * _203C8330B731DD85.a;
#endif

	_EB400D689750048A = _618E8A543B8FC66E < 0.0 ?
		vec4(mix(vec3(1, 1, 1), ld.rgb, ld.a), 1) :
		ld;
}

in makeFragColor()

#endif

