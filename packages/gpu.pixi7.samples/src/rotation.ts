import { Effect, EffectsUpdateGroup } from '@neutrinoparticles/gpu.pixi7';

// Pick one of two following imports to choose between JS and WASM
//import { default as npgpuLoader } from '@neutrinoparticles/gpu.js';
import { default as npgpuLoader } from '@neutrinoparticles/gpu.wasm';

import * as PIXI from 'pixi.js';

const app = new PIXI.Application({ background: '#1099bb', resizeTo: window,
    neutrinoGPU: { // Additional options for NeutrinoParticles Context
        npgpuLoader, // Mandatory. NeutrinoParticles GPU library loader
		texturesBasePath: 'textures/', // Base path for textures. Will be prefixed to exported texture paths in effects
	}
 });

// Wait for GPU library loaded
await app.neutrinoGPU!.loadedPromise;

document.body.appendChild(app.view as HTMLCanvasElement);

PIXI.Assets.addBundle('resources', [
    // Request loading effect in the bundle. 'data' is neccessary to mark it as an NP effect
	{ name: 'testEffect', srcs: 'src/shaders/Rotation.shader', data: app.neutrinoGPU!.loadData},
]);

// Update group is for updating effects in a batch. It works faster.
const updateGroup = new EffectsUpdateGroup(app.neutrinoGPU!);

PIXI.Assets.loadBundle('resources').then((resources) => {

    // Bunny sprite on background
    {
        const bunny = PIXI.Sprite.from('https://pixijs.com/assets/bunny.png');
        bunny.anchor.set(0.5);
        bunny.x = app.screen.width / 2;
        bunny.y = app.screen.height / 2;
        bunny.scale = new PIXI.Point(20, 20);
        app.stage.addChild(bunny);
    }

    // Create instance of the effect
    const effect = new Effect(resources.testEffect);

    // Add the effect to the scene graph
    app.stage.addChild(effect);

    // Start the effect. Should be called after it is added to the scene graph
    // to calculate world position correctly.
    effect.start({
        position: [app.screen.width / 2, app.screen.height / 2, 0]
    })

    // Add it to the update group to update all effects at once
    updateGroup.add(effect);

    // Listen for animate update
    app.ticker.add((_delta) =>
    {
        const seconds = app.ticker.elapsedMS / 1000.0;
        effect.rotation += seconds;

		// Update effects. Better in a group.
        updateGroup.update(seconds);
    });
});




