import { Bloom, Camera2DPerspective, Effect, EffectsUpdateGroup } from '@neutrinoparticles/gpu.pixi7';

// Pick one of two following imports to choose between JS and WASM
//import { default as npgpuLoader } from '@neutrinoparticles/gpu.js';
import { default as npgpuLoader } from '@neutrinoparticles/gpu.wasm';

import * as PIXI from 'pixi.js';

const app = new PIXI.Application({ background: '#000000', resizeTo: window,
    neutrinoGPU: { // Additional options for NeutrinoParticles Context
        npgpuLoader, // Mandatory. NeutrinoParticles GPU library loader
		texturesBasePath: 'textures/', // Base path for textures. Will be prefixed to exported texture paths in effects
	}
 });

// Wait for GPU library loaded
await app.neutrinoGPU!.loadedPromise;

document.body.appendChild(app.view as HTMLCanvasElement);

PIXI.Assets.addBundle('resources', [
    // Request loading effect in the bundle. 'data' is neccessary to mark it as an NP effect
	{ name: 'testEffect', srcs: 'src/shaders/Bloom.shader', data: app.neutrinoGPU!.loadData},
]);

// Update group is for updating effects in a batch. It works faster.
const updateGroup = new EffectsUpdateGroup(app.neutrinoGPU!);

PIXI.Assets.loadBundle('resources').then((resources) => {

    // The scene will be rendered to this renderTexture
    const renderTexture = PIXI.RenderTexture.create({
        width: app.renderer.width,
        height: app.renderer.height,
        type: PIXI.TYPES.FLOAT,
        format: PIXI.FORMATS.RGBA,
    });

    // Setup background color here
    renderTexture.baseTexture.clear.setValue('#1099bb');

    // Keep renderTexture updated to the canvas size (optional)
    {
        const rendererResizeObserver = new ResizeObserver(entries => {
            for (let entry of entries) {
                const { width, height } = entry.contentRect;
                renderTexture.resize(width, height);
            }
        });
        rendererResizeObserver.observe(app.view as HTMLCanvasElement);
    }

    // A perspective camera that will be used to render the scene to the renderTexture
    const camera = new Camera2DPerspective(45, { 
        flippedY: true // Flip Y axis to correspond renderTexture orientation
    });
    // Keep the camera size updated to the renderTexture size
    renderTexture.baseTexture.on('update', () => {
        camera.resize(renderTexture.baseTexture.width, renderTexture.baseTexture.height);
    });    

    // A new root container for the scene (instead of app.stage)
    const newStage = new PIXI.Container();

    // Bunny sprite on background
    {
        const bunny = PIXI.Sprite.from('https://pixijs.com/assets/bunny.png');
        bunny.anchor.set(0.5);
        bunny.x = app.renderer.width / 2;
        bunny.y = app.renderer.height / 2;
        bunny.scale = new PIXI.Point(20, 20);
        newStage.addChild(bunny);
    }

    // Create effect
    const effect = new Effect(resources.testEffect);
    
    // Add the effect to the scene graph
    newStage.addChild(effect);

    // Start the effect. Should be called after it is added to the scene graph
    // to calculate world position correctly.
    effect.start({
        position: [app.renderer.width / 2, app.renderer.height / 2, 0]
    })

    // Add it to the update group to update all effects at once
    updateGroup.add(effect);

    // Bloom is an object to render in the main scene. It renders bloomed renderTexture
    // stratched to the whole renderer viewport
    const bloom = new Bloom(app.neutrinoGPU!, renderTexture, 10.0);
    // This object should be added to the main stage. Basically it can be the only object
    // on the stage.
    app.stage.addChild(bloom);

    // Listen for update
    app.ticker.add((_delta) =>
    {
		// Update effects
        updateGroup.update(app.ticker.elapsedMS / 1000.0);

        // Setup a new scene root for NeutrinoParticles
        app.neutrinoGPU!.pushRenderRoot(newStage, camera);
        // Render the scene with root newStage to the renderTexture
        app.renderer.render(newStage, { 
            renderTexture: renderTexture, 
        });
    });
});




