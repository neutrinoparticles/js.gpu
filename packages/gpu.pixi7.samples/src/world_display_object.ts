import { Effect, EffectsUpdateGroup } from '@neutrinoparticles/gpu.pixi7';

// Pick one of two following imports to choose between JS and WASM
//import { default as npgpuLoader } from '@neutrinoparticles/gpu.js';
import { default as npgpuLoader } from '@neutrinoparticles/gpu.wasm';

import * as PIXI from 'pixi.js';

const app = new PIXI.Application({ background: '#1099bb', resizeTo: window,
    neutrinoGPU: { // Additional options for NeutrinoParticles Context
        npgpuLoader, // Mandatory. NeutrinoParticles GPU library loader
		texturesBasePath: 'textures/', // Base path for textures. Will be prefixed to exported texture paths in effects
	}
 });

// Wait for GPU library loaded
await app.neutrinoGPU!.loadedPromise;

document.body.appendChild(app.view as HTMLCanvasElement);

PIXI.Assets.addBundle('resources', [
    { name: 'background', srcs: 'back.png' },

    // Request loading effect in the bundle. 'data' is neccessary to mark it as an NP effect
	{ name: 'testEffect', srcs: 'src/shaders/SmallSpray.shader', data: app.neutrinoGPU!.loadData},
]);

// Update group is for updating effects in a batch. It works faster.
const updateGroup = new EffectsUpdateGroup(app.neutrinoGPU!);

PIXI.Assets.loadBundle('resources').then((resources) => {
    // Create background sprite. This sprite will define world coordinates for the effect
    const back = new PIXI.Sprite(resources.background);
    const backSize = { width: back.width, height: back.height };
    const backRatio = back.width / back.height;
    app.stage.addChild(back);

    const effectParent = new PIXI.Container();
    effectParent.position = new PIXI.Point(backSize.width / 2, backSize.height / 2);
    back.addChild(effectParent);

    // Create effect
    {
		// Create an effect from effect model
        const effect = new Effect(resources.testEffect, {
            worldDisplayObject: back,
        });

        // Add the effect to the scene graph
        effectParent.addChild(effect);

        // Start the effect. Should be called after it is added to the scene graph
        // to calculate world position correctly.
        effect.start({
            position: [50, 0, 0],
        });

		// Add the effect to the update group to update all effects at once
        updateGroup.add(effect);
    }

    // Listen for animate update
    app.ticker.add((_delta) =>
    {
        effectParent.rotation += app.ticker.elapsedMS / 1000.0 * 2;

		// Update effects. Better in a group.
        updateGroup.update(app.ticker.elapsedMS / 1000.0);
    });

    function onWindowResized() 
    {
        let w, h;
        if (window.innerWidth / window.innerHeight >= backRatio) 
        {
            w = window.innerHeight * backRatio;
            h = window.innerHeight;
            back.position.x = (window.innerWidth - w) * 0.5;
            back.position.y = 0;
        } else 
        {
            w = window.innerWidth;
            h = window.innerWidth / backRatio;
            back.position.x = 0;
            back.position.y = (window.innerHeight - h) * 0.5;
        }

        back.scale.x = w / backSize.width;
        back.scale.y = h / backSize.height;
        app.renderer.resize(window.innerWidth, window.innerHeight);
    }
    window.onresize = onWindowResized;
    onWindowResized();
});




