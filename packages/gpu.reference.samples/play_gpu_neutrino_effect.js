const playGPUNeutrinoEffect = async (canvas, scriptsPath, texturesPath, shaderPath) => {
    eval(await(await fetch(scriptsPath + 'neutrinoparticles.renderer.gpu.wasm.umd.js')).text());
    eval(await(await fetch(scriptsPath + 'neutrinoparticles.gpu.reference.umd.js')).text());

    const bloomMaxRange = 10.0;

    let bloom = null;

    const enableBloom = true;

    const gl = canvas.getContext("webgl2", { premultipliedAlpha: false, alpha: false });
        
    const camera = new NPGPUReference.CameraPointObserve(1000.0, 45, gl.canvas.width, gl.canvas.height);

    const context = new NPGPUReference.Context(gl, {
        npgpuLoader: NPGPU,
        texturesLoader: new NPGPUReference.RequestTexturesLoader(gl, texturesPath),
        camera
    });

    await context.loadedPromise;

    const npgpu = context.npgpu;

    const onCanvasResized = (width, height) => {
        context.camera.resize(width, height);
        if (bloom) {
            bloom.resize(width, height);
        }
    };

    const resizeObserver = new ResizeObserver(entries => {
        for (let entry of entries) {
            const { width, height } = entry.contentRect;
            onCanvasResized(width, height);
        }
    });

    resizeObserver.observe(canvas);
    
    if (enableBloom) {
        context.gpu33Context.defaultColorMultiplier = 1.0 / bloomMaxRange;

        bloom = npgpu.BloomNonFloat.create(bloomMaxRange);
        if (!bloom) {
            throw new Error(npgpu.errorMessage());
        }
    }

    onCanvasResized(gl.canvas.width, gl.canvas.height);

    const fileResponse = await fetch(shaderPath);
    if (!fileResponse.ok)
        throw new Error("Cannot load effect file");

    const effectModel = new NPGPUReference.EffectModel(context, await fileResponse.text());
    const effect = new NPGPUReference.Effect(effectModel);

    effect.on('ready', () => {
        requestAnimationFrame(updateFrame);
    });

    let lastFrameTime = null;

    const updateFrame = () => {
        // time from previous frame calculations
        if (lastFrameTime == null) {
            lastFrameTime = Date.now();
        }

        var currentTime = Date.now();
        var elapsedTime = (currentTime - lastFrameTime) / 1000;
        if (elapsedTime > 0.5)
            elapsedTime = 0.5;
        lastFrameTime = currentTime;

        // Start rendering
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

        NPGPUReference.EffectsUpdateGroup.updateOne(context, effect, elapsedTime);
        
        if (bloom) {
            context.pushViewportSettings();
            bloom.setupForRenderScene();
        }

        {
            context.beginPreRender(gl.canvas.width, gl.canvas.height);

            effect.preRender();

            context.endPreRender();
        }

        // Render
        {
            gl.clearColor(0, 0, 0, 1);
            gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

            context.gpu33Context.beginRender();
            
            effect.render();
            
            context.gpu33Context.endRender();
        }

        if (bloom) {
            bloom.processBloom();

            // Render bloom results
            {
                context.popViewportSettings();
                bloom.renderResult();
            }
        }

        requestAnimationFrame(() => updateFrame());
    }

    // Handle mouse controls
    {
        const getMousePosition = (event) => {
            const rect = canvas.getBoundingClientRect();
            return {
                x: event.clientX - rect.left,
                y: event.clientY - rect.top,
            };
        };

        let mouseX, mouseY;
        let rotating = false;

        canvas.addEventListener("mousemove", (event) => {
            const pos = getMousePosition(event);
            if (!rotating) return;
            const sens = 1;
            camera.rotate(-(pos.x - mouseX) * sens, -(pos.y - mouseY) * sens);
            mouseX = pos.x;
            mouseY = pos.y;
        });

        canvas.addEventListener("mousedown", (event) => {
            const pos = getMousePosition(event);
            rotating = true;
            mouseX = pos.x;
            mouseY = pos.y;
        });

        canvas.addEventListener("mouseup", (event) => {
            rotating = false;
        });

        canvas.addEventListener("mouseleave", (event) => {
            rotating = false;
        });
    }
}