//DESC 1;-1;0.0;10.0;0;1;0;1549;1551;18;17;1;1;4;1;300000;0;-1;-1;-1;6;0;4;Position;3;flags;1;Lifetime;1;Velocity;3;0;1;
//TEXTURES sparks_core.png

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;
precision highp sampler3D;
#define _6A0EA0315208361E 1549u
#define _2CE42649D807BFC4 1u

const uint a = 0u;
const uint b = 1u;
const uint c = 2u;
const uint d = 3u;

uniform sampler3D _5CABCD2CE500BEBB;

struct e
{
	uint f; 
	uint g; 
	uint h; 
	uint i; 
};

#if defined(_9CBA6082CFE05FA3) || defined(_B970C3D2AD57F959) || defined(_8CFBC1D9BA8D6516)

uint j = 0u;
uint k = 0u;
uint l = 0u;

void m(uint n)
{
	n &= 0xFFFFFFFFu;

	j = n < 2u ? n + 2u : n;
	k = n < 8u ? n + 8u : n;
	l = n < 16u ? n + 16u : n;
}

uint o()
{
	uint p = (((j << 13u) ^ j) & 0xFFFFFFFFu) >> 19u;
	j = ((j & 0xFFFFFFFEu) << 12u) ^ p;

	p = (((k << 2u) ^ k) & 0xFFFFFFFFu) >> 25u;
	k = ((k & 0xFFFFFFF8u) << 4u) ^ p;

	p = (((l << 3u) ^ l) & 0xFFFFFFFFu) >> 11u;
	l = ((l & 0xFFFFFFF0u) << 17u) ^ p;

	return (j ^ k) ^ l;
}

float q()
{
	return float(o()) / 4294967296.0;
}

#define _9D02A6055A27F4CF 3.1415926

uint r(uint s, uint p)
{
    return s - p * (s / p);
}

vec3 t(float u)
{
	vec3 v;
	float w = q() * _9D02A6055A27F4CF * 2.0;
	float x = -1.0 + q() * 2.0;
	float y = u * sqrt(1.0 - x * x);
	v.x = y * cos(w);
	v.y = y * sin(w);
	v.z = u * x;
	return v;
}

uniform usampler2D _E6B127475CD1AF15;
uniform usampler2D _1AAFF6BB1AC02A82;

uint z = 0u; 

float A(uint B)
{
	return uintBitsToFloat(texelFetch(_E6B127475CD1AF15,
		ivec2(B % _6A0EA0315208361E, B / _6A0EA0315208361E), 0).r);
}

uint C(uint B)
{
	return texelFetch(_E6B127475CD1AF15,
		ivec2(B % _6A0EA0315208361E, B / _6A0EA0315208361E), 0).r;
}

float D(uint B)
{
	return uintBitsToFloat(texelFetch(_1AAFF6BB1AC02A82,
		ivec2(B % _6A0EA0315208361E, B / _6A0EA0315208361E), 0).r);
}

uint E(uint B)
{
	return texelFetch(_1AAFF6BB1AC02A82,
		ivec2(B % _6A0EA0315208361E, B / _6A0EA0315208361E), 0).r;
}

vec3 F(uint B)
{
	return vec3(A(B), A(B + 1u), A(B + 2u));
}

vec3 G(uint B)
{
	return vec3(D(B), D(B + 1u), D(B + 2u));
}

uvec2 H(uint I)
{
	z = 1549u + I * 8u;
	return uvec2(0u, 3u); 
}

e J(uint K)
{
	return e(0u, 4u, 3u, r(K - 0u, 300000u)); 
}

#endif

#ifdef _9CBA6082CFE05FA3

uniform sampler2D _17DCC16C9305C31F;
uniform float _0D6ECD3D4CD5F8C7;
uniform float _B29D96F4CC68440F;
uniform uint _600D80823B393136;
uniform uint _A62B4010E834E4BC;
uniform uint _8B5E47AC853BBD76;

float L;

void M()
{
	gl_Position = vec4(-2.0, -2.0, 0, 1);
}

uint N(uvec2 O)
{
	return uint(texelFetch(_17DCC16C9305C31F, ivec2(O), 0).r * 1023.0);
}

uint N(uint P)
{
	return N(uvec2(P % 18u, P / 18u));
}

uvec2 Q;

#if (_53986AB86EF3B727 == 0) 

#define _83AB904282DA95AA 0u

void R()
{
	uint P;
	uint K = uint(gl_VertexID);

	P = 0u + (K) / 300000u * 294u + (K) % 300000u % 294u;

	Q = uvec2(P % 18u, P / 18u);
	gl_Position = vec4((float(Q.x) + 0.5) / float(18u) * 2.0 - 1.0, (float(Q.y) + 0.5) / float(17u) * 2.0 - 1.0,  0, 1);
}

uint S()
{
	return c;
}

vec3 T;
uint U;
float V;
vec3 W;

flat out vec3 _C09D6F3C5A9B9FBA;
flat out uint _98DBFB22A3024CC1;
flat out float _AF547B67E173F200;
flat out vec3 _BA00525347033428;

uint I()
{
	return uint(gl_VertexID);
}

void X(uint I)
{
	m((0xFFFFFFFFu / 300000u * I) ^ _600D80823B393136 ^ U);
}

bool Y(e Z, float _, out float _93AC3C19AEE8CE0B)
{
	uint ab = Z.f;
	uint bb = Z.f + Z.g;

	uint cb = N(Q);
	uint db = 1020u + ((uint(gl_VertexID) % 294u) < 120u ? 1u : 0u); 
	float eb = float(294u) * (float(db) - float(cb));
	float fb = _ / eb;
	float gb = q();

	if (gb < (fb * (50.0 * 3000.0))) { _93AC3C19AEE8CE0B = q(); return true; } 
	return false;
}

void hb(uint ib)
{
	U = (U & 0xFFFFFFFCu) | ib;
}

e jb()
{
	return J(uint(gl_VertexID));
}

void kb(e Z, float _93AC3C19AEE8CE0B)
{
	uint ab = Z.f;
	uint bb = Z.f + Z.g;

	V = 0.0;
	vec3  lb = vec3(0.0, 0.0, 0.0);
	T = lb;
	vec3 mb = mix(
		G(ab + 0u), 
		F(ab + 0u),
		_93AC3C19AEE8CE0B);
	T += mb;
	vec3 nb = t(100.0);
	W = nb;
}

void ob()
{
	L = _B29D96F4CC68440F;

	T = G(z + 0u);
	V = D(z + 4u);
	W = G(z + 5u);
}

void pb(e Z)
{
	uint ab = Z.f;
	uint bb = Z.f + Z.g;

	_AF547B67E173F200 = V + L; 
	_C09D6F3C5A9B9FBA = T;
	_BA00525347033428 = W;
	vec3 qb = _C09D6F3C5A9B9FBA + _BA00525347033428 * L;
	_C09D6F3C5A9B9FBA = qb;
	float lb = 2.0;
	if (_AF547B67E173F200 >= lb)
		hb(a);
}

void rb()
{
	_C09D6F3C5A9B9FBA = T; 
	_98DBFB22A3024CC1 = U; 
	_AF547B67E173F200 = V; 
	_BA00525347033428 = W; 
}

#endif

void main() {
	uint I = _83AB904282DA95AA + uint(gl_VertexID);
	uint h = H(I).y;

	U = E(z + h);
	uint ib = U & 3u;

	R();

	X(I);

	uint sb = 0u;
	uint tb;

	e ub = jb();

	switch (ib)
	{
	case a:
	{
		uint vb = C(ub.f + ub.h);
		uint wb = (vb & 3u);
		uint xb = ((U >> 2u) & 3u); 

		float yb = (xb != 0u ? 
			_B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - float((U >> 4u) + 1u) / float(0xFFFFFFFu)) : 
			_B29D96F4CC68440F
		);

float _93AC3C19AEE8CE0B = 0.f;

		if (wb == S() && Y(ub, yb, _93AC3C19AEE8CE0B))
		{
			L = yb * (1.0 - _93AC3C19AEE8CE0B);
			kb(ub, _93AC3C19AEE8CE0B);

#ifdef _73599E697EB8FFF2
			hb(b);
			tb = uint(_93AC3C19AEE8CE0B * float(0xFFFFFFFu)) << 4u;
			rb();
#else
			hb(c);	
			pb(ub);
			tb = o();
#endif
		}
		else
		{
			M();

			if (wb == b)
			{
				tb = vb; 
				sb = 1u; 
			}
			else
			{
				tb = o();
			}
		}
				
		break;
	}

#ifdef _73599E697EB8FFF2
	case b:
	{
		ob();
		float _93AC3C19AEE8CE0B = float(U >> 4u) / float(0xFFFFFFFu);
		L = _B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - _93AC3C19AEE8CE0B);
		pb(ub);

		if ((U & 3u) == a)
		{
			M();
		}
		else
		{
			hb(c);
			sb = min(((U >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		tb = U;

		break;
	}
#endif

	case c:
	{
		ob();
		L = _B29D96F4CC68440F;
		pb(ub);

		if ((U & 3u) == a)
		{
			M();
		}
		else
		{
			sb = min(((U >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		tb = o();
					
		break;
	}

#ifdef _73599E697EB8FFF2
	case d:
	{
		ob();
		zb();

		if ((U & 3u) == a)
		{
			M();
		}
		else
		{
			sb = min(((U >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		tb = o();
		break;
	}
#endif
	}

	U = (tb & 0xFFFFFFF0u) | (sb << 2u) | (U & 3u);

	_98DBFB22A3024CC1 = U;
	gl_PointSize = 1.0;
}

#endif 
#ifdef _CC5A472CC9FE17BA

out vec4 _4CCF54A3B6372A66;

void main()
{
	_4CCF54A3B6372A66 = vec4(1.01 / 1024.0, 0, 0, 0);
}

#endif

#ifdef _8CFBC1D9BA8D6516

uniform sampler2D _50099FB73CB57A9E; 

uniform float _30B9E4384BFDB31C;
uniform vec3 _953A2A72B2E8B0E9;
uniform uint _14DB390EE2887C41;

flat out float _9BC2E3181A315B34;

flat out uint _5A57BFB62CAEDE57;

float Ab;
uint Bb;

float Cb(float Db)
{
	return Db * 0.5 + 0.5;
}
float Eb(float Db)
{
	return (Db - 0.5) * 2.0;
}

float Fb(float Db)
{
	return Cb(log2(abs(Db) + 1.0) / 32.0 * sign(Db));
}

float Gb(float Db)
{
	return (exp2(abs(Eb(Db)) * 32.0) - 1.0) * sign(Db);
}

const uint Hb = 1024u;

bool Ib(uint I)
{
	uint h = H(I).y;
	uint Jb = C(z + h);
	uint ib = Jb & 3u;

	if (ib != c)
	{
		return false;
	}

	float Kb = Gb(texelFetch(_50099FB73CB57A9E, ivec2(0, 0), 0).r);
	vec2 Lb = vec2(0, Kb);

vec3 Mb = F(z + 0u);

	Ab = dot(Mb, _953A2A72B2E8B0E9) + _30B9E4384BFDB31C;

	float Nb = clamp((Ab - Lb.x) / (Lb.y - Lb.x), 0.0, 1.0);
	Bb = uint(Nb * float(Hb - 1u));

	return true;
}

void main() {

	float Ob = 3.402823466e+38;
	float Pb = -3.402823466e+38;

	uint Qb;

	uint Rb = 1u;

	if (Ib(uint(gl_VertexID) * 2u))
	{
		Ob = Ab;
		Pb = Ab;
		Qb = Bb;
		Rb = 0u;
	}
	else
	{
		Qb = Hb;
	}

	uint Sb = uint(gl_VertexID) * 2u + 1u;
	if (Sb < _14DB390EE2887C41)
	{
		if (Ib(Sb))
		{
			Ob = min(Ob, Ab);
			Pb = max(Pb, Ab);
			Qb |= (Bb << 16);
			Rb = 0u;
		}
		else
		{
			Qb |= (Hb << 16);
		}
	}

	_5A57BFB62CAEDE57 = Qb;

	_9BC2E3181A315B34 = Fb(Pb);

	gl_Position = vec4(-2.0 * float(Rb), 0.0, 0.0, 1.0);
	gl_PointSize = 1.0;
}

#endif

#ifdef _A9EE343B6D5FED1F

flat in float _9BC2E3181A315B34;

out vec4 _EB400D689750048A;

void main()
{
	_EB400D689750048A = vec4(0, 0, 0, 0);
	gl_FragDepth = _9BC2E3181A315B34;
}

#endif

#ifdef _B970C3D2AD57F959

layout (std140) uniform _F52D3B1AA302F088
{
    vec3 Tb;
    float Ub;
    vec3 Vb;
    uint Wb;
    vec3 _953A2A72B2E8B0E9;
    uint Xb;
    vec3 Yb;
    float Zb;
    mat4 _b;
};

uniform float _618E8A543B8FC66E;

uniform mat4 _4F2DACEFFBDAB8D1;

vec4 ac(vec4 bc)
{
	return _618E8A543B8FC66E < 0.0 ? bc : vec4(bc.rgb * _618E8A543B8FC66E, bc.a);
}

smooth out vec4 _203C8330B731DD85;
flat out uint _94B3AA5995C3B5A1;

vec4 cc = vec4(0, 0, 1, 1);

uniform uint _94426D1BFF511BB0;

in uint _4D7C1BF9E36F0C6C;

uvec2 dc()
{
	return uvec2(_4D7C1BF9E36F0C6C >> 8, _4D7C1BF9E36F0C6C & 0xFFu);
}

flat out vec4 _BDF6B55D84170610;

void ec(uint fc, vec3 Mb, vec2 gc, vec2 hc, vec4 bc, vec4 ic)
{
	vec3 jc = cross(Vb, _953A2A72B2E8B0E9);
	vec3 kc = Vb;

	_203C8330B731DD85 = ac(bc);
	_BDF6B55D84170610 = ic;

	float lc = hc.x;

	Mb -= (jc * (gc.x - 0.5) + kc * (gc.y - 0.5)) * lc;

	mat4 mc = _b * _4F2DACEFFBDAB8D1;

	vec4 nc = mc * vec4(Mb +
		Vb * lc * 0.5, 1.0);

	vec4 oc = mc * vec4(Mb, 1.0);
	gl_PointSize = abs(nc.y / nc.w - oc.y / oc.w) *
		float(Xb);
	gl_Position = oc;
}

void pc()
{
	gl_PointSize = 1.0;
	gl_Position = vec4(-2.0, -2.0, 0.0, 1.0);
}

void qc(uint rc, uint fc)
{
	e Z = J(rc - 0u);
	uint ab = Z.f;
	uint bb = Z.f + Z.g;

	vec3 Mb = F(z + 0u);
	float lb = 3.0;
	float sc = A(z + 4u);
	float tc = 2.0;
	float uc = (sc / tc);
	vec3 vc;
	float[6] wc = float[6](1.0,1.0,1.0,1.0,0.5,0.5);
	float xc = (uc < 0.0 ? 0.0:(uc>1.0?1.0:uc));
	float yc = xc<0.5?0.0+(xc - 0.0) * 2.0:3.0+(xc - 0.5) * 2.0;
	vc.x = mix(wc[int(yc)], wc[int(yc) + 1], fract(yc));
	float[6] zc = float[6](1.0,1.0,1.0,1.0,0.0,0.0);
	float Ac = (uc < 0.0 ? 0.0:(uc>1.0?1.0:uc));
	float Bc = Ac<0.5?0.0+(Ac - 0.0) * 2.0:3.0+(Ac - 0.5) * 2.0;
	vc.y = mix(zc[int(Bc)], zc[int(Bc) + 1], fract(Bc));
	float[6] Cc = float[6](1.0,0.0,0.0,0.0,0.0,0.0);
	float Dc = (uc < 0.0 ? 0.0:(uc>1.0?1.0:uc));
	float Ec = Dc<0.5?0.0+(Dc - 0.0) * 2.0:3.0+(Dc - 0.5) * 2.0;
	vc.z = mix(Cc[int(Ec)], Cc[int(Ec) + 1], fract(Ec));
	float Fc;
	float[6] Gc = float[6](1.0,1.0,1.0,1.0,0.0,0.0);
	float Hc = (uc < 0.0 ? 0.0:(uc>1.0?1.0:uc));
	float Ic = Hc<0.745402?0.0+(Hc - 0.0) * 1.34156:3.0+(Hc - 0.745402) * 3.92777;
	Fc = mix(Gc[int(Ic)], Gc[int(Ic) + 1], fract(Ic));
	_94B3AA5995C3B5A1 = 0u;
	ec(fc, F(z + 0u), vec2(0.5, 0.5), vec2(lb, lb), vec4(vc, Fc), cc);
}

void main() {
	uvec2 Jc = dc();
	uint I = Jc.x;
	uint fc = Jc.y;

	uvec2 Kc = H(I);
	uint Lc = Kc.x;
	uint h = Kc.y;
	uint Jb = C(z + h);
	uint ib = Jb & 3u;

	if (ib != c)
	{
		pc();
		return;
	}

	if (((Jb >> 2u) & 3u) < _94426D1BFF511BB0)
	{
		pc();
		return;
	}
	switch (Lc)
	{
	case 0u: qc(I, fc); break;
	}
}

#endif 

#ifdef _DC66F57E96C6C75C

uniform float _618E8A543B8FC66E;

flat in uint _94B3AA5995C3B5A1;
smooth in vec4 _203C8330B731DD85;

out vec4 _EB400D689750048A;

flat in vec4 _BDF6B55D84170610;

vec4 Mc(sampler2D Nc, mat3x2 Oc)
{
	vec2 Pc = vec2(gl_PointCoord.x, 1.0 - gl_PointCoord.y);
	return texture(Nc, 
		Oc * vec3(_BDF6B55D84170610.xy + Pc * _BDF6B55D84170610.zw, 1)) * _203C8330B731DD85;
}

void makeFragColor(sampler2D Nc, mat3x2 Oc)
{
	vec4 Qc = Mc(Nc, Oc);
#ifdef _6335F91D31E2CC83
	Qc.rgb = Qc.rgb * _203C8330B731DD85.a;
#endif

	_EB400D689750048A = _618E8A543B8FC66E < 0.0 ?
		vec4(mix(vec3(1, 1, 1), Qc.rgb, Qc.a), 1) :
		Qc;
}

in makeFragColor()

#endif

