//DESC 1;-1;0.0;10.0;0;1;0;2121;2123;23;22;1;1;4;1;500000;0;-1;-1;-1;6;0;5;Position;3;flags;1;ColorIterp;1;Lifetime;1;Velocity;3;0;0;
//TEXTURES tile.png

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;
precision highp sampler3D;
#define _6A0EA0315208361E 2121u
#define _2CE42649D807BFC4 1u

const uint a = 0u;
const uint b = 1u;
const uint c = 2u;
const uint d = 3u;

uniform sampler3D _5CABCD2CE500BEBB;

struct e
{
	uint f; 
	uint g; 
	uint h; 
	uint i; 
};

#if defined(_9CBA6082CFE05FA3) || defined(_B970C3D2AD57F959) || defined(_8CFBC1D9BA8D6516)

uint j = 0u;
uint k = 0u;
uint l = 0u;

void m(uint n)
{
	n &= 0xFFFFFFFFu;

	j = n < 2u ? n + 2u : n;
	k = n < 8u ? n + 8u : n;
	l = n < 16u ? n + 16u : n;
}

uint o()
{
	uint p = (((j << 13u) ^ j) & 0xFFFFFFFFu) >> 19u;
	j = ((j & 0xFFFFFFFEu) << 12u) ^ p;

	p = (((k << 2u) ^ k) & 0xFFFFFFFFu) >> 25u;
	k = ((k & 0xFFFFFFF8u) << 4u) ^ p;

	p = (((l << 3u) ^ l) & 0xFFFFFFFFu) >> 11u;
	l = ((l & 0xFFFFFFF0u) << 17u) ^ p;

	return (j ^ k) ^ l;
}

float q()
{
	return float(o()) / 4294967296.0;
}

#define _9D02A6055A27F4CF 3.1415926

uint r(uint s, uint p)
{
    return s - p * (s / p);
}

uniform usampler2D _E6B127475CD1AF15;
uniform usampler2D _1AAFF6BB1AC02A82;

uint t = 0u; 

float u(uint v)
{
	return uintBitsToFloat(texelFetch(_E6B127475CD1AF15,
		ivec2(v % _6A0EA0315208361E, v / _6A0EA0315208361E), 0).r);
}

uint w(uint v)
{
	return texelFetch(_E6B127475CD1AF15,
		ivec2(v % _6A0EA0315208361E, v / _6A0EA0315208361E), 0).r;
}

float x(uint v)
{
	return uintBitsToFloat(texelFetch(_1AAFF6BB1AC02A82,
		ivec2(v % _6A0EA0315208361E, v / _6A0EA0315208361E), 0).r);
}

uint y(uint v)
{
	return texelFetch(_1AAFF6BB1AC02A82,
		ivec2(v % _6A0EA0315208361E, v / _6A0EA0315208361E), 0).r;
}

vec3 z(uint v)
{
	return vec3(u(v), u(v + 1u), u(v + 2u));
}

vec3 A(uint v)
{
	return vec3(x(v), x(v + 1u), x(v + 2u));
}

uvec2 B(uint C)
{
	t = 2121u + C * 9u;
	return uvec2(0u, 3u); 
}

e D(uint E)
{
	return e(0u, 4u, 3u, r(E - 0u, 500000u)); 
}

#endif

#ifdef _9CBA6082CFE05FA3

uniform sampler2D _17DCC16C9305C31F;
uniform float _0D6ECD3D4CD5F8C7;
uniform float _B29D96F4CC68440F;
uniform uint _600D80823B393136;
uniform uint _A62B4010E834E4BC;
uniform uint _8B5E47AC853BBD76;

float F;

void G()
{
	gl_Position = vec4(-2.0, -2.0, 0, 1);
}

uint H(uvec2 I)
{
	return uint(texelFetch(_17DCC16C9305C31F, ivec2(I), 0).r * 1023.0);
}

uint H(uint J)
{
	return H(uvec2(J % 23u, J / 23u));
}

uvec2 K;

#if (_53986AB86EF3B727 == 0) 

#define _83AB904282DA95AA 0u

void L()
{
	uint J;
	uint E = uint(gl_VertexID);

	J = 0u + (E) / 500000u * 489u + (E) % 500000u % 489u;

	K = uvec2(J % 23u, J / 23u);
	gl_Position = vec4((float(K.x) + 0.5) / float(23u) * 2.0 - 1.0, (float(K.y) + 0.5) / float(22u) * 2.0 - 1.0,  0, 1);
}

uint M()
{
	return c;
}

vec3 N;
uint O;
float P;
float Q;
vec3 R;

flat out vec3 _C09D6F3C5A9B9FBA;
flat out uint _98DBFB22A3024CC1;
flat out float _9E69C311C9EE800E;
flat out float _AF547B67E173F200;
flat out vec3 _BA00525347033428;

uint C()
{
	return uint(gl_VertexID);
}

void S(uint C)
{
	m((0xFFFFFFFFu / 500000u * C) ^ _600D80823B393136 ^ O);
}

bool T(e U, float V, out float _93AC3C19AEE8CE0B)
{
	uint W = U.f;
	uint X = U.f + U.g;

	uint Y = H(K);
	uint Z = 1022u + ((uint(gl_VertexID) % 489u) < 242u ? 1u : 0u); 
	float _ = float(489u) * (float(Z) - float(Y));
	float ab = V / _;
	float bb = q();

	if (bb < (ab * (50.0 * 5000.0))) { _93AC3C19AEE8CE0B = q(); return true; } 
	return false;
}

void cb(uint db)
{
	O = (O & 0xFFFFFFFCu) | db;
}

e eb()
{
	return D(uint(gl_VertexID));
}

void fb(e U, float _93AC3C19AEE8CE0B)
{
	uint W = U.f;
	uint X = U.f + U.g;

	float gb = 0.0 + q() * (1.0 - 0.0);
	P = gb;
	Q = 0.0;
	float hb = -500.0 + q() * (500.0 - -500.0);
	float ib = -500.0 + q() * (500.0 - -500.0);
	float jb = -500.0 + q() * (500.0 - -500.0);
	vec3  kb = vec3(hb, ib, jb);
	N = kb;
	vec3 lb = mix(
		A(W + 0u), 
		z(W + 0u),
		_93AC3C19AEE8CE0B);
	N += lb;
	float mb = -1.0 + q() * (1.0 - -1.0);
	float nb = -1.0 + q() * (1.0 - -1.0);
	float ob = -1.0 + q() * (1.0 - -1.0);
	vec3  pb = vec3(mb, nb, ob);
	vec3 qb = (normalize(pb) * 2.5);
	R = qb;
}

void rb()
{
	F = _B29D96F4CC68440F;

	N = A(t + 0u);
	P = x(t + 4u);
	Q = x(t + 5u);
	R = A(t + 6u);
}

void sb(e U)
{
	uint W = U.f;
	uint X = U.f + U.g;

	_9E69C311C9EE800E = P;
	_AF547B67E173F200 = Q + F; 
	_C09D6F3C5A9B9FBA = N;
	_BA00525347033428 = R;
	vec3 tb = _C09D6F3C5A9B9FBA + _BA00525347033428 * F;
	_C09D6F3C5A9B9FBA = tb;
	float kb = 2.0;
	if (_AF547B67E173F200 >= kb)
		cb(a);
}

void ub()
{
	_C09D6F3C5A9B9FBA = N; 
	_98DBFB22A3024CC1 = O; 
	_9E69C311C9EE800E = P; 
	_AF547B67E173F200 = Q; 
	_BA00525347033428 = R; 
}

#endif

void main() {
	uint C = _83AB904282DA95AA + uint(gl_VertexID);
	uint h = B(C).y;

	O = y(t + h);
	uint db = O & 3u;

	L();

	S(C);

	uint vb = 0u;
	uint wb;

	e xb = eb();

	switch (db)
	{
	case a:
	{
		uint yb = w(xb.f + xb.h);
		uint zb = (yb & 3u);
		uint Ab = ((O >> 2u) & 3u); 

		float Bb = (Ab != 0u ? 
			_B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - float((O >> 4u) + 1u) / float(0xFFFFFFFu)) : 
			_B29D96F4CC68440F
		);

float _93AC3C19AEE8CE0B = 0.f;

		if (zb == M() && T(xb, Bb, _93AC3C19AEE8CE0B))
		{
			F = Bb * (1.0 - _93AC3C19AEE8CE0B);
			fb(xb, _93AC3C19AEE8CE0B);

#ifdef _73599E697EB8FFF2
			cb(b);
			wb = uint(_93AC3C19AEE8CE0B * float(0xFFFFFFFu)) << 4u;
			ub();
#else
			cb(c);	
			sb(xb);
			wb = o();
#endif
		}
		else
		{
			G();

			if (zb == b)
			{
				wb = yb; 
				vb = 1u; 
			}
			else
			{
				wb = o();
			}
		}
				
		break;
	}

#ifdef _73599E697EB8FFF2
	case b:
	{
		rb();
		float _93AC3C19AEE8CE0B = float(O >> 4u) / float(0xFFFFFFFu);
		F = _B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - _93AC3C19AEE8CE0B);
		sb(xb);

		if ((O & 3u) == a)
		{
			G();
		}
		else
		{
			cb(c);
			vb = min(((O >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		wb = O;

		break;
	}
#endif

	case c:
	{
		rb();
		F = _B29D96F4CC68440F;
		sb(xb);

		if ((O & 3u) == a)
		{
			G();
		}
		else
		{
			vb = min(((O >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		wb = o();
					
		break;
	}

#ifdef _73599E697EB8FFF2
	case d:
	{
		rb();
		Cb();

		if ((O & 3u) == a)
		{
			G();
		}
		else
		{
			vb = min(((O >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		wb = o();
		break;
	}
#endif
	}

	O = (wb & 0xFFFFFFF0u) | (vb << 2u) | (O & 3u);

	_98DBFB22A3024CC1 = O;
	gl_PointSize = 1.0;
}

#endif 
#ifdef _CC5A472CC9FE17BA

out vec4 _4CCF54A3B6372A66;

void main()
{
	_4CCF54A3B6372A66 = vec4(1.01 / 1024.0, 0, 0, 0);
}

#endif

#ifdef _B970C3D2AD57F959

layout (std140) uniform _F52D3B1AA302F088
{
    vec3 Db;
    float Eb;
    vec3 Fb;
    uint Gb;
    vec3 Hb;
    uint Ib;
    vec3 Jb;
    float Kb;
    mat4 Lb;
};

uniform float _618E8A543B8FC66E;

uniform mat4 _4F2DACEFFBDAB8D1;

vec4 Mb(vec4 Nb)
{
	return _618E8A543B8FC66E < 0.0 ? Nb : vec4(Nb.rgb * _618E8A543B8FC66E, Nb.a);
}

smooth out vec4 _203C8330B731DD85;
flat out uint _94B3AA5995C3B5A1;

vec4 Ob = vec4(0, 0, 1, 1);

uvec2 Pb()
{
	return uvec2(uint(gl_VertexID) / 1u, uint(gl_VertexID) % 1u);
}

flat out vec4 _BDF6B55D84170610;

void Qb(uint Rb, vec3 Sb, vec2 Tb, vec2 Ub, vec4 Nb, vec4 Vb)
{
	vec3 Wb = cross(Fb, Hb);
	vec3 Xb = Fb;

	_203C8330B731DD85 = Mb(Nb);
	_BDF6B55D84170610 = Vb;

	float Yb = Ub.x;

	Sb -= (Wb * (Tb.x - 0.5) + Xb * (Tb.y - 0.5)) * Yb;

	mat4 Zb = Lb * _4F2DACEFFBDAB8D1;

	vec4 _b = Zb * vec4(Sb +
		Fb * Yb * 0.5, 1.0);

	vec4 ac = Zb * vec4(Sb, 1.0);
	gl_PointSize = abs(_b.y / _b.w - ac.y / ac.w) *
		float(Ib);
	gl_Position = ac;
}

void bc()
{
	gl_PointSize = 1.0;
	gl_Position = vec4(-2.0, -2.0, 0.0, 1.0);
}

void cc(uint dc, uint Rb)
{
	e U = D(dc - 0u);
	uint W = U.f;
	uint X = U.f + U.g;

	vec3 Sb = z(t + 0u);
	float kb = 1.25;
	float P = u(t + 4u);
	vec3 qb = mix(vec3(1.0, 1.0, 0.0), vec3(1.0, 0.0, 0.0), P);
	_94B3AA5995C3B5A1 = 0u;
	Qb(Rb, z(t + 0u), vec2(0.5, 0.5), vec2(kb, kb), vec4(qb, 1.0), Ob);
}

void main() {
	uvec2 ec = Pb();
	uint C = ec.x;
	uint Rb = ec.y;

	uvec2 fc = B(C);
	uint gc = fc.x;
	uint h = fc.y;
	uint hc = w(t + h);
	uint db = hc & 3u;

	if (db != c)
	{
		bc();
		return;
	}
	switch (gc)
	{
	case 0u: cc(C, Rb); break;
	}
}

#endif 

#ifdef _DC66F57E96C6C75C

uniform float _618E8A543B8FC66E;

flat in uint _94B3AA5995C3B5A1;
smooth in vec4 _203C8330B731DD85;

out vec4 _EB400D689750048A;

flat in vec4 _BDF6B55D84170610;

vec4 ic(sampler2D jc, mat3x2 kc)
{
	vec2 lc = vec2(gl_PointCoord.x, 1.0 - gl_PointCoord.y);
	return texture(jc, 
		kc * vec3(_BDF6B55D84170610.xy + lc * _BDF6B55D84170610.zw, 1)) * _203C8330B731DD85;
}

void makeFragColor(sampler2D jc, mat3x2 kc)
{
	vec4 mc = ic(jc, kc);
#ifdef _6335F91D31E2CC83
	mc.rgb = mc.rgb * _203C8330B731DD85.a;
#endif

	_EB400D689750048A = _618E8A543B8FC66E < 0.0 ?
		vec4(mix(vec3(1, 1, 1), mc.rgb, mc.a), 1) :
		mc;
}

in makeFragColor()

#endif

