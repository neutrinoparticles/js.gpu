//DESC 1;-1;0.0;10.0;0;0;0;94;97;2;1;1;1;4;1;1000;0;-1;-1;-1;6;0;5;Position;3;flags;1;Lifetime;1;Velocity;3;Angle;1;0;0;
//TEXTURES sparks_core.png

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;
precision highp sampler3D;
#define _6A0EA0315208361E 94u
#define _2CE42649D807BFC4 1u

const uint a = 0u;
const uint b = 1u;
const uint c = 2u;
const uint d = 3u;

uniform sampler3D _5CABCD2CE500BEBB;

struct e
{
	uint f; 
	uint g; 
	uint h; 
	uint i; 
};

#if defined(_9CBA6082CFE05FA3) || defined(_B970C3D2AD57F959) || defined(_8CFBC1D9BA8D6516)

uint j = 0u;
uint k = 0u;
uint l = 0u;

void m(uint n)
{
	n &= 0xFFFFFFFFu;

	j = n < 2u ? n + 2u : n;
	k = n < 8u ? n + 8u : n;
	l = n < 16u ? n + 16u : n;
}

uint o()
{
	uint p = (((j << 13u) ^ j) & 0xFFFFFFFFu) >> 19u;
	j = ((j & 0xFFFFFFFEu) << 12u) ^ p;

	p = (((k << 2u) ^ k) & 0xFFFFFFFFu) >> 25u;
	k = ((k & 0xFFFFFFF8u) << 4u) ^ p;

	p = (((l << 3u) ^ l) & 0xFFFFFFFFu) >> 11u;
	l = ((l & 0xFFFFFFF0u) << 17u) ^ p;

	return (j ^ k) ^ l;
}

float q()
{
	return float(o()) / 4294967296.0;
}

#define _9D02A6055A27F4CF 3.1415926

uint r(uint s, uint p)
{
    return s - p * (s / p);
}

vec3 t(float u)
{
	vec3 v;
	float w = q() * _9D02A6055A27F4CF * 2.0;
	float x = -1.0 + q() * 2.0;
	float y = u * sqrt(1.0 - x * x);
	v.x = y * cos(w);
	v.y = y * sin(w);
	v.z = u * x;
	return v;
}

float z(float degrees)
{
	return degrees / 180.0 * _9D02A6055A27F4CF;
}

uniform usampler2D _E6B127475CD1AF15;
uniform usampler2D _1AAFF6BB1AC02A82;

uint A = 0u; 

float B(uint C)
{
	return uintBitsToFloat(texelFetch(_E6B127475CD1AF15,
		ivec2(C % _6A0EA0315208361E, C / _6A0EA0315208361E), 0).r);
}

uint D(uint C)
{
	return texelFetch(_E6B127475CD1AF15,
		ivec2(C % _6A0EA0315208361E, C / _6A0EA0315208361E), 0).r;
}

float E(uint C)
{
	return uintBitsToFloat(texelFetch(_1AAFF6BB1AC02A82,
		ivec2(C % _6A0EA0315208361E, C / _6A0EA0315208361E), 0).r);
}

uint F(uint C)
{
	return texelFetch(_1AAFF6BB1AC02A82,
		ivec2(C % _6A0EA0315208361E, C / _6A0EA0315208361E), 0).r;
}

vec3 G(uint C)
{
	return vec3(B(C), B(C + 1u), B(C + 2u));
}

vec3 H(uint C)
{
	return vec3(E(C), E(C + 1u), E(C + 2u));
}

uvec2 I(uint J)
{
	A = 94u + J * 9u;
	return uvec2(0u, 3u); 
}

e K(uint L)
{
	return e(0u, 4u, 3u, r(L - 0u, 1000u)); 
}

#endif

#ifdef _9CBA6082CFE05FA3

uniform sampler2D _17DCC16C9305C31F;
uniform float _0D6ECD3D4CD5F8C7;
uniform float _B29D96F4CC68440F;
uniform uint _600D80823B393136;
uniform uint _A62B4010E834E4BC;
uniform uint _8B5E47AC853BBD76;

float M;

void N()
{
	gl_Position = vec4(-2.0, -2.0, 0, 1);
}

uint O(uvec2 P)
{
	return uint(texelFetch(_17DCC16C9305C31F, ivec2(P), 0).r * 1023.0);
}

uint O(uint Q)
{
	return O(uvec2(Q % 2u, Q / 2u));
}

uvec2 R;

#if (_53986AB86EF3B727 == 0) 

#define _83AB904282DA95AA 0u

void S()
{
	uint Q;
	uint L = uint(gl_VertexID);

	Q = 0u + (L) / 1000u * 1u + (L) % 1000u % 1u;

	R = uvec2(Q % 2u, Q / 2u);
	gl_Position = vec4((float(R.x) + 0.5) / float(2u) * 2.0 - 1.0, (float(R.y) + 0.5) / float(1u) * 2.0 - 1.0,  0, 1);
}

uint T()
{
	return c;
}

vec3 U;
uint V;
float W;
vec3 X;
float Y;

flat out vec3 _C09D6F3C5A9B9FBA;
flat out uint _98DBFB22A3024CC1;
flat out float _AF547B67E173F200;
flat out vec3 _BA00525347033428;
flat out float _D95F711B69033EF4;

uint J()
{
	return uint(gl_VertexID);
}

void Z(uint J)
{
	m((0xFFFFFFFFu / 1000u * J) ^ _600D80823B393136 ^ V);
}

bool _(e ab, float bb, out float _93AC3C19AEE8CE0B)
{
	uint cb = ab.f;
	uint db = ab.f + ab.g;

	uint eb = O(R);
	uint fb = 1000u + ((uint(gl_VertexID) % 1u) < 0u ? 1u : 0u); 
	float gb = float(1u) * (float(fb) - float(eb));
	float hb = bb / gb;
	float ib = q();

	if (ib < (hb * (50.0 * 10.0))) { _93AC3C19AEE8CE0B = q(); return true; } 
	return false;
}

void jb(uint kb)
{
	V = (V & 0xFFFFFFFCu) | kb;
}

e lb()
{
	return K(uint(gl_VertexID));
}

void mb(e ab, float _93AC3C19AEE8CE0B)
{
	uint cb = ab.f;
	uint db = ab.f + ab.g;

	W = 0.0;
	vec3  nb = vec3(0.0, 0.0, 0.0);
	U = nb;
	vec3 ob = mix(
		H(cb + 0u), 
		G(cb + 0u),
		_93AC3C19AEE8CE0B);
	U += ob;
	vec3 pb = t(100.0);
	X = pb;
	Y = 0.0;
}

void qb()
{
	M = _B29D96F4CC68440F;

	U = H(A + 0u);
	W = E(A + 4u);
	X = H(A + 5u);
	Y = E(A + 8u);
}

void rb(e ab)
{
	uint cb = ab.f;
	uint db = ab.f + ab.g;

	_AF547B67E173F200 = W + M; 
	_C09D6F3C5A9B9FBA = U;
	_BA00525347033428 = X;
	vec3 sb = vec3(0.0, 0.0, 0.0);
	vec3 tb = vec3(0.0, 0.0, 0.0);
	vec3 ub = _BA00525347033428 + sb * M;
	vec3 vb = ub * M;
vb = vb + _C09D6F3C5A9B9FBA;
	_C09D6F3C5A9B9FBA = vb;
	_BA00525347033428 = ub;
	_D95F711B69033EF4 = Y;
	float nb = 2.0;
	if (_AF547B67E173F200 >= nb)
		jb(a);
}

void wb()
{
	_C09D6F3C5A9B9FBA = U; 
	_98DBFB22A3024CC1 = V; 
	_AF547B67E173F200 = W; 
	_BA00525347033428 = X; 
	_D95F711B69033EF4 = Y; 
}

#endif

void main() {
	uint J = _83AB904282DA95AA + uint(gl_VertexID);
	uint h = I(J).y;

	V = F(A + h);
	uint kb = V & 3u;

	S();

	Z(J);

	uint xb = 0u;
	uint yb;

	e zb = lb();

	switch (kb)
	{
	case a:
	{
		uint Ab = D(zb.f + zb.h);
		uint Bb = (Ab & 3u);
		uint Cb = ((V >> 2u) & 3u); 

		float Db = (Cb != 0u ? 
			_B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - float((V >> 4u) + 1u) / float(0xFFFFFFFu)) : 
			_B29D96F4CC68440F
		);

float _93AC3C19AEE8CE0B = 0.f;

		if (Bb == T() && _(zb, Db, _93AC3C19AEE8CE0B))
		{
			M = Db * (1.0 - _93AC3C19AEE8CE0B);
			mb(zb, _93AC3C19AEE8CE0B);

#ifdef _73599E697EB8FFF2
			jb(b);
			yb = uint(_93AC3C19AEE8CE0B * float(0xFFFFFFFu)) << 4u;
			wb();
#else
			jb(c);	
			rb(zb);
			yb = o();
#endif
		}
		else
		{
			N();

			if (Bb == b)
			{
				yb = Ab; 
				xb = 1u; 
			}
			else
			{
				yb = o();
			}
		}
				
		break;
	}

#ifdef _73599E697EB8FFF2
	case b:
	{
		qb();
		float _93AC3C19AEE8CE0B = float(V >> 4u) / float(0xFFFFFFFu);
		M = _B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - _93AC3C19AEE8CE0B);
		rb(zb);

		if ((V & 3u) == a)
		{
			N();
		}
		else
		{
			jb(c);
			xb = min(((V >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		yb = V;

		break;
	}
#endif

	case c:
	{
		qb();
		M = _B29D96F4CC68440F;
		rb(zb);

		if ((V & 3u) == a)
		{
			N();
		}
		else
		{
			xb = min(((V >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		yb = o();
					
		break;
	}

#ifdef _73599E697EB8FFF2
	case d:
	{
		qb();
		Eb();

		if ((V & 3u) == a)
		{
			N();
		}
		else
		{
			xb = min(((V >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		yb = o();
		break;
	}
#endif
	}

	V = (yb & 0xFFFFFFF0u) | (xb << 2u) | (V & 3u);

	_98DBFB22A3024CC1 = V;
	gl_PointSize = 1.0;
}

#endif 
#ifdef _CC5A472CC9FE17BA

out vec4 _4CCF54A3B6372A66;

void main()
{
	_4CCF54A3B6372A66 = vec4(1.01 / 1024.0, 0, 0, 0);
}

#endif

#ifdef _B970C3D2AD57F959

layout (std140) uniform _F52D3B1AA302F088
{
    vec3 Fb;
    float Gb;
    vec3 Hb;
    uint Ib;
    vec3 Jb;
    uint Kb;
    vec3 Lb;
    float Mb;
    mat4 Nb;
};

uniform float _618E8A543B8FC66E;

uniform mat4 _4F2DACEFFBDAB8D1;

vec4 Ob(vec4 Pb)
{
	return _618E8A543B8FC66E < 0.0 ? Pb : vec4(Pb.rgb * _618E8A543B8FC66E, Pb.a);
}

smooth out vec4 _203C8330B731DD85;
flat out uint _94B3AA5995C3B5A1;

vec4 Qb = vec4(0, 0, 1, 1);

uvec2 Rb()
{
	return uvec2(uint(gl_VertexID) / 6u, uint(gl_VertexID) % 6u);
}

smooth out vec2 _BEAD44E72AA5013D;

float[6] Sb = float[6](-1.0,   -1.0,   -1.0,   0.0,   0.0,    0.0);
float[6] Tb = float[6](0.0,    0.0,    -1.0,    0.0,   -1.0,   -1.0);

float[6] Ub = float[6](0.0, 0.0,    0.0,    1.0,    1.0,    1.0);
float[6] Vb = float[6](1.0, 1.0,    0.0,    1.0,    0.0,    0.0);

vec2[6] Wb = vec2[6](
	vec2(0.0, 1.0), vec2(0.0, 1.0), vec2(0.0, 0.0), vec2(1.0, 1.0), vec2(1.0, 0.0), vec2(1.0, 0.0)
	);

void Xb(uint Yb, vec3 Zb, vec2 _b, vec3 ac, vec3 bc, 
    vec4 Pb, vec4 cc)
{
	_203C8330B731DD85 = Ob(Pb);

	vec2 dc = vec2(1, 1) - _b;

	vec2 ec = vec2(Sb[Yb], Tb[Yb]) * _b
		+ vec2(Ub[Yb], Vb[Yb]) * dc;

	vec3 fc = ec.x * ac + ec.y * bc;

    mat4 gc = Nb * _4F2DACEFFBDAB8D1;

	gl_Position = gc * vec4(Zb + fc, 1.0);
	
	_BEAD44E72AA5013D = Wb[Yb] * cc.zw + cc.xy;
}

void hc(uint Yb, vec3 Zb, vec2 _b, float ic, vec2 jc, vec4 Pb, vec4 cc)
{
    vec3 kc = Lb;
    vec3 lc = Hb;

    float s = z(ic);
    float mc = -sin(s);
    float nc = cos(s);

    vec3 ac = vec3(
        kc.x * nc + lc.x * mc,
        kc.y * nc + lc.y * mc,
        kc.z * nc + lc.z * mc);

    vec3 bc = vec3(
        -kc.x * mc + lc.x * nc,
        -kc.y * mc + lc.y * nc,
        -kc.z * mc + lc.z * nc);

    Xb(Yb, Zb, _b, ac * jc.x, bc * jc.y, Pb, cc);
}

void oc()
{
	gl_Position = vec4(-2, -2, 0, 1);
}

void pc(uint qc, uint Yb)
{
	e ab = K(qc - 0u);
	uint cb = ab.f;
	uint db = ab.f + ab.g;

	vec3 Zb = G(A + 0u);
	float Y = B(A + 8u);
	float nb = 30.0;
	_94B3AA5995C3B5A1 = 0u;
	hc(Yb, G(A + 0u), vec2(0.5, 0.5), Y, vec2(nb, nb), vec4(vec3(1.0, 1.0, 1.0), 1.0), Qb);
}

void main() {
	uvec2 rc = Rb();
	uint J = rc.x;
	uint Yb = rc.y;

	uvec2 sc = I(J);
	uint tc = sc.x;
	uint h = sc.y;
	uint uc = D(A + h);
	uint kb = uc & 3u;

	if (kb != c)
	{
		oc();
		return;
	}
	switch (tc)
	{
	case 0u: pc(J, Yb); break;
	}
}

#endif 

#ifdef _DC66F57E96C6C75C

uniform float _618E8A543B8FC66E;

flat in uint _94B3AA5995C3B5A1;
smooth in vec4 _203C8330B731DD85;

out vec4 _EB400D689750048A;

smooth in vec2 _BEAD44E72AA5013D;

vec4 vc(sampler2D wc, mat3x2 xc)
{
	return texture(wc, xc * vec3(_BEAD44E72AA5013D, 1))* _203C8330B731DD85;
}

void makeFragColor(sampler2D wc, mat3x2 xc)
{
	vec4 yc = vc(wc, xc);

#ifdef _6335F91D31E2CC83
	yc.rgb = yc.rgb * _203C8330B731DD85.a;
#endif

	_EB400D689750048A = _618E8A543B8FC66E < 0.0 ?
		vec4(mix(vec3(1, 1, 1), yc.rgb, yc.a), 1) :
		yc;
}

in makeFragColor()

#endif

