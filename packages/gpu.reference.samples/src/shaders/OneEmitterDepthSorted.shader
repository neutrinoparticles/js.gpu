//DESC 1;-1;0.0;10.0;0;1;0;2121;2123;23;22;1;1;4;1;500000;0;-1;-1;-1;6;0;5;Position;3;flags;1;ColorIterp;1;Lifetime;1;Velocity;3;0;1;
//TEXTURES tile.png

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;
precision highp sampler3D;
#define _6A0EA0315208361E 2121u
#define _2CE42649D807BFC4 1u

const uint a = 0u;
const uint b = 1u;
const uint c = 2u;
const uint d = 3u;

uniform sampler3D _5CABCD2CE500BEBB;

struct e
{
	uint f; 
	uint g; 
	uint h; 
	uint i; 
};

#if defined(_9CBA6082CFE05FA3) || defined(_B970C3D2AD57F959) || defined(_8CFBC1D9BA8D6516)

uint j = 0u;
uint k = 0u;
uint l = 0u;

void m(uint n)
{
	n &= 0xFFFFFFFFu;

	j = n < 2u ? n + 2u : n;
	k = n < 8u ? n + 8u : n;
	l = n < 16u ? n + 16u : n;
}

uint o()
{
	uint p = (((j << 13u) ^ j) & 0xFFFFFFFFu) >> 19u;
	j = ((j & 0xFFFFFFFEu) << 12u) ^ p;

	p = (((k << 2u) ^ k) & 0xFFFFFFFFu) >> 25u;
	k = ((k & 0xFFFFFFF8u) << 4u) ^ p;

	p = (((l << 3u) ^ l) & 0xFFFFFFFFu) >> 11u;
	l = ((l & 0xFFFFFFF0u) << 17u) ^ p;

	return (j ^ k) ^ l;
}

float q()
{
	return float(o()) / 4294967296.0;
}

#define _9D02A6055A27F4CF 3.1415926

uint r(uint s, uint p)
{
    return s - p * (s / p);
}

uniform usampler2D _E6B127475CD1AF15;
uniform usampler2D _1AAFF6BB1AC02A82;

uint t = 0u; 

float u(uint v)
{
	return uintBitsToFloat(texelFetch(_E6B127475CD1AF15,
		ivec2(v % _6A0EA0315208361E, v / _6A0EA0315208361E), 0).r);
}

uint w(uint v)
{
	return texelFetch(_E6B127475CD1AF15,
		ivec2(v % _6A0EA0315208361E, v / _6A0EA0315208361E), 0).r;
}

float x(uint v)
{
	return uintBitsToFloat(texelFetch(_1AAFF6BB1AC02A82,
		ivec2(v % _6A0EA0315208361E, v / _6A0EA0315208361E), 0).r);
}

uint y(uint v)
{
	return texelFetch(_1AAFF6BB1AC02A82,
		ivec2(v % _6A0EA0315208361E, v / _6A0EA0315208361E), 0).r;
}

vec3 z(uint v)
{
	return vec3(u(v), u(v + 1u), u(v + 2u));
}

vec3 A(uint v)
{
	return vec3(x(v), x(v + 1u), x(v + 2u));
}

uvec2 B(uint C)
{
	t = 2121u + C * 9u;
	return uvec2(0u, 3u); 
}

e D(uint E)
{
	return e(0u, 4u, 3u, r(E - 0u, 500000u)); 
}

#endif

#ifdef _9CBA6082CFE05FA3

uniform sampler2D _17DCC16C9305C31F;
uniform float _0D6ECD3D4CD5F8C7;
uniform float _B29D96F4CC68440F;
uniform uint _600D80823B393136;
uniform uint _A62B4010E834E4BC;
uniform uint _8B5E47AC853BBD76;

float F;

void G()
{
	gl_Position = vec4(-2.0, -2.0, 0, 1);
}

uint H(uvec2 I)
{
	return uint(texelFetch(_17DCC16C9305C31F, ivec2(I), 0).r * 1023.0);
}

uint H(uint J)
{
	return H(uvec2(J % 23u, J / 23u));
}

uvec2 K;

#if (_53986AB86EF3B727 == 0) 

#define _83AB904282DA95AA 0u

void L()
{
	uint J;
	uint E = uint(gl_VertexID);

	J = 0u + (E) / 500000u * 489u + (E) % 500000u % 489u;

	K = uvec2(J % 23u, J / 23u);
	gl_Position = vec4((float(K.x) + 0.5) / float(23u) * 2.0 - 1.0, (float(K.y) + 0.5) / float(22u) * 2.0 - 1.0,  0, 1);
}

uint M()
{
	return c;
}

vec3 N;
uint O;
float P;
float Q;
vec3 R;

flat out vec3 _C09D6F3C5A9B9FBA;
flat out uint _98DBFB22A3024CC1;
flat out float _9E69C311C9EE800E;
flat out float _AF547B67E173F200;
flat out vec3 _BA00525347033428;

uint C()
{
	return uint(gl_VertexID);
}

void S(uint C)
{
	m((0xFFFFFFFFu / 500000u * C) ^ _600D80823B393136 ^ O);
}

bool T(e U, float V, out float _93AC3C19AEE8CE0B)
{
	uint W = U.f;
	uint X = U.f + U.g;

	uint Y = H(K);
	uint Z = 1022u + ((uint(gl_VertexID) % 489u) < 242u ? 1u : 0u); 
	float _ = float(489u) * (float(Z) - float(Y));
	float ab = V / _;
	float bb = q();

	if (bb < (ab * (50.0 * 5000.0))) { _93AC3C19AEE8CE0B = q(); return true; } 
	return false;
}

void cb(uint db)
{
	O = (O & 0xFFFFFFFCu) | db;
}

e eb()
{
	return D(uint(gl_VertexID));
}

void fb(e U, float _93AC3C19AEE8CE0B)
{
	uint W = U.f;
	uint X = U.f + U.g;

	float gb = 0.0 + q() * (1.0 - 0.0);
	P = gb;
	Q = 0.0;
	float hb = -500.0 + q() * (500.0 - -500.0);
	float ib = -500.0 + q() * (500.0 - -500.0);
	float jb = -500.0 + q() * (500.0 - -500.0);
	vec3  kb = vec3(hb, ib, jb);
	N = kb;
	vec3 lb = mix(
		A(W + 0u), 
		z(W + 0u),
		_93AC3C19AEE8CE0B);
	N += lb;
	float mb = -1.0 + q() * (1.0 - -1.0);
	float nb = -1.0 + q() * (1.0 - -1.0);
	float ob = -1.0 + q() * (1.0 - -1.0);
	vec3  pb = vec3(mb, nb, ob);
	vec3 qb = (normalize(pb) * 2.5);
	R = qb;
}

void rb()
{
	F = _B29D96F4CC68440F;

	N = A(t + 0u);
	P = x(t + 4u);
	Q = x(t + 5u);
	R = A(t + 6u);
}

void sb(e U)
{
	uint W = U.f;
	uint X = U.f + U.g;

	_9E69C311C9EE800E = P;
	_AF547B67E173F200 = Q + F; 
	_C09D6F3C5A9B9FBA = N;
	_BA00525347033428 = R;
	vec3 tb = _C09D6F3C5A9B9FBA + _BA00525347033428 * F;
	_C09D6F3C5A9B9FBA = tb;
	float kb = 2.0;
	if (_AF547B67E173F200 >= kb)
		cb(a);
}

void ub()
{
	_C09D6F3C5A9B9FBA = N; 
	_98DBFB22A3024CC1 = O; 
	_9E69C311C9EE800E = P; 
	_AF547B67E173F200 = Q; 
	_BA00525347033428 = R; 
}

#endif

void main() {
	uint C = _83AB904282DA95AA + uint(gl_VertexID);
	uint h = B(C).y;

	O = y(t + h);
	uint db = O & 3u;

	L();

	S(C);

	uint vb = 0u;
	uint wb;

	e xb = eb();

	switch (db)
	{
	case a:
	{
		uint yb = w(xb.f + xb.h);
		uint zb = (yb & 3u);
		uint Ab = ((O >> 2u) & 3u); 

		float Bb = (Ab != 0u ? 
			_B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - float((O >> 4u) + 1u) / float(0xFFFFFFFu)) : 
			_B29D96F4CC68440F
		);

float _93AC3C19AEE8CE0B = 0.f;

		if (zb == M() && T(xb, Bb, _93AC3C19AEE8CE0B))
		{
			F = Bb * (1.0 - _93AC3C19AEE8CE0B);
			fb(xb, _93AC3C19AEE8CE0B);

#ifdef _73599E697EB8FFF2
			cb(b);
			wb = uint(_93AC3C19AEE8CE0B * float(0xFFFFFFFu)) << 4u;
			ub();
#else
			cb(c);	
			sb(xb);
			wb = o();
#endif
		}
		else
		{
			G();

			if (zb == b)
			{
				wb = yb; 
				vb = 1u; 
			}
			else
			{
				wb = o();
			}
		}
				
		break;
	}

#ifdef _73599E697EB8FFF2
	case b:
	{
		rb();
		float _93AC3C19AEE8CE0B = float(O >> 4u) / float(0xFFFFFFFu);
		F = _B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - _93AC3C19AEE8CE0B);
		sb(xb);

		if ((O & 3u) == a)
		{
			G();
		}
		else
		{
			cb(c);
			vb = min(((O >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		wb = O;

		break;
	}
#endif

	case c:
	{
		rb();
		F = _B29D96F4CC68440F;
		sb(xb);

		if ((O & 3u) == a)
		{
			G();
		}
		else
		{
			vb = min(((O >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		wb = o();
					
		break;
	}

#ifdef _73599E697EB8FFF2
	case d:
	{
		rb();
		Cb();

		if ((O & 3u) == a)
		{
			G();
		}
		else
		{
			vb = min(((O >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		wb = o();
		break;
	}
#endif
	}

	O = (wb & 0xFFFFFFF0u) | (vb << 2u) | (O & 3u);

	_98DBFB22A3024CC1 = O;
	gl_PointSize = 1.0;
}

#endif 
#ifdef _CC5A472CC9FE17BA

out vec4 _4CCF54A3B6372A66;

void main()
{
	_4CCF54A3B6372A66 = vec4(1.01 / 1024.0, 0, 0, 0);
}

#endif

#ifdef _8CFBC1D9BA8D6516

uniform sampler2D _50099FB73CB57A9E; 

uniform float _30B9E4384BFDB31C;
uniform vec3 _953A2A72B2E8B0E9;
uniform uint _14DB390EE2887C41;

flat out float _9BC2E3181A315B34;

flat out uint _5A57BFB62CAEDE57;

float Db;
uint Eb;

float Fb(float Gb)
{
	return Gb * 0.5 + 0.5;
}
float Hb(float Gb)
{
	return (Gb - 0.5) * 2.0;
}

float Ib(float Gb)
{
	return Fb(log2(abs(Gb) + 1.0) / 32.0 * sign(Gb));
}

float Jb(float Gb)
{
	return (exp2(abs(Hb(Gb)) * 32.0) - 1.0) * sign(Gb);
}

const uint Kb = 1024u;

bool Lb(uint C)
{
	uint h = B(C).y;
	uint Mb = w(t + h);
	uint db = Mb & 3u;

	if (db != c)
	{
		return false;
	}

	float Nb = Jb(texelFetch(_50099FB73CB57A9E, ivec2(0, 0), 0).r);
	vec2 Ob = vec2(0, Nb);

vec3 Pb = z(t + 0u);

	Db = dot(Pb, _953A2A72B2E8B0E9) + _30B9E4384BFDB31C;

	float Qb = clamp((Db - Ob.x) / (Ob.y - Ob.x), 0.0, 1.0);
	Eb = uint(Qb * float(Kb - 1u));

	return true;
}

void main() {

	float Rb = 3.402823466e+38;
	float Sb = -3.402823466e+38;

	uint Tb;

	uint Ub = 1u;

	if (Lb(uint(gl_VertexID) * 2u))
	{
		Rb = Db;
		Sb = Db;
		Tb = Eb;
		Ub = 0u;
	}
	else
	{
		Tb = Kb;
	}

	uint Vb = uint(gl_VertexID) * 2u + 1u;
	if (Vb < _14DB390EE2887C41)
	{
		if (Lb(Vb))
		{
			Rb = min(Rb, Db);
			Sb = max(Sb, Db);
			Tb |= (Eb << 16);
			Ub = 0u;
		}
		else
		{
			Tb |= (Kb << 16);
		}
	}

	_5A57BFB62CAEDE57 = Tb;

	_9BC2E3181A315B34 = Ib(Sb);

	gl_Position = vec4(-2.0 * float(Ub), 0.0, 0.0, 1.0);
	gl_PointSize = 1.0;
}

#endif

#ifdef _A9EE343B6D5FED1F

flat in float _9BC2E3181A315B34;

out vec4 _EB400D689750048A;

void main()
{
	_EB400D689750048A = vec4(0, 0, 0, 0);
	gl_FragDepth = _9BC2E3181A315B34;
}

#endif

#ifdef _B970C3D2AD57F959

layout (std140) uniform _F52D3B1AA302F088
{
    vec3 Wb;
    float Xb;
    vec3 Yb;
    uint Zb;
    vec3 _953A2A72B2E8B0E9;
    uint _b;
    vec3 ac;
    float bc;
    mat4 cc;
};

uniform float _618E8A543B8FC66E;

uniform mat4 _4F2DACEFFBDAB8D1;

vec4 dc(vec4 ec)
{
	return _618E8A543B8FC66E < 0.0 ? ec : vec4(ec.rgb * _618E8A543B8FC66E, ec.a);
}

smooth out vec4 _203C8330B731DD85;
flat out uint _94B3AA5995C3B5A1;

vec4 fc = vec4(0, 0, 1, 1);

uniform uint _94426D1BFF511BB0;

in uint _4D7C1BF9E36F0C6C;

uvec2 gc()
{
	return uvec2(_4D7C1BF9E36F0C6C >> 8, _4D7C1BF9E36F0C6C & 0xFFu);
}

flat out vec4 _BDF6B55D84170610;

void hc(uint ic, vec3 Pb, vec2 jc, vec2 kc, vec4 ec, vec4 lc)
{
	vec3 mc = cross(Yb, _953A2A72B2E8B0E9);
	vec3 nc = Yb;

	_203C8330B731DD85 = dc(ec);
	_BDF6B55D84170610 = lc;

	float oc = kc.x;

	Pb -= (mc * (jc.x - 0.5) + nc * (jc.y - 0.5)) * oc;

	mat4 pc = cc * _4F2DACEFFBDAB8D1;

	vec4 qc = pc * vec4(Pb +
		Yb * oc * 0.5, 1.0);

	vec4 rc = pc * vec4(Pb, 1.0);
	gl_PointSize = abs(qc.y / qc.w - rc.y / rc.w) *
		float(_b);
	gl_Position = rc;
}

void sc()
{
	gl_PointSize = 1.0;
	gl_Position = vec4(-2.0, -2.0, 0.0, 1.0);
}

void tc(uint uc, uint ic)
{
	e U = D(uc - 0u);
	uint W = U.f;
	uint X = U.f + U.g;

	vec3 Pb = z(t + 0u);
	float kb = 1.25;
	float P = u(t + 4u);
	vec3 qb = mix(vec3(1.0, 1.0, 0.0), vec3(1.0, 0.0, 0.0), P);
	_94B3AA5995C3B5A1 = 0u;
	hc(ic, z(t + 0u), vec2(0.5, 0.5), vec2(kb, kb), vec4(qb, 1.0), fc);
}

void main() {
	uvec2 vc = gc();
	uint C = vc.x;
	uint ic = vc.y;

	uvec2 wc = B(C);
	uint xc = wc.x;
	uint h = wc.y;
	uint Mb = w(t + h);
	uint db = Mb & 3u;

	if (db != c)
	{
		sc();
		return;
	}

	if (((Mb >> 2u) & 3u) < _94426D1BFF511BB0)
	{
		sc();
		return;
	}
	switch (xc)
	{
	case 0u: tc(C, ic); break;
	}
}

#endif 

#ifdef _DC66F57E96C6C75C

uniform float _618E8A543B8FC66E;

flat in uint _94B3AA5995C3B5A1;
smooth in vec4 _203C8330B731DD85;

out vec4 _EB400D689750048A;

flat in vec4 _BDF6B55D84170610;

vec4 yc(sampler2D zc, mat3x2 Ac)
{
	vec2 Bc = vec2(gl_PointCoord.x, 1.0 - gl_PointCoord.y);
	return texture(zc, 
		Ac * vec3(_BDF6B55D84170610.xy + Bc * _BDF6B55D84170610.zw, 1)) * _203C8330B731DD85;
}

void makeFragColor(sampler2D zc, mat3x2 Ac)
{
	vec4 Cc = yc(zc, Ac);
#ifdef _6335F91D31E2CC83
	Cc.rgb = Cc.rgb * _203C8330B731DD85.a;
#endif

	_EB400D689750048A = _618E8A543B8FC66E < 0.0 ?
		vec4(mix(vec3(1, 1, 1), Cc.rgb, Cc.a), 1) :
		Cc;
}

in makeFragColor()

#endif

