//DESC 1;30;0.0;10.0;0;0;0;374;376;4;3;1;1;4;1;10000;0;-1;-1;0;6;0;8;Position;3;flags;1;Lifetime;1;Max0Life;1;Size;1;Color;3;Velocity;3;PulsingAlpha;1;0;0;
//TEXTURES spark_white.png

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;
precision highp sampler3D;
#define _6A0EA0315208361E 374u
#define _2CE42649D807BFC4 1u

const uint a = 0u;
const uint b = 1u;
const uint c = 2u;
const uint d = 3u;

uniform sampler3D _5CABCD2CE500BEBB;

struct e
{
	uint f; 
	uint g; 
	uint h; 
	uint i; 
};

#if defined(_9CBA6082CFE05FA3) || defined(_B970C3D2AD57F959) || defined(_8CFBC1D9BA8D6516)

uint j = 0u;
uint k = 0u;
uint l = 0u;

void m(uint n)
{
	n &= 0xFFFFFFFFu;

	j = n < 2u ? n + 2u : n;
	k = n < 8u ? n + 8u : n;
	l = n < 16u ? n + 16u : n;
}

uint o()
{
	uint p = (((j << 13u) ^ j) & 0xFFFFFFFFu) >> 19u;
	j = ((j & 0xFFFFFFFEu) << 12u) ^ p;

	p = (((k << 2u) ^ k) & 0xFFFFFFFFu) >> 25u;
	k = ((k & 0xFFFFFFF8u) << 4u) ^ p;

	p = (((l << 3u) ^ l) & 0xFFFFFFFFu) >> 11u;
	l = ((l & 0xFFFFFFF0u) << 17u) ^ p;

	return (j ^ k) ^ l;
}

float q()
{
	return float(o()) / 4294967296.0;
}

#define _9D02A6055A27F4CF 3.1415926

uint r(uint s, uint p)
{
    return s - p * (s / p);
}

vec3 t(float u)
{
	vec3 v;
	float w = q() * _9D02A6055A27F4CF * 2.0;
	float x = -1.0 + q() * 2.0;
	float y = u * sqrt(1.0 - x * x);
	v.x = y * cos(w);
	v.y = y * sin(w);
	v.z = u * x;
	return v;
}

vec4 z(vec3 A, vec3 B, vec3 x)
{
    vec4 C;
    float D = A.x + B.y + x.z;

    if (D > 0.0)
    {
        float E = sqrt(D + 1.0) * 2.0;
        C.w = 0.25 * E;
        C.x = (B.z - x.y) / E;
        C.y = (x.x - A.z) / E;
        C.z = (A.y - B.x) / E;
    }
    else if ((A.x > B.y) && (A.x > x.z))
    {
        float E = sqrt(1.0 + A.x - B.y - x.z) * 2.0;
        C.w = (B.z - x.y) / E;
        C.x = 0.25 * E;
        C.y = (A.y + B.x) / E;
        C.z = (x.x + A.z) / E;
    }
    else if (B.y > x.z)
    {
        float E = sqrt(1.0 + B.y - A.x - x.z) * 2.0;
        C.w = (x.x - A.z) / E;
        C.x = (A.y + B.x) / E;
        C.y = 0.25 * E;
        C.z = (B.z + x.y) / E;
    }
    else
    {
        float E = sqrt(1.0 + x.z - A.x - B.y) * 2.0;
        C.w = (A.y - B.x) / E;
        C.x = (x.x + A.z) / E;
        C.y = (B.z + x.y) / E;
        C.z = 0.25 * E;
    }

    return C;
}

uniform usampler2D _E6B127475CD1AF15;
uniform usampler2D _1AAFF6BB1AC02A82;

uint F = 0u; 

float G(uint H)
{
	return uintBitsToFloat(texelFetch(_E6B127475CD1AF15,
		ivec2(H % _6A0EA0315208361E, H / _6A0EA0315208361E), 0).r);
}

uint I(uint H)
{
	return texelFetch(_E6B127475CD1AF15,
		ivec2(H % _6A0EA0315208361E, H / _6A0EA0315208361E), 0).r;
}

float J(uint H)
{
	return uintBitsToFloat(texelFetch(_1AAFF6BB1AC02A82,
		ivec2(H % _6A0EA0315208361E, H / _6A0EA0315208361E), 0).r);
}

uint K(uint H)
{
	return texelFetch(_1AAFF6BB1AC02A82,
		ivec2(H % _6A0EA0315208361E, H / _6A0EA0315208361E), 0).r;
}

vec3 L(uint H)
{
	return vec3(G(H), G(H + 1u), G(H + 2u));
}

vec3 M(uint H)
{
	return vec3(J(H), J(H + 1u), J(H + 2u));
}

uvec2 N(uint O)
{
	F = 374u + O * 14u;
	return uvec2(0u, 3u); 
}

e P(uint Q)
{
	return e(0u, 4u, 3u, r(Q - 0u, 10000u)); 
}

#endif

#ifdef _9CBA6082CFE05FA3

uniform sampler2D _17DCC16C9305C31F;
uniform float _0D6ECD3D4CD5F8C7;
uniform float _B29D96F4CC68440F;
uniform uint _600D80823B393136;
uniform uint _A62B4010E834E4BC;
uniform uint _8B5E47AC853BBD76;

float R;

void S()
{
	gl_Position = vec4(-2.0, -2.0, 0, 1);
}

uint T(uvec2 U)
{
	return uint(texelFetch(_17DCC16C9305C31F, ivec2(U), 0).r * 1023.0);
}

uniform float _099D929DBDB01227;

uint T(uint V)
{
	return T(uvec2(V % 4u, V / 4u));
}

uvec2 W;

#if (_53986AB86EF3B727 == 0) 

#define _83AB904282DA95AA 0u

void X()
{
	uint V;
	uint Q = uint(gl_VertexID);

	V = 0u + (Q) / 10000u * 10u + (Q) % 10000u % 10u;

	W = uvec2(V % 4u, V / 4u);
	gl_Position = vec4((float(W.x) + 0.5) / float(4u) * 2.0 - 1.0, (float(W.y) + 0.5) / float(3u) * 2.0 - 1.0,  0, 1);
}

uint Y()
{
	return c;
}

vec3 Z;
uint _;
float ab;
float bb;
float cb;
vec3 db;
vec3 eb;
float fb;

flat out vec3 _C09D6F3C5A9B9FBA;
flat out uint _98DBFB22A3024CC1;
flat out float _AF547B67E173F200;
flat out float _01240EE9092A917F;
flat out float _B041C3644577F7CA;
flat out vec3 _203C8330B731DD85;
flat out vec3 _BA00525347033428;
flat out float _9381A221E3535D10;

uint O()
{
	return uint(gl_VertexID);
}

void gb(uint O)
{
	m((0xFFFFFFFFu / 10000u * O) ^ _600D80823B393136 ^ _);
}

bool hb(e ib, float jb, out float _93AC3C19AEE8CE0B)
{
	uint kb = ib.f;
	uint lb = ib.f + ib.g;

	uint mb = T(W);
	uint nb = 1000u + ((uint(gl_VertexID) % 10u) < 0u ? 1u : 0u); 
	float ob = float(10u) * (float(nb) - float(mb));
	float pb = jb / ob;
	float qb = q();

	if (qb < (pb * (370.0 * 100.0))) { _93AC3C19AEE8CE0B = q(); return true; } 
	return false;
}

void rb(uint sb)
{
	_ = (_ & 0xFFFFFFFCu) | sb;
}

e tb()
{
	return P(uint(gl_VertexID));
}

void ub(e ib, float _93AC3C19AEE8CE0B)
{
	uint kb = ib.f;
	uint lb = ib.f + ib.g;

	ab = 0.0;
	float vb = 0.0 + q() * (1.0 - 0.0);
	float wb;
	float[17] xb = float[17](0.236236,0.236626,0.237009,0.237439,0.237991,0.238768,0.239915,0.241637,0.24424,0.248194,0.254262,0.263774,0.279323,0.306915,0.366216,0.829032,0.829032);
	float yb = (vb < 0.0 ? 0.0:(vb>1.0?1.0:vb));
	float zb = 0.0+(yb - 0.0) * 15.0;
	wb = mix(xb[int(zb)], xb[int(zb) + 1], fract(zb));
	bb = wb;
	float Ab = 0.5 + q() * (1.5 - 0.5);
	cb = Ab;
	float Bb = 0.0 + q() * (1.0 - 0.0);
	vec3 Cb;
	float[3] Db = float[3](1.0,1.0,1.0);
	float Eb = (Bb < 0.0 ? 0.0:(Bb>1.0?1.0:Bb));
	float Fb = 0.0+(Eb - 0.0) * 1.0;
	Cb.x = mix(Db[int(Fb)], Db[int(Fb) + 1], fract(Fb));
	float[3] Gb = float[3](0.714092,0.577592,0.577592);
	float Hb = (Bb < 0.0 ? 0.0:(Bb>1.0?1.0:Bb));
	float Ib = 0.0+(Hb - 0.0) * 1.0;
	Cb.y = mix(Gb[int(Ib)], Gb[int(Ib) + 1], fract(Ib));
	float[3] Jb = float[3](0.387158,0.387158,0.387158);
	float Kb = (Bb < 0.0 ? 0.0:(Bb>1.0?1.0:Bb));
	float Lb = 0.0+(Kb - 0.0) * 1.0;
	Cb.z = mix(Jb[int(Lb)], Jb[int(Lb) + 1], fract(Lb));
	db = Cb;
	float Mb = 0.0 + q() * (1.0 - 0.0);
	vec2[64] Nb = vec2[64](vec2(-299.311, -0.553781),vec2(-298.469, 31.0931),vec2(-294.667, 62.5188),vec2(-287.744, 93.4066),vec2(-277.614, 123.395),vec2(-264.281, 152.101),vec2(-247.845, 179.15),vec2(-228.495, 204.198),vec2(-206.489, 226.948),vec2(-182.128, 247.157),vec2(-155.736, 264.633),vec2(-127.645, 279.221),vec2(-98.1829, 290.794),vec2(-67.6771, 299.241),vec2(-36.4563, 304.456),vec2(-4.85954, 306.339),vec2(-4.85964, 306.339),vec2(26.9412, 304.822),vec2(58.3983, 299.919),vec2(89.1584, 291.708),vec2(118.886, 280.312),vec2(147.268, 265.887),vec2(174.011, 248.61),vec2(198.84, 228.681),vec2(221.495, 206.311),vec2(241.732, 181.733),vec2(259.325, 155.197),vec2(274.072, 126.981),vec2(285.807, 97.3846),vec2(294.409, 66.7307),vec2(299.816, 35.3556),vec2(302.033, 3.59351),vec2(302.033, 3.59342),vec2(300.829, -27.5544),vec2(295.944, -58.3396),vec2(287.44, -88.3274),vec2(275.477, -117.112),vec2(260.294, -144.336),vec2(242.174, -169.702),vec2(221.426, -192.969),vec2(198.36, -213.941),vec2(173.282, -232.462),vec2(146.491, -248.406),vec2(118.276, -261.668),vec2(88.921, -272.17),vec2(58.7047, -279.851),vec2(27.9034, -284.68),vec2(-3.2125, -286.652),vec2(-3.21244, -286.652),vec2(-33.3264, -285.221),vec2(-63.0512, -280.162),vec2(-92.0464, -271.879),vec2(-120.059, -260.709),vec2(-146.879, -246.919),vec2(-172.315, -230.717),vec2(-196.172, -212.266),vec2(-218.233, -191.704),vec2(-238.258, -169.156),vec2(-255.975, -144.753),vec2(-271.086, -118.657),vec2(-283.279, -91.079),vec2(-292.261, -62.2961),vec2(-297.793, -32.6574),vec2(-299.737, -2.56887));
	vec2[64] Ob = vec2[64](vec2(0.0265855, 0.999646),vec2(0.120111, 0.992761),vec2(0.218703, 0.975791),vec2(0.320042, 0.947404),vec2(0.42125, 0.906944),vec2(0.519278, 0.854606),vec2(0.61134, 0.791368),vec2(0.695266, 0.718752),vec2(0.769648, 0.638468),vec2(0.833782, 0.552093),vec2(0.887468, 0.46087),vec2(0.930759, 0.365632),vec2(0.963737, 0.266852),vec2(0.986334, 0.164755),vec2(0.998229, 0.0594938),vec2(0.998229, 0.0594938),vec2(0.998863, -0.0476671),vec2(0.988072, -0.153993),vec2(0.966173, -0.257896),vec2(0.933743, -0.357944),vec2(0.891459, -0.453101),vec2(0.839971, -0.542631),vec2(0.779848, -0.62597),vec2(0.711575, -0.70261),vec2(0.635631, -0.771994),vec2(0.552579, -0.83346),vec2(0.463195, -0.886256),vec2(0.368572, -0.929599),vec2(0.27018, -0.96281),vec2(0.169847, -0.98547),vec2(0.0696349, -0.997573),vec2(0.0696349, -0.997573),vec2(-0.0386435, -0.999253),vec2(-0.156702, -0.987646),vec2(-0.272839, -0.96206),vec2(-0.383776, -0.923426),vec2(-0.487082, -0.873356),vec2(-0.581245, -0.813729),vec2(-0.665564, -0.746341),vec2(-0.739894, -0.672723),vec2(-0.804398, -0.594091),vec2(-0.859345, -0.511396),vec2(-0.905002, -0.425407),vec2(-0.941566, -0.336829),vec2(-0.969173, -0.246383),vec2(-0.987933, -0.154879),vec2(-0.997997, -0.0632573),vec2(-0.997997, -0.0632573),vec2(-0.998873, 0.0474629),vec2(-0.98582, 0.167804),vec2(-0.961538, 0.274673),vec2(-0.928877, 0.370388),vec2(-0.889331, 0.457264),vec2(-0.843429, 0.53724),vec2(-0.791029, 0.611779),vec2(-0.731528, 0.681812),vec2(-0.664027, 0.747709),vec2(-0.587511, 0.809216),vec2(-0.501094, 0.865393),vec2(-0.404379, 0.914592),vec2(-0.297892, 0.954599),vec2(-0.183487, 0.983022),vec2(-0.0644789, 0.997919),vec2(-0.0644789, 0.997919));
	float Pb = clamp(Mb, 0.0, 1.0);
	float Qb = Pb<0.50866?Pb<0.253591?0.0+(Pb - 0.0) * 59.1504:16.0+(Pb - 0.253591) * 58.8076:Pb<0.758415?32.0+(Pb - 0.50866) * 60.0589:48.0+(Pb - 0.758415) * 62.0899;
	vec2 Rb = mix(Nb[int(Qb)], Nb[int(Qb) + 1], fract(Qb));
	vec2 Sb = mix(Ob[int(Qb)], Ob[int(Qb) + 1], fract(Qb));
	vec3 Tb = vec3(Rb, 0.0);
	float Ub = 0.0 + q() * (1.0 - 0.0);
	float Vb;
	float[3] Wb = float[3](0.0,40.0,40.0);
	float Xb = (Ub < 0.0 ? 0.0:(Ub>1.0?1.0:Ub));
	float Yb = 0.0+(Xb - 0.0) * 1.0;
	Vb = mix(Wb[int(Yb)], Wb[int(Yb) + 1], fract(Yb));
	vec3 Zb = t(Vb);
	vec3 _b = (Tb + (Zb * 1.5));
	Z = _b;
	vec3 ac = mix(
		M(kb + 0u), 
		L(kb + 0u),
		_93AC3C19AEE8CE0B);
	Z += ac;
	vec3 bc = vec3(Sb, 0.0);
	float cc = 0.0 + q() * (1.0 - 0.0);
	float dc;
	float[17] ec = float[17](690.259,755.478,832.895,923.931,1030.16,1153.35,1295.49,1458.88,1646.17,1860.5,2105.6,2386.01,2707.36,3076.78,3503.5,4000.0,4000.0);
	float fc = (cc < 0.0 ? 0.0:(cc>1.0?1.0:cc));
	float gc = 0.0+(fc - 0.0) * 15.0;
	dc = mix(ec[int(gc)], ec[int(gc) + 1], fract(gc));
	vec3 hc = (bc * dc);
	eb = hc;
	float ic = mix(
		J(lb + 0u), 
		G(lb + 0u),
		_93AC3C19AEE8CE0B);
	float jc = (Mb - ic);
	float kc;
	float[12] lc = float[12](0.5,1.0,1.0,0.5,1.0,1.0,0.5,1.0,1.0,0.5,1.0,1.0);
	float mc = (jc < 0.0 ? (1.0-(mod(0.0-jc, 1.0))):(jc>1.0?(0.0+(mod(jc-0.0,1.0))):jc));
	float nc = mc<0.5?mc<0.2?0.0+(mc - 0.0) * 5.0:3.0+(mc - 0.2) * 3.33333:mc<0.8?6.0+(mc - 0.5) * 3.33333:9.0+(mc - 0.8) * 5.0;
	kc = mix(lc[int(nc)], lc[int(nc) + 1], fract(nc));
	float oc = kc;
	fb = oc;
}

void pc()
{
	R = _B29D96F4CC68440F;

	Z = M(F + 0u);
	ab = J(F + 4u);
	bb = J(F + 5u);
	cb = J(F + 6u);
	db = M(F + 7u);
	eb = M(F + 10u);
	fb = J(F + 13u);
}

void qc(e ib)
{
	uint kb = ib.f;
	uint lb = ib.f + ib.g;

	_AF547B67E173F200 = ab + R; 
	_01240EE9092A917F = bb;
	_B041C3644577F7CA = cb;
	_203C8330B731DD85 = db;
	_C09D6F3C5A9B9FBA = Z;
	_BA00525347033428 = eb;
	vec3 rc = (vec3(100.0, 150.0, 200.0) * _099D929DBDB01227 + _C09D6F3C5A9B9FBA) / 500.0; 
	vec3 noise = (texture(_5CABCD2CE500BEBB, rc).xyz * vec3(2.0, 2.0, 2.0) + vec3(-1.0, -1.0, -1.0)) * 4000.0;
	vec3 sc = noise;
	sc += vec3(0.0, -4000.0, 0.0);
	vec3 tc = vec3(0.0, 0.0, 0.0);
	float uc = R;
	vec3 vc = _BA00525347033428;
	vec3 wc = _C09D6F3C5A9B9FBA;
	while (uc > 0.0001F) {
		float xc = uc;
		vec3 yc = sc;
		vec3 zc = tc - vc;
		float Ac = dot(zc, zc);
		if (Ac > 0.000001) {
			Ac = sqrt(Ac);
			vec3 Bc = zc / Ac;
			float Cc = 0.01 * 0.2 * Ac;
			if (Cc * xc > 0.2)
				xc = 0.2 / Cc;
			yc = yc + (Bc * (Ac * Cc));
		}
		vc = vc + (yc * xc);
		wc = wc + (vc * xc);
		uc -= xc;
	}
	_C09D6F3C5A9B9FBA = wc;
	_BA00525347033428 = vc;
	_9381A221E3535D10 = fb;
	if (_AF547B67E173F200 >= _01240EE9092A917F)
		rb(a);
}

void Dc()
{
	_C09D6F3C5A9B9FBA = Z; 
	_98DBFB22A3024CC1 = _; 
	_AF547B67E173F200 = ab; 
	_01240EE9092A917F = bb; 
	_B041C3644577F7CA = cb; 
	_203C8330B731DD85 = db; 
	_BA00525347033428 = eb; 
	_9381A221E3535D10 = fb; 
}

#endif

void main() {
	uint O = _83AB904282DA95AA + uint(gl_VertexID);
	uint h = N(O).y;

	_ = K(F + h);
	uint sb = _ & 3u;

	X();

	gb(O);

	uint Ec = 0u;
	uint Fc;

	e Gc = tb();

	switch (sb)
	{
	case a:
	{
		uint Hc = I(Gc.f + Gc.h);
		uint Ic = (Hc & 3u);
		uint Jc = ((_ >> 2u) & 3u); 

		float Kc = (Jc != 0u ? 
			_B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - float((_ >> 4u) + 1u) / float(0xFFFFFFFu)) : 
			_B29D96F4CC68440F
		);

float _93AC3C19AEE8CE0B = 0.f;

		if (Ic == Y() && hb(Gc, Kc, _93AC3C19AEE8CE0B))
		{
			R = Kc * (1.0 - _93AC3C19AEE8CE0B);
			ub(Gc, _93AC3C19AEE8CE0B);

#ifdef _73599E697EB8FFF2
			rb(b);
			Fc = uint(_93AC3C19AEE8CE0B * float(0xFFFFFFFu)) << 4u;
			Dc();
#else
			rb(c);	
			qc(Gc);
			Fc = o();
#endif
		}
		else
		{
			S();

			if (Ic == b)
			{
				Fc = Hc; 
				Ec = 1u; 
			}
			else
			{
				Fc = o();
			}
		}
				
		break;
	}

#ifdef _73599E697EB8FFF2
	case b:
	{
		pc();
		float _93AC3C19AEE8CE0B = float(_ >> 4u) / float(0xFFFFFFFu);
		R = _B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - _93AC3C19AEE8CE0B);
		qc(Gc);

		if ((_ & 3u) == a)
		{
			S();
		}
		else
		{
			rb(c);
			Ec = min(((_ >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		Fc = _;

		break;
	}
#endif

	case c:
	{
		pc();
		R = _B29D96F4CC68440F;
		qc(Gc);

		if ((_ & 3u) == a)
		{
			S();
		}
		else
		{
			Ec = min(((_ >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		Fc = o();
					
		break;
	}

#ifdef _73599E697EB8FFF2
	case d:
	{
		pc();
		Lc();

		if ((_ & 3u) == a)
		{
			S();
		}
		else
		{
			Ec = min(((_ >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		Fc = o();
		break;
	}
#endif
	}

	_ = (Fc & 0xFFFFFFF0u) | (Ec << 2u) | (_ & 3u);

	_98DBFB22A3024CC1 = _;
	gl_PointSize = 1.0;
}

#endif 
#ifdef _CC5A472CC9FE17BA

out vec4 _4CCF54A3B6372A66;

void main()
{
	_4CCF54A3B6372A66 = vec4(1.01 / 1024.0, 0, 0, 0);
}

#endif

#ifdef _B970C3D2AD57F959

layout (std140) uniform _F52D3B1AA302F088
{
    vec3 Mc;
    float Nc;
    vec3 Oc;
    uint Pc;
    vec3 Qc;
    uint Rc;
    vec3 Sc;
    float Tc;
    mat4 Uc;
};

uniform float _618E8A543B8FC66E;

uniform mat4 _4F2DACEFFBDAB8D1;

vec4 Vc(vec4 Wc)
{
	return _618E8A543B8FC66E < 0.0 ? Wc : vec4(Wc.rgb * _618E8A543B8FC66E, Wc.a);
}

smooth out vec4 _203C8330B731DD85;
flat out uint _94B3AA5995C3B5A1;

vec4 Xc = vec4(0, 0, 1, 1);

uvec2 Yc()
{
	return uvec2(uint(gl_VertexID) / 6u, uint(gl_VertexID) % 6u);
}

smooth out vec2 _BEAD44E72AA5013D;

float[6] Zc = float[6](-1.0,   -1.0,   -1.0,   0.0,   0.0,    0.0);
float[6] _c = float[6](0.0,    0.0,    -1.0,    0.0,   -1.0,   -1.0);

float[6] ad = float[6](0.0, 0.0,    0.0,    1.0,    1.0,    1.0);
float[6] bd = float[6](1.0, 1.0,    0.0,    1.0,    0.0,    0.0);

vec2[6] cd = vec2[6](
	vec2(0.0, 1.0), vec2(0.0, 1.0), vec2(0.0, 0.0), vec2(1.0, 1.0), vec2(1.0, 0.0), vec2(1.0, 0.0)
	);

void dd(uint ed, vec3 fd, vec2 gd, vec3 hd, vec3 id, 
    vec4 Wc, vec4 jd)
{
	_203C8330B731DD85 = Vc(Wc);

	vec2 kd = vec2(1, 1) - gd;

	vec2 ld = vec2(Zc[ed], _c[ed]) * gd
		+ vec2(ad[ed], bd[ed]) * kd;

	vec3 md = ld.x * hd + ld.y * id;

    mat4 nd = Uc * _4F2DACEFFBDAB8D1;

	gl_Position = nd * vec4(fd + md, 1.0);
	
	_BEAD44E72AA5013D = cd[ed] * jd.zw + jd.xy;
}

void od(uint ed, vec3 fd, vec2 gd, vec4 pd, vec2 qd, vec4 Wc, 
    vec4 jd)
{
    vec4 C = pd;

    float rd = 2.0 * C.z * C.z;
    float sd = 2.0 * C.x * C.y;
    float td = 2.0 * C.w * C.z;

    vec3 hd = vec3(
        1.0 - 2.0 * C.y * C.y - rd,
        sd + td,
        2.0 * C.x * C.z - 2.0 * C.w * C.y);

    vec3 id = vec3(
        sd - td,
        1.0 - 2.0 * C.x * C.x - rd,
        2.0 * C.y * C.z + 2.0 * C.w * C.x);

    dd(ed, fd, gd, hd * qd.x, id * qd.y, Wc, jd);
}

void ud()
{
	gl_Position = vec4(-2, -2, 0, 1);
}

void vd(uint wd, uint ed)
{
	e ib = P(wd - 0u);
	uint kb = ib.f;
	uint lb = ib.f + ib.g;

	vec3 fd = L(F + 0u);
	vec3 eb = L(F + 10u);
	vec3 xd = normalize(Qc);
	vec3 yd = cross(eb, xd);
	yd = normalize(yd);
	vec3 zd = cross(xd, yd);
	vec4 Ad = z(yd, zd, xd);
	vec2 Bd = vec2(10.0, 25.0);
	float cb = G(F + 6u);
	vec2 _b = (Bd * cb);
	float hc = length(eb);
	vec2 wb;
	float[3] xb = float[3](1.0,1.0,1.0);
	float yb = (hc < 0.0 ? 0.0:(hc>4000.0?4000.0:hc));
	float zb = 0.0+(yb - 0.0) * 0.00025;
	wb.x = mix(xb[int(zb)], xb[int(zb) + 1], fract(zb));
	float[3] Cd = float[3](0.33517,2.0,2.0);
	float Dd = (hc < 0.0 ? 0.0:(hc>4000.0?4000.0:hc));
	float Ed = 0.0+(Dd - 0.0) * 0.00025;
	wb.y = mix(Cd[int(Ed)], Cd[int(Ed) + 1], fract(Ed));
	vec2 jc = (_b * wb);
	vec3 db = L(F + 7u);
	vec3 oc = (db * 3.5);
	float Fd = G(F + 4u);
	float bb = G(F + 5u);
	float Gd = (Fd / bb);
	float Cb;
	float[3] Db = float[3](1.0,0.0,0.0);
	float Eb = (Gd < 0.0 ? 0.0:(Gd>1.0?1.0:Gd));
	float Fb = 0.0+(Eb - 0.0) * 1.0;
	Cb = mix(Db[int(Fb)], Db[int(Fb) + 1], fract(Fb));
	float fb = G(F + 13u);
	float Hd = (Cb * fb);
	_94B3AA5995C3B5A1 = 0u;
	od(ed, L(F + 0u), vec2(0.5, 0.5), Ad, jc, vec4(oc, Hd), Xc);
}

void main() {
	uvec2 Id = Yc();
	uint O = Id.x;
	uint ed = Id.y;

	uvec2 Jd = N(O);
	uint Kd = Jd.x;
	uint h = Jd.y;
	uint Ld = I(F + h);
	uint sb = Ld & 3u;

	if (sb != c)
	{
		ud();
		return;
	}
	switch (Kd)
	{
	case 0u: vd(O, ed); break;
	}
}

#endif 

#ifdef _DC66F57E96C6C75C

uniform float _618E8A543B8FC66E;

flat in uint _94B3AA5995C3B5A1;
smooth in vec4 _203C8330B731DD85;

out vec4 _EB400D689750048A;

smooth in vec2 _BEAD44E72AA5013D;

vec4 Md(sampler2D Nd, mat3x2 Od)
{
	return texture(Nd, Od * vec3(_BEAD44E72AA5013D, 1))* _203C8330B731DD85;
}

void makeFragColor(sampler2D Nd, mat3x2 Od)
{
	vec4 Pd = Md(Nd, Od);

#ifdef _6335F91D31E2CC83
	Pd.rgb = Pd.rgb * _203C8330B731DD85.a;
#endif

	_EB400D689750048A = _618E8A543B8FC66E < 0.0 ?
		vec4(mix(vec3(1, 1, 1), Pd.rgb, Pd.a), 1) :
		Pd;
}

in makeFragColor()

#endif

