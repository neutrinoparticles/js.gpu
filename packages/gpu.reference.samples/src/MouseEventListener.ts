
interface ListenMouseEventsOptions {
    click?: (x: number, y: number) => void;
    mousemove?: (x: number, y: number) => void;
    mousedown?: (x: number, y: number) => void;
    mouseup?: (x: number, y: number) => void;
    mouseleave?: () => void;
}

export const listenMouseEvents = (canvas: HTMLCanvasElement, listeners: ListenMouseEventsOptions) => {

    const getMousePosition = (event: MouseEvent): { x: number; y: number } => {
        const rect = canvas.getBoundingClientRect();
        return {
            x: event.clientX - rect.left,
            y: event.clientY - rect.top,
        };
    };

    if (listeners.click) {
        canvas.addEventListener("click", (event: MouseEvent) => {
            const pos = getMousePosition(event);
            listeners.click!(pos.x, pos.y);
        });
    }
    
    if (listeners.mousemove) {
        canvas.addEventListener("mousemove", (event: MouseEvent) => {
            const pos = getMousePosition(event);
            listeners.mousemove!(pos.x, pos.y);
        });
    }

    if (listeners.mousedown) {
        canvas.addEventListener("mousedown", (event: MouseEvent) => {
            const pos = getMousePosition(event);
            listeners.mousedown!(pos.x, pos.y);
        });
    }

    if (listeners.mouseup) {
        canvas.addEventListener("mouseup", (event: MouseEvent) => {
            const pos = getMousePosition(event);
            listeners.mouseup!(pos.x, pos.y);
        });
    }

    if (listeners.mouseleave) {
        canvas.addEventListener("mouseleave", (event: MouseEvent) => {
            listeners.mouseleave!();
        });
    }

}