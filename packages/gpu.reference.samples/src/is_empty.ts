
import { CameraPointObserve, Context, Effect, EffectModel, EffectsUpdateGroup, RequestTexturesLoader } from '@neutrinoparticles/gpu.reference';
import * as NPGPU from "@neutrinoparticles/gpu.types";
import { default as npgpuLoader } from "@neutrinoparticles/gpu.wasm";
import Stats from 'stats.js';

const stats = new Stats();
document.body.appendChild(stats.dom);

const bloomMaxRange = 10.0;

const stateElem = document.querySelector("#state");
const canvas = document.querySelector("#appCanvas") as HTMLCanvasElement;

let bloom: NPGPU.BloomFloat | null = null;

const enableBloom: boolean = true;

const gl = canvas.getContext("webgl2",  
    { premultipliedAlpha: false, alpha: false }) as WebGL2RenderingContext;
    
const camera = new CameraPointObserve(1400.0, 45, gl.canvas.width, gl.canvas.height);

const context = new Context(gl, {
    npgpuLoader,
    texturesLoader: new RequestTexturesLoader(gl, "textures/"),
    camera
});

await context.loadedPromise;
const npgpu = context.npgpu!;

if (enableBloom) {
    //context.gpu33Context.defaultColorMultiplier = 1.0 / bloomMaxRange;

    bloom = npgpu.BloomFloat.create(bloomMaxRange);
    if (!bloom) {
        throw new Error(npgpu.errorMessage());
    }

    bloom.resize(canvas.width, canvas.height);
}

const fileResponse = await fetch("/src/shaders/PortalPulsing.shader");
if (!fileResponse.ok)
    throw new Error("Cannot load effect file");

const effectModel = new EffectModel(context, await fileResponse.text());
const effect = new Effect(effectModel);

effect.on('ready', () => {
    requestAnimationFrame(updateFrame);
});

let lastFrameTime = 0;

const updateFrame = function(time: number) {
    stats.begin();

    if (!lastFrameTime) lastFrameTime = time;
    let dt = (time - lastFrameTime) / 1000;
    dt = (dt > 0.5) ? 0.5 : dt;
    lastFrameTime = time;

    // Start rendering
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

    EffectsUpdateGroup.updateOne(context, effect!, dt);
    stateElem!.textContent = effect?.gpu33Effect!.isEmpty() ? "EMPTY" : "ACTIVE";
    
    if (bloom) {
        context.pushViewportSettings();
        bloom!.setupForRenderScene();
    }

    {
        context.beginPreRender(gl.canvas.width, gl.canvas.height);

        effect!.preRender();

        context.endPreRender();
    }

    // Render
    {
        gl.clearColor(0, 0, 0, 1);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        context.gpu33Context.beginRender();
        
        effect!.render();
        
        context.gpu33Context.endRender();
    }

    if (bloom) {
        bloom.processBloom();

        // Render bloom results
        context.popViewportSettings();
        bloom.renderResult();
    }

    stats.end();

    requestAnimationFrame(updateFrame);
}
