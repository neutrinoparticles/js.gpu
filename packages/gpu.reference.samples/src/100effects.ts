
import { CameraPointObserve, Context, Effect, EffectModel, EffectsUpdateGroup, RequestTexturesLoader } from '@neutrinoparticles/gpu.reference';
import { default as npgpuLoader } from "@neutrinoparticles/gpu.wasm";
import Stats from 'stats.js';

const stats = new Stats();
document.body.appendChild(stats.dom);

const canvas = document.querySelector("#appCanvas") as HTMLCanvasElement;

const gl = canvas.getContext("webgl2",  
    { premultipliedAlpha: false, alpha: false }) as WebGL2RenderingContext;
    
const camera = new CameraPointObserve(6000.0, 45, gl.canvas.width, gl.canvas.height);

const context = new Context(gl, {
    npgpuLoader,
    texturesLoader: new RequestTexturesLoader(gl, "textures/"),
    camera
});

await context.loadedPromise;

const fileResponse = await fetch("/src/shaders/SmallSpray.shader");
if (!fileResponse.ok)
    throw new Error("Cannot load effect file");

const effectModel = new EffectModel(context, await fileResponse.text());
effectModel.setMaxListeners(200); // Increasing amount of listeners (effects) for the model
let effects: Array<Effect>;

// Create 100 instances of the effect
{
    const numX = 15;
    const numY = 10;
    const total = numX * numY;
    const step = 350;

    effects = new Array<Effect>(total);
    for (let x = 0; x < numX; ++x) {
        for (let y = 0; y < numY; ++y) {
            const effect = new Effect(effectModel, {
                position: [
                    -(numX - 1) * step / 2 + x * step,
                    -(numY - 1) * step / 2 + y * step,
                    0
                ]
            });

            effects[numX * y + x] = effect;
        }
    }
}

const updateGroup = new EffectsUpdateGroup(context, effects);

effectModel.on('ready', () => {
    requestAnimationFrame(updateFrame);
});

let lastFrameTime = 0;

const updateFrame = function(time: number) {
    stats.begin();

    if (!lastFrameTime == null) lastFrameTime = time;
    let dt = (time - lastFrameTime) / 1000;
    dt = (dt > 0.5) ? 0.5 : dt;
    lastFrameTime = time;

    updateGroup.update(dt);

    // Pre-render (basically for depth sorting)
    {
        context.beginPreRender(gl.canvas.width, gl.canvas.height);

        effects.forEach((effect) => {
            effect.preRender();
        });

        context.endPreRender();
    }

    // Render
    {
        // Start rendering
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

        gl.clearColor(0, 0, 0, 1);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        context.gpu33Context.beginRender();
        
        effects.forEach((effect) => {
            effect.render();
        });
        
        context.gpu33Context.endRender();
    }

    stats.end();
    requestAnimationFrame(updateFrame);
}
