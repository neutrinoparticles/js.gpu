
import { CameraPointObserve, Context, Effect, EffectModel, EffectsUpdateGroup, RequestTexturesLoader } from '@neutrinoparticles/gpu.reference';

// Use following import instead to disable Web Assembly and use plain JS
//import { default as npgpuLoader } from "@neutrinoparticles/gpu.js";
import { default as npgpuLoader } from "@neutrinoparticles/gpu.wasm";

const canvas = document.getElementById('appCanvas') as HTMLCanvasElement;

const gl = canvas.getContext("webgl2",  
    { premultipliedAlpha: false, alpha: false }) as WebGL2RenderingContext;
    
const camera = new CameraPointObserve(1000.0, 45, gl.canvas.width, gl.canvas.height);

const context = new Context(gl, {
    npgpuLoader,
    texturesLoader: new RequestTexturesLoader(gl, "textures/"),
    camera
});

await context.loadedPromise;

const fileResponse = await fetch("/src/shaders/SmallSpray.shader");
if (!fileResponse.ok)
    throw new Error("Cannot load effect file");

const effectModel = new EffectModel(context, await fileResponse.text());
const effect = new Effect(effectModel);

effect.on('ready', () => {
    requestAnimationFrame(updateFrame);
});

let lastFrameTime = 0;

const updateFrame = function(time: number) {

    if (!lastFrameTime) lastFrameTime = time;
    let dt = (time - lastFrameTime) / 1000;
    dt = (dt > 0.5) ? 0.5 : dt;
    lastFrameTime = time;

    EffectsUpdateGroup.updateOne(context, effect!, dt);

    {
        context.beginPreRender(gl.canvas.width, gl.canvas.height);

        effect!.preRender();

        context.endPreRender();
    }

    // Render
    {
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
        gl.clearColor(0, 0, 0, 1);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        context.gpu33Context.beginRender();
        
        effect!.render();
        
        context.gpu33Context.endRender();
    }

    requestAnimationFrame(updateFrame);
}

