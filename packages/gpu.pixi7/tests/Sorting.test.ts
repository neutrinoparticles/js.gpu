import { ibct, setupIbctHooks } from './ibct';

describe('Sorting', () => {
  setupIbctHooks();

  ibct('None', `
    promise(async (resolve) => {
      const screenshots = [];

      const updateGroup = new NPGPUPixi.EffectsUpdateGroup(app.neutrinoGPU);
      const effect = await loadEffect('effects/Sorting/None.shader');

      app.stage.addChild(effect);

      effect.start({
        position: [app.screen.width / 2, app.screen.height / 2, 0],
      });

      // Add the effect to the update group to update all effects at once
      updateGroup.add(effect);

      for (let i = 0; i < 3; ++i) {
        updateGroup.update(0.33);
        app.renderer.render(app.stage);
        app.renderer.gl.finish();
        screenshots.push(app.view.toDataURL());
      }

      resolve(screenshots);
    })`
  );
  
  ibct('Depth', `
    promise(async (resolve) => {
      const screenshots = [];

      const updateGroup = new NPGPUPixi.EffectsUpdateGroup(app.neutrinoGPU);
      const effect = await loadEffect('effects/Sorting/Depth.shader');

      app.stage.addChild(effect);

      effect.start({
        position: [app.screen.width / 2, app.screen.height / 2, 0],
      });

      // Add the effect to the update group to update all effects at once
      updateGroup.add(effect);

      let frameIndex = 0;
      const update = () => {
        updateGroup.update(1.0/30.0);
        app.renderer.render(app.stage);

        if ((++frameIndex % 10) === 0)
        {
          app.renderer.gl.finish();
          screenshots.push(app.view.toDataURL());
        }

        if (screenshots.length === 3)
          resolve(screenshots);
        else
          requestAnimationFrame(update);
      }      

      requestAnimationFrame(update);      
    })`,
    () => {
      expect(globalThis.warnings.length).toBeLessThan(4);
    }
  );
});
