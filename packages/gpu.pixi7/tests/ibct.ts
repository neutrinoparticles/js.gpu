import { toMatchImageSnapshot } from 'jest-image-snapshot';

expect.extend({ toMatchImageSnapshot });

export const setupIbctHooks = () => {
  beforeEach(beforeEachIbct);
  afterEach(afterEachIbct);
}

export const beforeEachIbct = async () => {
  const page = await globalThis.browser.newPage();

  page.on('console', msg => {
    const msgType = msg.type();
    const msgText = msg.text();

    if (msgType === 'warn') {
      globalThis.warnings.push(msgText);
    } else if (msgType === 'error') {
      globalThis.errors.push(msgText);
      console.error('Error:', msgText);
    }
  });

  await page.goto(globalThis.rootUrl + 'tests/page.html');
  await page.waitForFunction(`typeof initNpGPU === "function"`);
  const initNpGPU =  await page.evaluate(`initNpGPU()`);
  await initNpGPU;

  globalThis.page = page;
};

export const afterEachIbct = async () => {
  await globalThis.page.close();
  globalThis.warnings.length = 0;
  globalThis.errors.length = 0;
}

export const ibct = (name: string, code: string, finished?: ()=>void) => {
  test(name, async () => {
    const screenshots = await globalThis.page.evaluate(code) as Array<string>;

    for (let i = 0; i < screenshots.length; i++) {
      const imageBuffer = Buffer.from(
        screenshots[i].replace('data:image/png;base64,', ''),
        'base64'
      );

      const testName = expect.getState().currentTestName;

      expect(imageBuffer).toMatchImageSnapshot({
        customSnapshotIdentifier: `${testName} ${i}`,
      });

      expect(globalThis.errors.length).toBe(0);

      if (finished) {
        finished();
      }
    }
  });
}

