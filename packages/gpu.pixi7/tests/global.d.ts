import { ChildProcess } from 'child_process';
import { Browser, BrowserContext, Page } from 'playwright';

export {};

declare global {
    var server: ChildProcess;
    var browser: Browser;
    var context: BrowserContext;
    var page: Page;
    var rootUrl: string;
    var warnings: Array<string>;
    var errors: Array<string>;
}
