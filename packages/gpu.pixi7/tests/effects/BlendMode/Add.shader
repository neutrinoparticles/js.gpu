//DESC 1;30;0.0;10.0;1;0;0;0;6;2;2;1;1;1;4;1;1;1;-1;-1;0;6;0;4;Position;3;flags;1;Lifetime;1;Angle;1;0;0;
//TEXTURES default.png

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;
precision highp sampler3D;
#define _6A0EA0315208361E 6u
#define _2CE42649D807BFC4 1u

const uint a = 0u;
const uint b = 1u;
const uint c = 2u;
const uint d = 3u;

uniform sampler3D _5CABCD2CE500BEBB;

struct e
{
	uint f; 
	uint g; 
	uint h; 
	uint i; 
};

#if defined(_9CBA6082CFE05FA3) || defined(_B970C3D2AD57F959) || defined(_8CFBC1D9BA8D6516)

uint j = 0u;
uint k = 0u;
uint l = 0u;

void m(uint n)
{
	n &= 0xFFFFFFFFu;

	j = n < 2u ? n + 2u : n;
	k = n < 8u ? n + 8u : n;
	l = n < 16u ? n + 16u : n;
}

uint o()
{
	uint p = (((j << 13u) ^ j) & 0xFFFFFFFFu) >> 19u;
	j = ((j & 0xFFFFFFFEu) << 12u) ^ p;

	p = (((k << 2u) ^ k) & 0xFFFFFFFFu) >> 25u;
	k = ((k & 0xFFFFFFF8u) << 4u) ^ p;

	p = (((l << 3u) ^ l) & 0xFFFFFFFFu) >> 11u;
	l = ((l & 0xFFFFFFF0u) << 17u) ^ p;

	return (j ^ k) ^ l;
}

float q()
{
	return float(o()) / 4294967296.0;
}

#define _9D02A6055A27F4CF 3.1415926

uint r(uint s, uint p)
{
    return s - p * (s / p);
}

float t(float degrees)
{
	return degrees / 180.0 * _9D02A6055A27F4CF;
}

uniform usampler2D _E6B127475CD1AF15;
uniform usampler2D _1AAFF6BB1AC02A82;

uint u = 0u; 

float v(uint w)
{
	return uintBitsToFloat(texelFetch(_E6B127475CD1AF15,
		ivec2(w % _6A0EA0315208361E, w / _6A0EA0315208361E), 0).r);
}

uint x(uint w)
{
	return texelFetch(_E6B127475CD1AF15,
		ivec2(w % _6A0EA0315208361E, w / _6A0EA0315208361E), 0).r;
}

float y(uint w)
{
	return uintBitsToFloat(texelFetch(_1AAFF6BB1AC02A82,
		ivec2(w % _6A0EA0315208361E, w / _6A0EA0315208361E), 0).r);
}

uint z(uint w)
{
	return texelFetch(_1AAFF6BB1AC02A82,
		ivec2(w % _6A0EA0315208361E, w / _6A0EA0315208361E), 0).r;
}

vec3 A(uint w)
{
	return vec3(v(w), v(w + 1u), v(w + 2u));
}

vec3 B(uint w)
{
	return vec3(y(w), y(w + 1u), y(w + 2u));
}

uvec2 C(uint D)
{
	u = 6u + D * 6u;
	return uvec2(0u, 3u); 
}

e E(uint F)
{
	return e(0u, 4u, 3u, r(F - 0u, 1u)); 
}

#endif

#ifdef _9CBA6082CFE05FA3

uniform sampler2D _17DCC16C9305C31F;
uniform float _0D6ECD3D4CD5F8C7;
uniform float _B29D96F4CC68440F;
uniform uint _600D80823B393136;
uniform uint _A62B4010E834E4BC;
uniform uint _8B5E47AC853BBD76;

float G;

void H()
{
	gl_Position = vec4(-2.0, -2.0, 0, 1);
}

uint I(uvec2 J)
{
	return uint(texelFetch(_17DCC16C9305C31F, ivec2(J), 0).r * 1023.0);
}

uint I(uint K)
{
	return I(uvec2(K % 2u, K / 2u));
}

uvec2 L;

#if (_53986AB86EF3B727 == 0) 

#define _83AB904282DA95AA 0u

void M()
{
	uint K;
	uint F = uint(gl_VertexID);

	K = 0u + (F) / 1u * 1u + (F) % 1u % 1u;

	L = uvec2(K % 2u, K / 2u);
	gl_Position = vec4((float(L.x) + 0.5) / float(2u) * 2.0 - 1.0, (float(L.y) + 0.5) / float(1u) * 2.0 - 1.0,  0, 1);
}

uint N()
{
	return c;
}

vec3 O;
uint P;
float Q;
float R;

flat out vec3 _C09D6F3C5A9B9FBA;
flat out uint _98DBFB22A3024CC1;
flat out float _AF547B67E173F200;
flat out float _D95F711B69033EF4;

uint D()
{
	return uint(gl_VertexID);
}

void S(uint D)
{
	m((0xFFFFFFFFu / 1u * D) ^ _600D80823B393136 ^ P);
}

bool T(e U, float V, out float _93AC3C19AEE8CE0B)
{
	uint W = U.f;
	uint X = U.f + U.g;

	uint Y = I(L);
	uint Z = 1u + ((uint(gl_VertexID) % 1u) < 0u ? 1u : 0u); 
	float _ = float(1u) * (float(Z) - float(Y));
	float ab = V / _;
	float bb = q();

	float cb = v(X + 0u);
	{
		uint db = uint(1.0);
		uint eb = 1u / db;
		float length = 100.f / 49.0;
		float fb = length / float(eb);
		float gb = 0.0 + fb * (1.0 - 1.0);
		if (cb >= gb)
		{
			float hb = mod(cb - gb, length);
			float ib = hb - V;
			float jb = float(U.i / uint(1.0)) * fb;
			if (ib <= jb && hb >= jb)
			{
				_93AC3C19AEE8CE0B = (jb - ib) / V;
				return true;
			}
		}
	}
	return false;
}

void kb(uint lb)
{
	P = (P & 0xFFFFFFFCu) | lb;
}

e mb()
{
	return E(uint(gl_VertexID));
}

void nb(e U, float _93AC3C19AEE8CE0B)
{
	uint W = U.f;
	uint X = U.f + U.g;

	Q = 0.0;
	vec3  ob = vec3(0.0, 0.0, 0.0);
	O = ob;
	vec3 pb = mix(
		B(W + 0u), 
		A(W + 0u),
		_93AC3C19AEE8CE0B);
	O += pb;
	R = 0.0;
}

void qb()
{
	G = _B29D96F4CC68440F;

	O = B(u + 0u);
	Q = y(u + 4u);
	R = y(u + 5u);
}

void rb(e U)
{
	uint W = U.f;
	uint X = U.f + U.g;

	_AF547B67E173F200 = Q + G; 
	_C09D6F3C5A9B9FBA = O;
	_D95F711B69033EF4 = R;
	float ob = 2.0;
	if (_AF547B67E173F200 >= ob)
		kb(a);
}

void sb()
{
	_C09D6F3C5A9B9FBA = O; 
	_98DBFB22A3024CC1 = P; 
	_AF547B67E173F200 = Q; 
	_D95F711B69033EF4 = R; 
}

#endif

void main() {
	uint D = _83AB904282DA95AA + uint(gl_VertexID);
	uint h = C(D).y;

	P = z(u + h);
	uint lb = P & 3u;

	M();

	S(D);

	uint tb = 0u;
	uint ub;

	e vb = mb();

	switch (lb)
	{
	case a:
	{
		uint wb = x(vb.f + vb.h);
		uint xb = (wb & 3u);
		uint yb = ((P >> 2u) & 3u); 

		float zb = (yb != 0u ? 
			_B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - float((P >> 4u) + 1u) / float(0xFFFFFFFu)) : 
			_B29D96F4CC68440F
		);

float _93AC3C19AEE8CE0B = 0.f;

		if (xb == N() && T(vb, zb, _93AC3C19AEE8CE0B))
		{
			G = zb * (1.0 - _93AC3C19AEE8CE0B);
			nb(vb, _93AC3C19AEE8CE0B);

#ifdef _73599E697EB8FFF2
			kb(b);
			ub = uint(_93AC3C19AEE8CE0B * float(0xFFFFFFFu)) << 4u;
			sb();
#else
			kb(c);	
			rb(vb);
			ub = o();
#endif
		}
		else
		{
			H();

			if (xb == b)
			{
				ub = wb; 
				tb = 1u; 
			}
			else
			{
				ub = o();
			}
		}
				
		break;
	}

#ifdef _73599E697EB8FFF2
	case b:
	{
		qb();
		float _93AC3C19AEE8CE0B = float(P >> 4u) / float(0xFFFFFFFu);
		G = _B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - _93AC3C19AEE8CE0B);
		rb(vb);

		if ((P & 3u) == a)
		{
			H();
		}
		else
		{
			kb(c);
			tb = min(((P >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		ub = P;

		break;
	}
#endif

	case c:
	{
		qb();
		G = _B29D96F4CC68440F;
		rb(vb);

		if ((P & 3u) == a)
		{
			H();
		}
		else
		{
			tb = min(((P >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		ub = o();
					
		break;
	}

#ifdef _73599E697EB8FFF2
	case d:
	{
		qb();
		Ab();

		if ((P & 3u) == a)
		{
			H();
		}
		else
		{
			tb = min(((P >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		ub = o();
		break;
	}
#endif
	}

	P = (ub & 0xFFFFFFF0u) | (tb << 2u) | (P & 3u);

	_98DBFB22A3024CC1 = P;
	gl_PointSize = 1.0;
}

#endif 
#ifdef _CC5A472CC9FE17BA

out vec4 _4CCF54A3B6372A66;

void main()
{
	_4CCF54A3B6372A66 = vec4(1.01 / 1024.0, 0, 0, 0);
}

#endif

#ifdef _B970C3D2AD57F959

layout (std140) uniform _F52D3B1AA302F088
{
    vec3 Bb;
    float Cb;
    vec3 Db;
    uint Eb;
    vec3 Fb;
    uint Gb;
    vec3 Hb;
    float Ib;
    mat4 Jb;
};

uniform float _618E8A543B8FC66E;

uniform mat4 _4F2DACEFFBDAB8D1;

vec4 Kb(vec4 Lb)
{
	return _618E8A543B8FC66E < 0.0 ? Lb : vec4(Lb.rgb * _618E8A543B8FC66E, Lb.a);
}

smooth out vec4 _203C8330B731DD85;
flat out uint _94B3AA5995C3B5A1;

vec4 Mb = vec4(0, 0, 1, 1);

uvec2 Nb()
{
	return uvec2(uint(gl_VertexID) / 6u, uint(gl_VertexID) % 6u);
}

smooth out vec2 _BEAD44E72AA5013D;

float[6] Ob = float[6](-1.0,   -1.0,   -1.0,   0.0,   0.0,    0.0);
float[6] Pb = float[6](0.0,    0.0,    -1.0,    0.0,   -1.0,   -1.0);

float[6] Qb = float[6](0.0, 0.0,    0.0,    1.0,    1.0,    1.0);
float[6] Rb = float[6](1.0, 1.0,    0.0,    1.0,    0.0,    0.0);

vec2[6] Sb = vec2[6](
	vec2(0.0, 1.0), vec2(0.0, 1.0), vec2(0.0, 0.0), vec2(1.0, 1.0), vec2(1.0, 0.0), vec2(1.0, 0.0)
	);

void Tb(uint Ub, vec3 Vb, vec2 Wb, vec3 Xb, vec3 Yb, 
    vec4 Lb, vec4 Zb)
{
	_203C8330B731DD85 = Kb(Lb);

	vec2 _b = vec2(1, 1) - Wb;

	vec2 ac = vec2(Ob[Ub], Pb[Ub]) * Wb
		+ vec2(Qb[Ub], Rb[Ub]) * _b;

	vec3 bc = ac.x * Xb + ac.y * Yb;

    mat4 cc = Jb * _4F2DACEFFBDAB8D1;

	gl_Position = cc * vec4(Vb + bc, 1.0);
	
	_BEAD44E72AA5013D = Sb[Ub] * Zb.zw + Zb.xy;
}

void dc(uint Ub, vec3 Vb, vec2 Wb, float ec, vec2 fc, vec4 Lb, vec4 Zb)
{
    vec3 gc = Hb;
    vec3 hc = Db;

    float s = t(ec);
    float ic = -sin(s);
    float jc = cos(s);

    vec3 Xb = vec3(
        gc.x * jc + hc.x * ic,
        gc.y * jc + hc.y * ic,
        gc.z * jc + hc.z * ic);

    vec3 Yb = vec3(
        -gc.x * ic + hc.x * jc,
        -gc.y * ic + hc.y * jc,
        -gc.z * ic + hc.z * jc);

    Tb(Ub, Vb, Wb, Xb * fc.x, Yb * fc.y, Lb, Zb);
}

void kc()
{
	gl_Position = vec4(-2, -2, 0, 1);
}

void lc(uint mc, uint Ub)
{
	e U = E(mc - 0u);
	uint W = U.f;
	uint X = U.f + U.g;

	vec3 Vb = A(u + 0u);
	float R = v(u + 5u);
	float ob = 300.0;
	_94B3AA5995C3B5A1 = 0u;
	dc(Ub, A(u + 0u), vec2(0.5, 0.5), R, vec2(ob, ob), vec4(vec3(1.0, 1.0, 1.0), 1.0), Mb);
}

void main() {
	uvec2 nc = Nb();
	uint D = nc.x;
	uint Ub = nc.y;

	uvec2 oc = C(D);
	uint pc = oc.x;
	uint h = oc.y;
	uint qc = x(u + h);
	uint lb = qc & 3u;

	if (lb != c)
	{
		kc();
		return;
	}
	switch (pc)
	{
	case 0u: lc(D, Ub); break;
	}
}

#endif 

#ifdef _DC66F57E96C6C75C

uniform float _618E8A543B8FC66E;

flat in uint _94B3AA5995C3B5A1;
smooth in vec4 _203C8330B731DD85;

out vec4 _EB400D689750048A;

smooth in vec2 _BEAD44E72AA5013D;

vec4 rc(sampler2D sc, mat3x2 tc)
{
	return texture(sc, tc * vec3(_BEAD44E72AA5013D, 1))* _203C8330B731DD85;
}

void makeFragColor(sampler2D sc, mat3x2 tc)
{
	vec4 uc = rc(sc, tc);

#ifdef _6335F91D31E2CC83
	uc.rgb = uc.rgb * _203C8330B731DD85.a;
#endif

	_EB400D689750048A = _618E8A543B8FC66E < 0.0 ?
		vec4(mix(vec3(1, 1, 1), uc.rgb, uc.a), 1) :
		uc;
}

in makeFragColor()

#endif

