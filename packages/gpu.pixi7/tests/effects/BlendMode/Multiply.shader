//DESC 1;30;0.0;10.0;1;0;0;0;9;11;2;1;1;1;4;1;10;2;-1;-1;0;6;0;5;Position;3;flags;1;Lifetime;1;Velocity;3;Angle;1;0;0;
//TEXTURES default.png

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;
precision highp sampler3D;
#define _6A0EA0315208361E 9u
#define _2CE42649D807BFC4 1u

const uint a = 0u;
const uint b = 1u;
const uint c = 2u;
const uint d = 3u;

uniform sampler3D _5CABCD2CE500BEBB;

struct e
{
	uint f; 
	uint g; 
	uint h; 
	uint i; 
};

#if defined(_9CBA6082CFE05FA3) || defined(_B970C3D2AD57F959) || defined(_8CFBC1D9BA8D6516)

uint j = 0u;
uint k = 0u;
uint l = 0u;

void m(uint n)
{
	n &= 0xFFFFFFFFu;

	j = n < 2u ? n + 2u : n;
	k = n < 8u ? n + 8u : n;
	l = n < 16u ? n + 16u : n;
}

uint o()
{
	uint p = (((j << 13u) ^ j) & 0xFFFFFFFFu) >> 19u;
	j = ((j & 0xFFFFFFFEu) << 12u) ^ p;

	p = (((k << 2u) ^ k) & 0xFFFFFFFFu) >> 25u;
	k = ((k & 0xFFFFFFF8u) << 4u) ^ p;

	p = (((l << 3u) ^ l) & 0xFFFFFFFFu) >> 11u;
	l = ((l & 0xFFFFFFF0u) << 17u) ^ p;

	return (j ^ k) ^ l;
}

float q()
{
	return float(o()) / 4294967296.0;
}

#define _9D02A6055A27F4CF 3.1415926

uint r(uint s, uint p)
{
    return s - p * (s / p);
}

float t(float degrees)
{
	return degrees / 180.0 * _9D02A6055A27F4CF;
}

uniform usampler2D _E6B127475CD1AF15;
uniform usampler2D _1AAFF6BB1AC02A82;

uint u = 0u; 

float v(uint w)
{
	return uintBitsToFloat(texelFetch(_E6B127475CD1AF15,
		ivec2(w % _6A0EA0315208361E, w / _6A0EA0315208361E), 0).r);
}

uint x(uint w)
{
	return texelFetch(_E6B127475CD1AF15,
		ivec2(w % _6A0EA0315208361E, w / _6A0EA0315208361E), 0).r;
}

float y(uint w)
{
	return uintBitsToFloat(texelFetch(_1AAFF6BB1AC02A82,
		ivec2(w % _6A0EA0315208361E, w / _6A0EA0315208361E), 0).r);
}

uint z(uint w)
{
	return texelFetch(_1AAFF6BB1AC02A82,
		ivec2(w % _6A0EA0315208361E, w / _6A0EA0315208361E), 0).r;
}

vec3 A(uint w)
{
	return vec3(v(w), v(w + 1u), v(w + 2u));
}

vec3 B(uint w)
{
	return vec3(y(w), y(w + 1u), y(w + 2u));
}

uvec2 C(uint D)
{
	u = 9u + D * 9u;
	return uvec2(0u, 3u); 
}

e E(uint F)
{
	return e(0u, 4u, 3u, r(F - 0u, 10u)); 
}

#endif

#ifdef _9CBA6082CFE05FA3

uniform sampler2D _17DCC16C9305C31F;
uniform float _0D6ECD3D4CD5F8C7;
uniform float _B29D96F4CC68440F;
uniform uint _600D80823B393136;
uniform uint _A62B4010E834E4BC;
uniform uint _8B5E47AC853BBD76;

float G;

void H()
{
	gl_Position = vec4(-2.0, -2.0, 0, 1);
}

uint I(uvec2 J)
{
	return uint(texelFetch(_17DCC16C9305C31F, ivec2(J), 0).r * 1023.0);
}

uint I(uint K)
{
	return I(uvec2(K % 2u, K / 2u));
}

uvec2 L;

#if (_53986AB86EF3B727 == 0) 

#define _83AB904282DA95AA 0u

void M()
{
	uint K;
	uint F = uint(gl_VertexID);

	K = 0u + (F) / 10u * 1u + (F) % 10u % 1u;

	L = uvec2(K % 2u, K / 2u);
	gl_Position = vec4((float(L.x) + 0.5) / float(2u) * 2.0 - 1.0, (float(L.y) + 0.5) / float(1u) * 2.0 - 1.0,  0, 1);
}

uint N()
{
	return c;
}

vec3 O;
uint P;
float Q;
vec3 R;
float S;

flat out vec3 _C09D6F3C5A9B9FBA;
flat out uint _98DBFB22A3024CC1;
flat out float _AF547B67E173F200;
flat out vec3 _BA00525347033428;
flat out float _D95F711B69033EF4;

uint D()
{
	return uint(gl_VertexID);
}

void T(uint D)
{
	m((0xFFFFFFFFu / 10u * D) ^ _600D80823B393136 ^ P);
}

bool U(e V, float W, out float _93AC3C19AEE8CE0B)
{
	uint X = V.f;
	uint Y = V.f + V.g;

	uint Z = I(L);
	uint _ = 10u + ((uint(gl_VertexID) % 1u) < 0u ? 1u : 0u); 
	float ab = float(1u) * (float(_) - float(Z));
	float bb = W / ab;
	float cb = q();

	float db = v(Y + 0u);
	{
		uint eb = uint(1.0);
		uint fb = 10u / eb;
		float length = 100.f / 49.0;
		float gb = length / float(fb);
		float hb = 0.0 + gb * (1.0 - 1.0);
		if (db >= hb)
		{
			float ib = mod(db - hb, length);
			float jb = ib - W;
			float kb = float(V.i / uint(1.0)) * gb;
			if (jb <= kb && ib >= kb)
			{
				_93AC3C19AEE8CE0B = (kb - jb) / W;
				return true;
			}
		}
	}
	return false;
}

void lb(uint mb)
{
	P = (P & 0xFFFFFFFCu) | mb;
}

e nb()
{
	return E(uint(gl_VertexID));
}

void ob(e V, float _93AC3C19AEE8CE0B)
{
	uint X = V.f;
	uint Y = V.f + V.g;

	Q = 0.0;
	vec3  pb = vec3(-200.0, 0.0, 0.0);
	O = pb;
	vec3 qb = mix(
		B(X + 0u), 
		A(X + 0u),
		_93AC3C19AEE8CE0B);
	O += qb;
	vec3  rb = vec3(200.0, 0.0, 0.0);
	R = rb;
	S = 0.0;
}

void sb()
{
	G = _B29D96F4CC68440F;

	O = B(u + 0u);
	Q = y(u + 4u);
	R = B(u + 5u);
	S = y(u + 8u);
}

void tb(e V)
{
	uint X = V.f;
	uint Y = V.f + V.g;

	_AF547B67E173F200 = Q + G; 
	_C09D6F3C5A9B9FBA = O;
	_BA00525347033428 = R;
	vec3 ub = _C09D6F3C5A9B9FBA + _BA00525347033428 * G;
	_C09D6F3C5A9B9FBA = ub;
	_D95F711B69033EF4 = S;
	float pb = 2.0;
	if (_AF547B67E173F200 >= pb)
		lb(a);
}

void vb()
{
	_C09D6F3C5A9B9FBA = O; 
	_98DBFB22A3024CC1 = P; 
	_AF547B67E173F200 = Q; 
	_BA00525347033428 = R; 
	_D95F711B69033EF4 = S; 
}

#endif

void main() {
	uint D = _83AB904282DA95AA + uint(gl_VertexID);
	uint h = C(D).y;

	P = z(u + h);
	uint mb = P & 3u;

	M();

	T(D);

	uint wb = 0u;
	uint xb;

	e yb = nb();

	switch (mb)
	{
	case a:
	{
		uint zb = x(yb.f + yb.h);
		uint Ab = (zb & 3u);
		uint Bb = ((P >> 2u) & 3u); 

		float Cb = (Bb != 0u ? 
			_B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - float((P >> 4u) + 1u) / float(0xFFFFFFFu)) : 
			_B29D96F4CC68440F
		);

float _93AC3C19AEE8CE0B = 0.f;

		if (Ab == N() && U(yb, Cb, _93AC3C19AEE8CE0B))
		{
			G = Cb * (1.0 - _93AC3C19AEE8CE0B);
			ob(yb, _93AC3C19AEE8CE0B);

#ifdef _73599E697EB8FFF2
			lb(b);
			xb = uint(_93AC3C19AEE8CE0B * float(0xFFFFFFFu)) << 4u;
			vb();
#else
			lb(c);	
			tb(yb);
			xb = o();
#endif
		}
		else
		{
			H();

			if (Ab == b)
			{
				xb = zb; 
				wb = 1u; 
			}
			else
			{
				xb = o();
			}
		}
				
		break;
	}

#ifdef _73599E697EB8FFF2
	case b:
	{
		sb();
		float _93AC3C19AEE8CE0B = float(P >> 4u) / float(0xFFFFFFFu);
		G = _B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - _93AC3C19AEE8CE0B);
		tb(yb);

		if ((P & 3u) == a)
		{
			H();
		}
		else
		{
			lb(c);
			wb = min(((P >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		xb = P;

		break;
	}
#endif

	case c:
	{
		sb();
		G = _B29D96F4CC68440F;
		tb(yb);

		if ((P & 3u) == a)
		{
			H();
		}
		else
		{
			wb = min(((P >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		xb = o();
					
		break;
	}

#ifdef _73599E697EB8FFF2
	case d:
	{
		sb();
		Db();

		if ((P & 3u) == a)
		{
			H();
		}
		else
		{
			wb = min(((P >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		xb = o();
		break;
	}
#endif
	}

	P = (xb & 0xFFFFFFF0u) | (wb << 2u) | (P & 3u);

	_98DBFB22A3024CC1 = P;
	gl_PointSize = 1.0;
}

#endif 
#ifdef _CC5A472CC9FE17BA

out vec4 _4CCF54A3B6372A66;

void main()
{
	_4CCF54A3B6372A66 = vec4(1.01 / 1024.0, 0, 0, 0);
}

#endif

#ifdef _B970C3D2AD57F959

layout (std140) uniform _F52D3B1AA302F088
{
    vec3 Eb;
    float Fb;
    vec3 Gb;
    uint Hb;
    vec3 Ib;
    uint Jb;
    vec3 Kb;
    float Lb;
    mat4 Mb;
};

uniform float _618E8A543B8FC66E;

uniform mat4 _4F2DACEFFBDAB8D1;

vec4 Nb(vec4 Ob)
{
	return _618E8A543B8FC66E < 0.0 ? Ob : vec4(Ob.rgb * _618E8A543B8FC66E, Ob.a);
}

smooth out vec4 _203C8330B731DD85;
flat out uint _94B3AA5995C3B5A1;

vec4 Pb = vec4(0, 0, 1, 1);

uvec2 Qb()
{
	return uvec2(uint(gl_VertexID) / 6u, uint(gl_VertexID) % 6u);
}

smooth out vec2 _BEAD44E72AA5013D;

float[6] Rb = float[6](-1.0,   -1.0,   -1.0,   0.0,   0.0,    0.0);
float[6] Sb = float[6](0.0,    0.0,    -1.0,    0.0,   -1.0,   -1.0);

float[6] Tb = float[6](0.0, 0.0,    0.0,    1.0,    1.0,    1.0);
float[6] Ub = float[6](1.0, 1.0,    0.0,    1.0,    0.0,    0.0);

vec2[6] Vb = vec2[6](
	vec2(0.0, 1.0), vec2(0.0, 1.0), vec2(0.0, 0.0), vec2(1.0, 1.0), vec2(1.0, 0.0), vec2(1.0, 0.0)
	);

void Wb(uint Xb, vec3 Yb, vec2 Zb, vec3 _b, vec3 ac, 
    vec4 Ob, vec4 bc)
{
	_203C8330B731DD85 = Nb(Ob);

	vec2 cc = vec2(1, 1) - Zb;

	vec2 dc = vec2(Rb[Xb], Sb[Xb]) * Zb
		+ vec2(Tb[Xb], Ub[Xb]) * cc;

	vec3 ec = dc.x * _b + dc.y * ac;

    mat4 fc = Mb * _4F2DACEFFBDAB8D1;

	gl_Position = fc * vec4(Yb + ec, 1.0);
	
	_BEAD44E72AA5013D = Vb[Xb] * bc.zw + bc.xy;
}

void gc(uint Xb, vec3 Yb, vec2 Zb, float hc, vec2 ic, vec4 Ob, vec4 bc)
{
    vec3 jc = Kb;
    vec3 kc = Gb;

    float s = t(hc);
    float lc = -sin(s);
    float mc = cos(s);

    vec3 _b = vec3(
        jc.x * mc + kc.x * lc,
        jc.y * mc + kc.y * lc,
        jc.z * mc + kc.z * lc);

    vec3 ac = vec3(
        -jc.x * lc + kc.x * mc,
        -jc.y * lc + kc.y * mc,
        -jc.z * lc + kc.z * mc);

    Wb(Xb, Yb, Zb, _b * ic.x, ac * ic.y, Ob, bc);
}

void nc()
{
	gl_Position = vec4(-2, -2, 0, 1);
}

void oc(uint pc, uint Xb)
{
	e V = E(pc - 0u);
	uint X = V.f;
	uint Y = V.f + V.g;

	vec3 Yb = A(u + 0u);
	float S = v(u + 8u);
	float pb = 50.0;
	float qc = v(u + 4u);
	float rb = 2.0;
	float rc = (qc / rb);
	float sc;
	float[3] tc = float[3](1.0,0.0,0.0);
	float uc = (rc < 0.0 ? 0.0:(rc>1.0?1.0:rc));
	float vc = 0.0+(uc - 0.0) * 1.0;
	sc = mix(tc[int(vc)], tc[int(vc) + 1], fract(vc));
	_94B3AA5995C3B5A1 = 0u;
	gc(Xb, A(u + 0u), vec2(0.5, 0.5), S, vec2(pb, pb), vec4(vec3(1.0, 1.0, 1.0), sc), Pb);
}

void main() {
	uvec2 wc = Qb();
	uint D = wc.x;
	uint Xb = wc.y;

	uvec2 xc = C(D);
	uint yc = xc.x;
	uint h = xc.y;
	uint zc = x(u + h);
	uint mb = zc & 3u;

	if (mb != c)
	{
		nc();
		return;
	}
	switch (yc)
	{
	case 0u: oc(D, Xb); break;
	}
}

#endif 

#ifdef _DC66F57E96C6C75C

uniform float _618E8A543B8FC66E;

flat in uint _94B3AA5995C3B5A1;
smooth in vec4 _203C8330B731DD85;

out vec4 _EB400D689750048A;

smooth in vec2 _BEAD44E72AA5013D;

vec4 Ac(sampler2D Bc, mat3x2 Cc)
{
	return texture(Bc, Cc * vec3(_BEAD44E72AA5013D, 1))* _203C8330B731DD85;
}

void makeFragColor(sampler2D Bc, mat3x2 Cc)
{
	vec4 Dc = Ac(Bc, Cc);

#ifdef _6335F91D31E2CC83
	Dc.rgb = Dc.rgb * _203C8330B731DD85.a;
#endif

	_EB400D689750048A = _618E8A543B8FC66E < 0.0 ?
		vec4(mix(vec3(1, 1, 1), Dc.rgb, Dc.a), 1) :
		Dc;
}

in makeFragColor()

#endif

