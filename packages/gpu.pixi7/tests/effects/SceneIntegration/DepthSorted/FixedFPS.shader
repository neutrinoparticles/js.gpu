//DESC 1;30;0.0;10.0;1;0;0;0;30;31;2;1;1;1;4;1;100;0;-1;-1;-1;6;0;5;Position;3;flags;1;Lifetime;1;Velocity;3;Angle;1;0;1;
//TEXTURES default.png

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;
precision highp sampler3D;
#define _6A0EA0315208361E 30u
#define _2CE42649D807BFC4 1u

const uint a = 0u;
const uint b = 1u;
const uint c = 2u;
const uint d = 3u;

uniform sampler3D _5CABCD2CE500BEBB;

struct e
{
	uint f; 
	uint g; 
	uint h; 
	uint i; 
};

#if defined(_9CBA6082CFE05FA3) || defined(_B970C3D2AD57F959) || defined(_8CFBC1D9BA8D6516)

uint j = 0u;
uint k = 0u;
uint l = 0u;

void m(uint n)
{
	n &= 0xFFFFFFFFu;

	j = n < 2u ? n + 2u : n;
	k = n < 8u ? n + 8u : n;
	l = n < 16u ? n + 16u : n;
}

uint o()
{
	uint p = (((j << 13u) ^ j) & 0xFFFFFFFFu) >> 19u;
	j = ((j & 0xFFFFFFFEu) << 12u) ^ p;

	p = (((k << 2u) ^ k) & 0xFFFFFFFFu) >> 25u;
	k = ((k & 0xFFFFFFF8u) << 4u) ^ p;

	p = (((l << 3u) ^ l) & 0xFFFFFFFFu) >> 11u;
	l = ((l & 0xFFFFFFF0u) << 17u) ^ p;

	return (j ^ k) ^ l;
}

float q()
{
	return float(o()) / 4294967296.0;
}

#define _9D02A6055A27F4CF 3.1415926

uint r(uint s, uint p)
{
    return s - p * (s / p);
}

vec3 t(float u)
{
	vec3 v;
	float w = q() * _9D02A6055A27F4CF * 2.0;
	float x = -1.0 + q() * 2.0;
	float y = u * sqrt(1.0 - x * x);
	v.x = y * cos(w);
	v.y = y * sin(w);
	v.z = u * x;
	return v;
}

float z(float degrees)
{
	return degrees / 180.0 * _9D02A6055A27F4CF;
}

uniform usampler2D _E6B127475CD1AF15;
uniform usampler2D _1AAFF6BB1AC02A82;

uint A = 0u; 

float B(uint C)
{
	return uintBitsToFloat(texelFetch(_E6B127475CD1AF15,
		ivec2(C % _6A0EA0315208361E, C / _6A0EA0315208361E), 0).r);
}

uint D(uint C)
{
	return texelFetch(_E6B127475CD1AF15,
		ivec2(C % _6A0EA0315208361E, C / _6A0EA0315208361E), 0).r;
}

float E(uint C)
{
	return uintBitsToFloat(texelFetch(_1AAFF6BB1AC02A82,
		ivec2(C % _6A0EA0315208361E, C / _6A0EA0315208361E), 0).r);
}

uint F(uint C)
{
	return texelFetch(_1AAFF6BB1AC02A82,
		ivec2(C % _6A0EA0315208361E, C / _6A0EA0315208361E), 0).r;
}

vec3 G(uint C)
{
	return vec3(B(C), B(C + 1u), B(C + 2u));
}

vec3 H(uint C)
{
	return vec3(E(C), E(C + 1u), E(C + 2u));
}

uvec2 I(uint J)
{
	A = 30u + J * 9u;
	return uvec2(0u, 3u); 
}

e K(uint L)
{
	return e(0u, 4u, 3u, r(L - 0u, 100u)); 
}

#endif

#ifdef _9CBA6082CFE05FA3

uniform sampler2D _17DCC16C9305C31F;
uniform float _0D6ECD3D4CD5F8C7;
uniform float _B29D96F4CC68440F;
uniform uint _600D80823B393136;
uniform uint _A62B4010E834E4BC;
uniform uint _8B5E47AC853BBD76;

float M;

void N()
{
	gl_Position = vec4(-2.0, -2.0, 0, 1);
}

uint O(uvec2 P)
{
	return uint(texelFetch(_17DCC16C9305C31F, ivec2(P), 0).r * 1023.0);
}

uint O(uint Q)
{
	return O(uvec2(Q % 2u, Q / 2u));
}

uvec2 R;

#if (_53986AB86EF3B727 == 0) 

#define _83AB904282DA95AA 0u

void S()
{
	uint Q;
	uint L = uint(gl_VertexID);

	Q = 0u + (L) / 100u * 1u + (L) % 100u % 1u;

	R = uvec2(Q % 2u, Q / 2u);
	gl_Position = vec4((float(R.x) + 0.5) / float(2u) * 2.0 - 1.0, (float(R.y) + 0.5) / float(1u) * 2.0 - 1.0,  0, 1);
}

uint T()
{
	return c;
}

vec3 U;
uint V;
float W;
vec3 X;
float Y;

flat out vec3 _C09D6F3C5A9B9FBA;
flat out uint _98DBFB22A3024CC1;
flat out float _AF547B67E173F200;
flat out vec3 _BA00525347033428;
flat out float _D95F711B69033EF4;

uint J()
{
	return uint(gl_VertexID);
}

void Z(uint J)
{
	m((0xFFFFFFFFu / 100u * J) ^ _600D80823B393136 ^ V);
}

bool _(e ab, float bb, out float _93AC3C19AEE8CE0B)
{
	uint cb = ab.f;
	uint db = ab.f + ab.g;

	uint eb = O(R);
	uint fb = 100u + ((uint(gl_VertexID) % 1u) < 0u ? 1u : 0u); 
	float gb = float(1u) * (float(fb) - float(eb));
	float hb = bb / gb;
	float ib = q();

	if (ib < (hb * (50.0 * 1.0))) { _93AC3C19AEE8CE0B = q(); return true; } 
	return false;
}

void jb(uint kb)
{
	V = (V & 0xFFFFFFFCu) | kb;
}

e lb()
{
	return K(uint(gl_VertexID));
}

void mb(e ab, float _93AC3C19AEE8CE0B)
{
	uint cb = ab.f;
	uint db = ab.f + ab.g;

	W = 0.0;
	vec3  nb = vec3(0.0, 0.0, 0.0);
	U = nb;
	vec3 ob = mix(
		H(cb + 0u), 
		G(cb + 0u),
		_93AC3C19AEE8CE0B);
	U += ob;
	vec3 pb = t(100.0);
	X = pb;
	Y = 0.0;
}

void qb()
{
	M = _B29D96F4CC68440F;

	U = H(A + 0u);
	W = E(A + 4u);
	X = H(A + 5u);
	Y = E(A + 8u);
}

void rb(e ab)
{
	uint cb = ab.f;
	uint db = ab.f + ab.g;

	_AF547B67E173F200 = W + M; 
	_C09D6F3C5A9B9FBA = U;
	_BA00525347033428 = X;
	vec3 sb = _C09D6F3C5A9B9FBA + _BA00525347033428 * M;
	_C09D6F3C5A9B9FBA = sb;
	_D95F711B69033EF4 = Y;
	float nb = 2.0;
	if (_AF547B67E173F200 >= nb)
		jb(a);
}

void tb()
{
	_C09D6F3C5A9B9FBA = U; 
	_98DBFB22A3024CC1 = V; 
	_AF547B67E173F200 = W; 
	_BA00525347033428 = X; 
	_D95F711B69033EF4 = Y; 
}

#endif

void main() {
	uint J = _83AB904282DA95AA + uint(gl_VertexID);
	uint h = I(J).y;

	V = F(A + h);
	uint kb = V & 3u;

	S();

	Z(J);

	uint ub = 0u;
	uint vb;

	e wb = lb();

	switch (kb)
	{
	case a:
	{
		uint xb = D(wb.f + wb.h);
		uint yb = (xb & 3u);
		uint zb = ((V >> 2u) & 3u); 

		float Ab = (zb != 0u ? 
			_B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - float((V >> 4u) + 1u) / float(0xFFFFFFFu)) : 
			_B29D96F4CC68440F
		);

float _93AC3C19AEE8CE0B = 0.f;

		if (yb == T() && _(wb, Ab, _93AC3C19AEE8CE0B))
		{
			M = Ab * (1.0 - _93AC3C19AEE8CE0B);
			mb(wb, _93AC3C19AEE8CE0B);

#ifdef _73599E697EB8FFF2
			jb(b);
			vb = uint(_93AC3C19AEE8CE0B * float(0xFFFFFFFu)) << 4u;
			tb();
#else
			jb(c);	
			rb(wb);
			vb = o();
#endif
		}
		else
		{
			N();

			if (yb == b)
			{
				vb = xb; 
				ub = 1u; 
			}
			else
			{
				vb = o();
			}
		}
				
		break;
	}

#ifdef _73599E697EB8FFF2
	case b:
	{
		qb();
		float _93AC3C19AEE8CE0B = float(V >> 4u) / float(0xFFFFFFFu);
		M = _B29D96F4CC68440F + _0D6ECD3D4CD5F8C7 * (1.0 - _93AC3C19AEE8CE0B);
		rb(wb);

		if ((V & 3u) == a)
		{
			N();
		}
		else
		{
			jb(c);
			ub = min(((V >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		vb = V;

		break;
	}
#endif

	case c:
	{
		qb();
		M = _B29D96F4CC68440F;
		rb(wb);

		if ((V & 3u) == a)
		{
			N();
		}
		else
		{
			ub = min(((V >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		vb = o();
					
		break;
	}

#ifdef _73599E697EB8FFF2
	case d:
	{
		qb();
		Bb();

		if ((V & 3u) == a)
		{
			N();
		}
		else
		{
			ub = min(((V >> 2u) & 3u) + _8B5E47AC853BBD76, 3u);
		}

		vb = o();
		break;
	}
#endif
	}

	V = (vb & 0xFFFFFFF0u) | (ub << 2u) | (V & 3u);

	_98DBFB22A3024CC1 = V;
	gl_PointSize = 1.0;
}

#endif 
#ifdef _CC5A472CC9FE17BA

out vec4 _4CCF54A3B6372A66;

void main()
{
	_4CCF54A3B6372A66 = vec4(1.01 / 1024.0, 0, 0, 0);
}

#endif

#ifdef _8CFBC1D9BA8D6516

uniform sampler2D _50099FB73CB57A9E; 

uniform float _30B9E4384BFDB31C;
uniform vec3 _953A2A72B2E8B0E9;
uniform uint _14DB390EE2887C41;

flat out float _9BC2E3181A315B34;

flat out uint _5A57BFB62CAEDE57;

float Cb;
uint Db;

float Eb(float Fb)
{
	return Fb * 0.5 + 0.5;
}
float Gb(float Fb)
{
	return (Fb - 0.5) * 2.0;
}

float Hb(float Fb)
{
	return Eb(log2(abs(Fb) + 1.0) / 32.0 * sign(Fb));
}

float Ib(float Fb)
{
	return (exp2(abs(Gb(Fb)) * 32.0) - 1.0) * sign(Fb);
}

const uint Jb = 1024u;

bool Kb(uint J)
{
	uint h = I(J).y;
	uint Lb = D(A + h);
	uint kb = Lb & 3u;

	if (kb != c)
	{
		return false;
	}

	float Mb = Ib(texelFetch(_50099FB73CB57A9E, ivec2(0, 0), 0).r);
	vec2 Nb = vec2(0, Mb);

vec3 Ob = G(A + 0u);

	Cb = dot(Ob, _953A2A72B2E8B0E9) + _30B9E4384BFDB31C;

	float Pb = clamp((Cb - Nb.x) / (Nb.y - Nb.x), 0.0, 1.0);
	Db = uint(Pb * float(Jb - 1u));

	return true;
}

void main() {

	float Qb = 3.402823466e+38;
	float Rb = -3.402823466e+38;

	uint Sb;

	uint Tb = 1u;

	if (Kb(uint(gl_VertexID) * 2u))
	{
		Qb = Cb;
		Rb = Cb;
		Sb = Db;
		Tb = 0u;
	}
	else
	{
		Sb = Jb;
	}

	uint Ub = uint(gl_VertexID) * 2u + 1u;
	if (Ub < _14DB390EE2887C41)
	{
		if (Kb(Ub))
		{
			Qb = min(Qb, Cb);
			Rb = max(Rb, Cb);
			Sb |= (Db << 16);
			Tb = 0u;
		}
		else
		{
			Sb |= (Jb << 16);
		}
	}

	_5A57BFB62CAEDE57 = Sb;

	_9BC2E3181A315B34 = Hb(Rb);

	gl_Position = vec4(-2.0 * float(Tb), 0.0, 0.0, 1.0);
	gl_PointSize = 1.0;
}

#endif

#ifdef _A9EE343B6D5FED1F

flat in float _9BC2E3181A315B34;

out vec4 _EB400D689750048A;

void main()
{
	_EB400D689750048A = vec4(0, 0, 0, 0);
	gl_FragDepth = _9BC2E3181A315B34;
}

#endif

#ifdef _B970C3D2AD57F959

layout (std140) uniform _F52D3B1AA302F088
{
    vec3 Vb;
    float Wb;
    vec3 Xb;
    uint Yb;
    vec3 _953A2A72B2E8B0E9;
    uint Zb;
    vec3 _b;
    float ac;
    mat4 bc;
};

uniform float _618E8A543B8FC66E;

uniform mat4 _4F2DACEFFBDAB8D1;

vec4 cc(vec4 dc)
{
	return _618E8A543B8FC66E < 0.0 ? dc : vec4(dc.rgb * _618E8A543B8FC66E, dc.a);
}

smooth out vec4 _203C8330B731DD85;
flat out uint _94B3AA5995C3B5A1;

vec4 ec = vec4(0, 0, 1, 1);

uniform uint _94426D1BFF511BB0;

in uint _4D7C1BF9E36F0C6C;

uvec2 fc()
{
	return uvec2(_4D7C1BF9E36F0C6C >> 8, _4D7C1BF9E36F0C6C & 0xFFu);
}

smooth out vec2 _BEAD44E72AA5013D;

float[6] gc = float[6](-1.0,   -1.0,   -1.0,   0.0,   0.0,    0.0);
float[6] hc = float[6](0.0,    0.0,    -1.0,    0.0,   -1.0,   -1.0);

float[6] ic = float[6](0.0, 0.0,    0.0,    1.0,    1.0,    1.0);
float[6] jc = float[6](1.0, 1.0,    0.0,    1.0,    0.0,    0.0);

vec2[6] kc = vec2[6](
	vec2(0.0, 1.0), vec2(0.0, 1.0), vec2(0.0, 0.0), vec2(1.0, 1.0), vec2(1.0, 0.0), vec2(1.0, 0.0)
	);

void lc(uint mc, vec3 Ob, vec2 nc, vec3 oc, vec3 pc, 
    vec4 dc, vec4 qc)
{
	_203C8330B731DD85 = cc(dc);

	vec2 rc = vec2(1, 1) - nc;

	vec2 sc = vec2(gc[mc], hc[mc]) * nc
		+ vec2(ic[mc], jc[mc]) * rc;

	vec3 tc = sc.x * oc + sc.y * pc;

    mat4 uc = bc * _4F2DACEFFBDAB8D1;

	gl_Position = uc * vec4(Ob + tc, 1.0);
	
	_BEAD44E72AA5013D = kc[mc] * qc.zw + qc.xy;
}

void vc(uint mc, vec3 Ob, vec2 nc, float wc, vec2 xc, vec4 dc, vec4 qc)
{
    vec3 yc = _b;
    vec3 zc = Xb;

    float s = z(wc);
    float Ac = -sin(s);
    float Bc = cos(s);

    vec3 oc = vec3(
        yc.x * Bc + zc.x * Ac,
        yc.y * Bc + zc.y * Ac,
        yc.z * Bc + zc.z * Ac);

    vec3 pc = vec3(
        -yc.x * Ac + zc.x * Bc,
        -yc.y * Ac + zc.y * Bc,
        -yc.z * Ac + zc.z * Bc);

    lc(mc, Ob, nc, oc * xc.x, pc * xc.y, dc, qc);
}

void Cc()
{
	gl_Position = vec4(-2, -2, 0, 1);
}

void Dc(uint Ec, uint mc)
{
	e ab = K(Ec - 0u);
	uint cb = ab.f;
	uint db = ab.f + ab.g;

	vec3 Ob = G(A + 0u);
	float Y = B(A + 8u);
	float nb = 30.0;
	_94B3AA5995C3B5A1 = 0u;
	vc(mc, G(A + 0u), vec2(0.5, 0.5), Y, vec2(nb, nb), vec4(vec3(1.0, 1.0, 1.0), 1.0), ec);
}

void main() {
	uvec2 Fc = fc();
	uint J = Fc.x;
	uint mc = Fc.y;

	uvec2 Gc = I(J);
	uint Hc = Gc.x;
	uint h = Gc.y;
	uint Lb = D(A + h);
	uint kb = Lb & 3u;

	if (kb != c)
	{
		Cc();
		return;
	}

	if (((Lb >> 2u) & 3u) < _94426D1BFF511BB0)
	{
		Cc();
		return;
	}
	switch (Hc)
	{
	case 0u: Dc(J, mc); break;
	}
}

#endif 

#ifdef _DC66F57E96C6C75C

uniform float _618E8A543B8FC66E;

flat in uint _94B3AA5995C3B5A1;
smooth in vec4 _203C8330B731DD85;

out vec4 _EB400D689750048A;

smooth in vec2 _BEAD44E72AA5013D;

vec4 Ic(sampler2D Jc, mat3x2 Kc)
{
	return texture(Jc, Kc * vec3(_BEAD44E72AA5013D, 1))* _203C8330B731DD85;
}

void makeFragColor(sampler2D Jc, mat3x2 Kc)
{
	vec4 Lc = Ic(Jc, Kc);

#ifdef _6335F91D31E2CC83
	Lc.rgb = Lc.rgb * _203C8330B731DD85.a;
#endif

	_EB400D689750048A = _618E8A543B8FC66E < 0.0 ?
		vec4(mix(vec3(1, 1, 1), Lc.rgb, Lc.a), 1) :
		Lc;
}

in makeFragColor()

#endif

