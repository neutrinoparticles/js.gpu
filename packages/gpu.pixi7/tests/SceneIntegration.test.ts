import { ibct, setupIbctHooks } from './ibct';

describe('SceneIntegration', () => {
  setupIbctHooks();

  ['DepthSorted', 'NotSorted'].forEach((folderName) => {

    describe(folderName, () => {
      ibct('SpriteOnBack', `
        promise(async (resolve) => {
          const screenshots = [];
    
          // Sprite on background
          {
              const bunny = PIXI.Sprite.from('${globalThis.rootUrl + '/tests/textures/default_inv.png'}');
              bunny.anchor.set(0.5);
              bunny.x = app.screen.width / 2;
              bunny.y = app.screen.height / 2;
              bunny.scale = new PIXI.Point(2, 2);
              app.stage.addChild(bunny);
          }

          const updateGroup = new NPGPUPixi.EffectsUpdateGroup(app.neutrinoGPU);
          const effect = await loadEffect('effects/SceneIntegration/${folderName}/FixedFPS.shader');
    
          app.stage.addChild(effect);
    
          effect.start({
            position: [app.screen.width / 2, app.screen.height / 2, 0],
          });
    
          // Add the effect to the update group to update all effects at once
          updateGroup.add(effect);
    
          let frameIndex = 0;
          const update = () => {
            updateGroup.update(1.0/10.0);
            app.renderer.render(app.stage);
    
            if (++frameIndex > 10)
            {
              app.renderer.gl.finish();
              screenshots.push(app.view.toDataURL());
            }
    
            if (screenshots.length === 5)
              resolve(screenshots);
            else
              requestAnimationFrame(update);
          }      
    
          requestAnimationFrame(update);      
        })`,
        () => {
          expect(globalThis.warnings.length).toBeLessThan(4);
        }
      );

      // This test shows a case when frame time is less then fixed FPS of the effect,
      // so there are frames when there is no effect's update made at all.
      ibct('SpriteOnBackWithFastFrame', `
        promise(async (resolve) => {
          const screenshots = [];
          
          const updateGroup = new NPGPUPixi.EffectsUpdateGroup(app.neutrinoGPU);
          const effect = await loadEffect('effects/SceneIntegration/${folderName}/FixedFPS.shader');
    
          // Sprite on background
          {
              const bunny = PIXI.Sprite.from('${globalThis.rootUrl + '/tests/textures/default_inv.png'}');
              bunny.anchor.set(0.5);
              bunny.x = app.screen.width / 2;
              bunny.y = app.screen.height / 2;
              bunny.scale = new PIXI.Point(2, 2);
              app.stage.addChild(bunny);
          }

          app.stage.addChild(effect);
    
          effect.start({
            position: [app.screen.width / 2, app.screen.height / 2, 0],
          });
    
          // Add the effect to the update group to update all effects at once
          updateGroup.add(effect);
    
          let frameIndex = 0;
          const update = () => {
            updateGroup.update(1.0/60.0);
            app.renderer.render(app.stage);
    
            if (++frameIndex > 60)
            {
              app.renderer.gl.finish();
              screenshots.push(app.view.toDataURL());
            }
    
            if (screenshots.length === 5)
              resolve(screenshots);
            else
              requestAnimationFrame(update);
          }      
    
          requestAnimationFrame(update);      
        })`,
        () => {
          expect(globalThis.warnings.length).toBeLessThan(4);
        }
      );

      // Zero frame update
      ibct('SpriteOnBackWithZeroFrame', `
        promise(async (resolve) => {
          const screenshots = [];
          
          const updateGroup = new NPGPUPixi.EffectsUpdateGroup(app.neutrinoGPU);
          const effect = await loadEffect('effects/SceneIntegration/${folderName}/FixedFPS.shader');
    
          // Sprite on background
          const bunny = PIXI.Sprite.from('${globalThis.rootUrl + '/tests/textures/default_inv.png'}');
          bunny.anchor.set(0.5);
          bunny.x = app.screen.width / 2;
          bunny.y = app.screen.height / 2;
          bunny.scale = new PIXI.Point(2, 2);
          app.stage.addChild(bunny);

          app.stage.addChild(effect);
    
          effect.start({
            position: [app.screen.width / 2, app.screen.height / 2, 0],
          });
    
          // Add the effect to the update group to update all effects at once
          updateGroup.add(effect);
    
          let frameIndex = 0;
          const update = () => {
            updateGroup.update(0); // <<<<<<
            app.renderer.render(app.stage);

            if (bunny.texture.baseTexture.valid)
            {
              app.renderer.gl.finish();
              screenshots.push(app.view.toDataURL());
            }
    
            if (screenshots.length === 5)
              resolve(screenshots);
            else
              requestAnimationFrame(update);
          }      
    
          requestAnimationFrame(update);      
        })`,
        () => {
          expect(globalThis.warnings.length).toBeLessThan(4);
        }
      );

      // Zero frame update
      ibct('SpriteOnBackWithNoUpdate', `
        promise(async (resolve) => {
          const screenshots = [];
          
          const updateGroup = new NPGPUPixi.EffectsUpdateGroup(app.neutrinoGPU);
          const effect = await loadEffect('effects/SceneIntegration/${folderName}/FixedFPS.shader');
    
          // Sprite on background
          const bunny = PIXI.Sprite.from('${globalThis.rootUrl + '/tests/textures/default_inv.png'}');
          bunny.anchor.set(0.5);
          bunny.x = app.screen.width / 2;
          bunny.y = app.screen.height / 2;
          bunny.scale = new PIXI.Point(2, 2);
          app.stage.addChild(bunny);

          app.stage.addChild(effect);
    
          effect.start({
            position: [app.screen.width / 2, app.screen.height / 2, 0],
          });
    
          // Add the effect to the update group to update all effects at once
          updateGroup.add(effect);
    
          let frameIndex = 0;
          const update = () => {
            //updateGroup.update(0); // <<<<<<
            app.renderer.render(app.stage);

            if (bunny.texture.baseTexture.valid)
            {
              app.renderer.gl.finish();
              screenshots.push(app.view.toDataURL());
            }
    
            if (screenshots.length === 5)
              resolve(screenshots);
            else
              requestAnimationFrame(update);
          }      
    
          requestAnimationFrame(update);      
        })`,
        () => {
          expect(globalThis.warnings.length).toBeLessThan(4);
        }
      );

      ibct('SpriteOnBackAndFront', `
        promise(async (resolve) => {
          const screenshots = [];
    
          // Sprite on background
          {
              const bunny = PIXI.Sprite.from('${globalThis.rootUrl + '/tests/textures/default_inv.png'}');
              bunny.anchor.set(0.5);
              bunny.x = app.screen.width / 2;
              bunny.y = app.screen.height / 2;
              bunny.scale = new PIXI.Point(2, 2);
              app.stage.addChild(bunny);
          }

          const updateGroup = new NPGPUPixi.EffectsUpdateGroup(app.neutrinoGPU);
          const effect = await loadEffect('effects/SceneIntegration/${folderName}/FixedFPS.shader');
    
          app.stage.addChild(effect);
    
          effect.start({
            position: [app.screen.width / 2, app.screen.height / 2, 0],
          });
    
          // Add the effect to the update group to update all effects at once
          updateGroup.add(effect);
    
          // Sprite on foreground
          {
              const bunny = PIXI.Sprite.from('${globalThis.rootUrl + '/tests/textures/default_inv.png'}');
              bunny.anchor.set(0.5);
              bunny.x = app.screen.width / 2;
              bunny.y = app.screen.height / 2;
              bunny.scale = new PIXI.Point(0.5, 0.5);
              app.stage.addChild(bunny);
          }

          let frameIndex = 0;
          const update = () => {
            updateGroup.update(1.0/10.0);
            app.renderer.render(app.stage);
    
            if (++frameIndex > 10)
            {
              app.renderer.gl.finish();
              screenshots.push(app.view.toDataURL());
            }
    
            if (screenshots.length === 5)
              resolve(screenshots);
            else
              requestAnimationFrame(update);
          }      
    
          requestAnimationFrame(update);      
        })`,
        () => {
          expect(globalThis.warnings.length).toBeLessThan(4);
        }
      );

      // This test shows a case when frame time is less then fixed FPS of the effect,
      // so there are frames when there is no effect's update made at all.
      ibct('SpriteOnBackAndFrontWithFastFrame', `
        promise(async (resolve) => {
          const screenshots = [];
          
          const updateGroup = new NPGPUPixi.EffectsUpdateGroup(app.neutrinoGPU);
          const effect = await loadEffect('effects/SceneIntegration/${folderName}/FixedFPS.shader');
    
          // Sprite on background
          {
              const bunny = PIXI.Sprite.from('${globalThis.rootUrl + '/tests/textures/default_inv.png'}');
              bunny.anchor.set(0.5);
              bunny.x = app.screen.width / 2;
              bunny.y = app.screen.height / 2;
              bunny.scale = new PIXI.Point(2, 2);
              app.stage.addChild(bunny);
          }

          app.stage.addChild(effect);
    
          effect.start({
            position: [app.screen.width / 2, app.screen.height / 2, 0],
          });
    
          // Add the effect to the update group to update all effects at once
          updateGroup.add(effect);
    
          // Sprite on foreground
          {
              const bunny = PIXI.Sprite.from('${globalThis.rootUrl + '/tests/textures/default_inv.png'}');
              bunny.anchor.set(0.5);
              bunny.x = app.screen.width / 2;
              bunny.y = app.screen.height / 2;
              bunny.scale = new PIXI.Point(0.5, 0.5);
              app.stage.addChild(bunny);
          }

          let frameIndex = 0;
          const update = () => {
            updateGroup.update(1.0/60.0);
            app.renderer.render(app.stage);
    
            if (++frameIndex > 60)
            {
              app.renderer.gl.finish();
              screenshots.push(app.view.toDataURL());
            }
    
            if (screenshots.length === 5)
              resolve(screenshots);
            else
              requestAnimationFrame(update);
          }      
    
          requestAnimationFrame(update);      
        })`,
        () => {
          expect(globalThis.warnings.length).toBeLessThan(4);
        }
      );

      // Zero frame update
      ibct('SpriteOnBackAndFrontWithZeroFrame', `
        promise(async (resolve) => {
          const screenshots = [];
          
          const updateGroup = new NPGPUPixi.EffectsUpdateGroup(app.neutrinoGPU);
          const effect = await loadEffect('effects/SceneIntegration/${folderName}/FixedFPS.shader');
    
          // Sprite on background
          const bunny = PIXI.Sprite.from('${globalThis.rootUrl + '/tests/textures/default_inv.png'}');
          bunny.anchor.set(0.5);
          bunny.x = app.screen.width / 2;
          bunny.y = app.screen.height / 2;
          bunny.scale = new PIXI.Point(2, 2);
          app.stage.addChild(bunny);

          app.stage.addChild(effect);
    
          effect.start({
            position: [app.screen.width / 2, app.screen.height / 2, 0],
          });
    
          // Add the effect to the update group to update all effects at once
          updateGroup.add(effect);
    
          // Sprite on foreground
          {
              const bunny = PIXI.Sprite.from('${globalThis.rootUrl + '/tests/textures/default_inv.png'}');
              bunny.anchor.set(0.5);
              bunny.x = app.screen.width / 2;
              bunny.y = app.screen.height / 2;
              bunny.scale = new PIXI.Point(0.5, 0.5);
              app.stage.addChild(bunny);
          }

          let frameIndex = 0;
          const update = () => {
            updateGroup.update(0); // <<<<<<
            app.renderer.render(app.stage);

            if (bunny.texture.baseTexture.valid)
            {
              app.renderer.gl.finish();
              screenshots.push(app.view.toDataURL());
            }
    
            if (screenshots.length === 5)
              resolve(screenshots);
            else
              requestAnimationFrame(update);
          }      
    
          requestAnimationFrame(update);      
        })`,
        () => {
          expect(globalThis.warnings.length).toBeLessThan(4);
        }
      );

      // Zero frame update
      ibct('SpriteOnBackAndFrontWithNoUpdate', `
        promise(async (resolve) => {
          const screenshots = [];
          
          const updateGroup = new NPGPUPixi.EffectsUpdateGroup(app.neutrinoGPU);
          const effect = await loadEffect('effects/SceneIntegration/${folderName}/FixedFPS.shader');
    
          // Sprite on background
          const bunny = PIXI.Sprite.from('${globalThis.rootUrl + '/tests/textures/default_inv.png'}');
          bunny.anchor.set(0.5);
          bunny.x = app.screen.width / 2;
          bunny.y = app.screen.height / 2;
          bunny.scale = new PIXI.Point(2, 2);
          app.stage.addChild(bunny);

          app.stage.addChild(effect);
    
          effect.start({
            position: [app.screen.width / 2, app.screen.height / 2, 0],
          });
    
          // Add the effect to the update group to update all effects at once
          updateGroup.add(effect);
    
          // Sprite on foreground
          {
              const bunny = PIXI.Sprite.from('${globalThis.rootUrl + '/tests/textures/default_inv.png'}');
              bunny.anchor.set(0.5);
              bunny.x = app.screen.width / 2;
              bunny.y = app.screen.height / 2;
              bunny.scale = new PIXI.Point(0.5, 0.5);
              app.stage.addChild(bunny);
          }

          let frameIndex = 0;
          const update = () => {
            //updateGroup.update(0); // <<<<<<
            app.renderer.render(app.stage);

            if (bunny.texture.baseTexture.valid)
            {
              app.renderer.gl.finish();
              screenshots.push(app.view.toDataURL());
            }
    
            if (screenshots.length === 5)
              resolve(screenshots);
            else
              requestAnimationFrame(update);
          }      
    
          requestAnimationFrame(update);      
        })`,
        () => {
          expect(globalThis.warnings.length).toBeLessThan(4);
        }
      );

      ibct('SpriteOnFront', `
        promise(async (resolve) => {
          const screenshots = [];
    
          const updateGroup = new NPGPUPixi.EffectsUpdateGroup(app.neutrinoGPU);
          const effect = await loadEffect('effects/SceneIntegration/${folderName}/FixedFPS.shader');
    
          app.stage.addChild(effect);
    
          effect.start({
            position: [app.screen.width / 2, app.screen.height / 2, 0],
          });
    
          // Add the effect to the update group to update all effects at once
          updateGroup.add(effect);
    
          // Sprite on background
          {
              const bunny = PIXI.Sprite.from('${globalThis.rootUrl + '/tests/textures/default_inv.png'}');
              bunny.anchor.set(0.5);
              bunny.x = app.screen.width / 2;
              bunny.y = app.screen.height / 2;
              bunny.scale = new PIXI.Point(0.5, 0.5);
              app.stage.addChild(bunny);
          }

          let frameIndex = 0;
          const update = () => {
            updateGroup.update(1.0/10.0);
            app.renderer.render(app.stage);
    
            if (++frameIndex > 10)
            {
              app.renderer.gl.finish();
              screenshots.push(app.view.toDataURL());
            }
    
            if (screenshots.length === 5)
              resolve(screenshots);
            else
              requestAnimationFrame(update);
          }      
    
          requestAnimationFrame(update);      
        })`,
        () => {
          expect(globalThis.warnings.length).toBeLessThan(4);
        }
      );

      // This test shows a case when frame time is less then fixed FPS of the effect,
      // so there are frames when there is no effect's update made at all.
      ibct('SpriteOnFrontWithFastFrame', `
        promise(async (resolve) => {
          const screenshots = [];
          
          const updateGroup = new NPGPUPixi.EffectsUpdateGroup(app.neutrinoGPU);
          const effect = await loadEffect('effects/SceneIntegration/${folderName}/FixedFPS.shader');

          app.stage.addChild(effect);
    
          effect.start({
            position: [app.screen.width / 2, app.screen.height / 2, 0],
          });
    
          // Add the effect to the update group to update all effects at once
          updateGroup.add(effect);
    
          // Sprite on foreground
          {
              const bunny = PIXI.Sprite.from('${globalThis.rootUrl + '/tests/textures/default_inv.png'}');
              bunny.anchor.set(0.5);
              bunny.x = app.screen.width / 2;
              bunny.y = app.screen.height / 2;
              bunny.scale = new PIXI.Point(0.5, 0.5);
              app.stage.addChild(bunny);
          }

          let frameIndex = 0;
          const update = () => {
            updateGroup.update(1.0/60.0);
            app.renderer.render(app.stage);
    
            if (++frameIndex > 60)
            {
              app.renderer.gl.finish();
              screenshots.push(app.view.toDataURL());
            }
    
            if (screenshots.length === 5)
              resolve(screenshots);
            else
              requestAnimationFrame(update);
          }      
    
          requestAnimationFrame(update);      
        })`,
        () => {
          expect(globalThis.warnings.length).toBeLessThan(4);
        }
      );

      // Zero frame update
      ibct('SpriteOnFrontWithZeroFrame', `
        promise(async (resolve) => {
          const screenshots = [];
          
          const updateGroup = new NPGPUPixi.EffectsUpdateGroup(app.neutrinoGPU);
          const effect = await loadEffect('effects/SceneIntegration/${folderName}/FixedFPS.shader');
    
          app.stage.addChild(effect);
    
          effect.start({
            position: [app.screen.width / 2, app.screen.height / 2, 0],
          });
    
          // Add the effect to the update group to update all effects at once
          updateGroup.add(effect);
    
          // Sprite on foreground
          
          const bunny = PIXI.Sprite.from('${globalThis.rootUrl + '/tests/textures/default_inv.png'}');
          bunny.anchor.set(0.5);
          bunny.x = app.screen.width / 2;
          bunny.y = app.screen.height / 2;
          bunny.scale = new PIXI.Point(0.5, 0.5);
          app.stage.addChild(bunny);
          
          let frameIndex = 0;
          const update = () => {
            updateGroup.update(0); // <<<<<<
            app.renderer.render(app.stage);

            if (bunny.texture.baseTexture.valid)
            {
              app.renderer.gl.finish();
              screenshots.push(app.view.toDataURL());
            }
    
            if (screenshots.length === 5)
              resolve(screenshots);
            else
              requestAnimationFrame(update);
          }      
    
          requestAnimationFrame(update);      
        })`,
        () => {
          expect(globalThis.warnings.length).toBeLessThan(4);
        }
      );

      // Zero frame update
      ibct('SpriteOnFrontWithNoUpdate', `
        promise(async (resolve) => {
          const screenshots = [];
          
          const updateGroup = new NPGPUPixi.EffectsUpdateGroup(app.neutrinoGPU);
          const effect = await loadEffect('effects/SceneIntegration/${folderName}/FixedFPS.shader');
    
          app.stage.addChild(effect);
    
          effect.start({
            position: [app.screen.width / 2, app.screen.height / 2, 0],
          });
    
          // Add the effect to the update group to update all effects at once
          updateGroup.add(effect);
    
          // Sprite on foreground
          const bunny = PIXI.Sprite.from('${globalThis.rootUrl + '/tests/textures/default_inv.png'}');
          bunny.anchor.set(0.5);
          bunny.x = app.screen.width / 2;
          bunny.y = app.screen.height / 2;
          bunny.scale = new PIXI.Point(0.5, 0.5);
          app.stage.addChild(bunny);
          
          let frameIndex = 0;
          const update = () => {
            //updateGroup.update(0); // <<<<<<
            app.renderer.render(app.stage);

            if (bunny.texture.baseTexture.valid)
            {
              app.renderer.gl.finish();
              screenshots.push(app.view.toDataURL());
            }
    
            if (screenshots.length === 5)
              resolve(screenshots);
            else
              requestAnimationFrame(update);
          }      
    
          requestAnimationFrame(update);      
        })`,
        () => {
          expect(globalThis.warnings.length).toBeLessThan(4);
        }
      );
    }); // describe
  }); // forEach
  
});
