import { ibct, setupIbctHooks } from './ibct';

describe('BlendModes', () => {
  setupIbctHooks();

  ibct('Normal', `
    promise(async (resolve) => {
      app.renderer.backgroundColor = 0xffffff;
      const screenshots = [];

      const updateGroup = new NPGPUPixi.EffectsUpdateGroup(app.neutrinoGPU);
      const effect = await loadEffect('effects/BlendMode/Normal.shader');

      app.stage.addChild(effect);

      effect.start({
        position: [app.screen.width / 2, app.screen.height / 2, 0],
      });

      // Add the effect to the update group to update all effects at once
      updateGroup.add(effect);

      updateGroup.update(1);
      app.renderer.render(app.stage);
      app.renderer.gl.finish();
      screenshots.push(app.view.toDataURL());

      resolve(screenshots);
    })`
  );

  ibct('Add', `
    promise(async (resolve) => {
      app.renderer.backgroundColor = 0x808080;
      const screenshots = [];

      const updateGroup = new NPGPUPixi.EffectsUpdateGroup(app.neutrinoGPU);
      const effect = await loadEffect('effects/BlendMode/Add.shader');

      app.stage.addChild(effect);

      effect.start({
        position: [app.screen.width / 2, app.screen.height / 2, 0],
      });

      // Add the effect to the update group to update all effects at once
      updateGroup.add(effect);

      updateGroup.update(1);
      app.renderer.render(app.stage);
      app.renderer.gl.finish();
      screenshots.push(app.view.toDataURL());

      resolve(screenshots);
    })`
  );

  ibct('Multiply', `
    promise(async (resolve) => {
      app.renderer.backgroundColor = 0x808080;
      const screenshots = [];

      const updateGroup = new NPGPUPixi.EffectsUpdateGroup(app.neutrinoGPU);
      const effect = await loadEffect('effects/BlendMode/Multiply.shader');

      app.stage.addChild(effect);

      effect.start({
        position: [app.screen.width / 2, app.screen.height / 2, 0],
      });

      // Add the effect to the update group to update all effects at once
      updateGroup.add(effect);

      updateGroup.update(2);
      app.renderer.render(app.stage);
      app.renderer.gl.finish();
      screenshots.push(app.view.toDataURL());

      resolve(screenshots);
    })`
  );
  
});
