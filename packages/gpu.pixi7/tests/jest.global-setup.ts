// Only firefox shutdowns correctly. chromium hangs on close.
import { chromium } from 'playwright';
import { default as Server }  from './server';

const port = 8080;

export default async () => {
  await Server.start(8080, '../..');

  globalThis.browser = await chromium.launch({headless: false});
  globalThis.context = await browser.newContext();

  globalThis.warnings = new Array<string>();
  globalThis.errors = new Array<string>();

  globalThis.rootUrl = `http://localhost:${port}/packages/gpu.pixi7/`;
};