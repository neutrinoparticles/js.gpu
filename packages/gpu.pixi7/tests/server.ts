// server.ts
import express, { Application } from 'express';
import path from 'path';

let server: ReturnType<Application['listen']> | null = null;

interface ServerModule {
  start: (port: number, rootDir: string) => Promise<void>;
  stop: () => Promise<void>;
}

const start = async (port: number, rootDir: string): Promise<void> => {
  const app: Application = express();

  const absoluteRootDir = path.resolve(rootDir);
  app.use(express.static(absoluteRootDir));

  return new Promise((resolve, reject) => {
    server = app.listen(port, () => {
      console.log(`Server is started on port ${port}.`);
      resolve();
    });

    server.on('error', (error: Error) => {
      console.error('Error running the server:', error);
      reject(error);
    });
  });
};

const stop = async (): Promise<void> => {
  return new Promise((resolve, reject) => {
    if (server) {
      server.close((err?: Error) => {
        if (err) {
          console.error('Error stopping the server:', err);
          return reject(err);
        }
        console.log('Server is stopped.');
        server = null;
        resolve();
      });
    } else {
      resolve();
    }
  });
};

const serverModule: ServerModule = {
  start,
  stop,
};

export default serverModule;
