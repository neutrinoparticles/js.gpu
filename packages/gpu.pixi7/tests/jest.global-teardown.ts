import { default as Server }  from './server';

export default async () => {
  await globalThis.context.close();
  await globalThis.browser.close();
  Server.stop();
};