import { defineConfig } from 'vite';
import path from 'path';
import { exec } from 'child_process';

const postBuild = () => {
  exec('npm run postbuild', (err, stdout, stderr) => {
    console.log(`=== POST BUILD BEGIN ===`);
    if (err) {
      console.error(`Error: ${err.message}`);
      return;
    }
    if (stderr) {
      console.error(`Stderr: ${stderr}`);
      return;
    }
    console.log(`${stdout}`);
    console.log(`=== POST BUILD END ===`);
  });
}

export default defineConfig(({ mode }) => {
  const isDevelopment = mode === 'dev';
  return {
    build: {
      sourcemap: true,
      minify: !isDevelopment,
      lib: {
        entry: path.resolve(__dirname, 'src/index.ts'),
        name: 'NPGPUPixi',
        fileName: (format) => `neutrinoparticles.gpu.pixi7.${format}.js`,
        formats: ['es', 'cjs', 'umd'] 
      },
      rollupOptions: {
        external: ['pixi.js'], 
        output: {
          globals: {
            'pixi.js': 'PIXI',
          }
        }
      }
    },
    plugins: [
      {
        name: 'postbuild',
        //handleHotUpdate: async () => {  },
        buildStart: async () => { postBuild(); },
      }
    ],
  }
});
