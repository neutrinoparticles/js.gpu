# neutrinoparticles.gpu.pixi7

Цей пекедж являє собою інтеграцію ефектів [NeutrinoParticles](https://neutrinoparticles.com/) у графічний рушій **`PIXI.js v7`**.

![Bunny in portal](./img/bunny_in_portal.gif)

[**NeutrinoParticles**](https://neutrinoparticles.com/) - це універсальне рішення для створення та рендеру партікл ефектів для різних платформ та рушіїв. Ефекти експортуються у вигляді вихідного коду у різні мови програмування. Далі, цей код компілюється, оптимізується та збирається разом з додатком, що забеспечує максимальну продуктивність.

[**neutrinoparticles.gpu.(js|wasm)**](../gpu.js/README.md) - це рендерер для ефектів експортованих у спеціальній версії редактору `NeutrinoParticles Editor GPU`. У цьому редакторі ефекти експортуються у вихідний код GLSL, який використовується рендерером `neutrinoparticles.gpu.(js|wasm)` для апдейту та рендеру ефектів ресурсами відеокарти, тим самим звільнюючи CPU для інших завдань.

Цей пекедж `neutrinoparticles.gpu.pixi7` інтегрує [neutrinoparticles.gpu.(js|wasm)](../gpu.js/README.md) у рушій PIXI.js версії 7.

## Що я отримаю?
Ефекти з повним GPU прискоренням у сцені графічного рушія PIXI.js. Сотні тисяч партіклів у браузерах на десктопах та від десятків до сотень тисяч на браузерах мобільних пристроїв.

Можливість створювати фантастично складні та красиві ефекти у редакторі NeutrinoParticles та використовувати їх в іграх та додатках на PIXI.js.

## Що таке повне GPU прискорення для ефектів?
Ефекти не тільки рендеряться, а ще й апдейтяться за допомогою відеокарти (GPU - Graphics Processing Unit). Це звільнює основний процесор (CPU - Central Processing Unit) для інших задач у грі та дозволяє обробляти у 100 разів більше партіклів у ефектах.

## Яке залізо треба?
Будь яке, аби браузер підтримував WebGL 2.

> Для ефекту повноекранного сяйва [Bloom](../gpu.pixi7.samples/README.ua.md#bloom) потрібно, щоб браузер підтримував [FLOAT текстури](https://web3dsurvey.com/webgl2/extensions/EXT_color_buffer_float) (99.0% девайсів) та  [FLOAT Blend](https://web3dsurvey.com/webgl2/extensions/EXT_float_blend) (89.58% девайсів). 

## Як встановити?
Треба два пекеджи:
1. `@neutrinoparticles/gpu.pixi7` для інтеграції в PIXI
1. `@neutrinoparticles/gpu.js` або `@neutrinoparticles/gpu.wasm` для вибору бібліотеки рендереру. Перша на чистому JS, а друга на WebAssembly. 

Тобто, в терміналі буде щось типу:
```bash
npm install @neutrinoparticles/gpu.pixi7 @neutrinoparticles/gpu.wasm
```

## Швидкий приклад
```typescript
import { Effect, EffectsUpdateGroup } from '@neutrinoparticles/gpu.pixi7';

// Pick one of two following imports to choose between JS and WASM
//import { default as npgpuLoader } from '@neutrinoparticles/gpu.js';
import { default as npgpuLoader } from '@neutrinoparticles/gpu.wasm';

import * as PIXI from 'pixi.js';

// Creating PIXI Application
const app = new PIXI.Application({ background: '#1099bb', resizeTo: window,
    neutrinoGPU: { // Additional options for NeutrinoParticles Context
        npgpuLoader, // Mandatory. NeutrinoParticles GPU library loader
		texturesBasePath: 'textures/', // Base path for textures. Will be prefixed to exported texture paths in effects
	}
 });

// Wait for GPU library loaded
await app.neutrinoGPU!.loadedPromise;

document.body.appendChild(app.view as HTMLCanvasElement);

PIXI.Assets.addBundle('resources', [
    // Request loading effect in the bundle
	{ 
        name: 'testEffect', // You choose a name of EffectModel in the bundle
        srcs: 'src/shaders/Test.shader', // Path to the effect
        data: app.neutrinoGPU!.loadData // Mark this resource as NeutrinoParticles effect
    },
]);

// Update group is for updating effects in a batch. It works faster.
const updateGroup = new EffectsUpdateGroup(app.neutrinoGPU!);

PIXI.Assets.loadBundle('resources').then((resources) => {

    // Bunny sprite on background
    {
        const bunny = PIXI.Sprite.from('https://pixijs.com/assets/bunny.png');
        bunny.anchor.set(0.5);
        bunny.x = app.screen.width / 2;
        bunny.y = app.screen.height / 2;
        bunny.scale = new PIXI.Point(20, 20);
        app.stage.addChild(bunny);
    }

    // Create effect
    {
		// Create instance of the effect
        const effect = new Effect(resources.testEffect);

        // Add the effect to the scene graph
        app.stage.addChild(effect);

        // Start the effect. Should be called after it is added to the scene graph
        // to calculate world position correctly.
        effect.start({
            position: [app.screen.width / 2, app.screen.height / 2, 0]
        })

		// Add it to the update group to update all effects at once
        updateGroup.add(effect);
    }

    // Listen for update
    app.ticker.add((_delta) =>
    {
		// Update effects. Better in a group.
        updateGroup.update(app.ticker.elapsedMS / 1000.0);
    });
});
```

## Більше прикладів
У [**neutrinoparticles.gpu.pixi7.samples**](../gpu.pixi7.samples/README.md) ви знайдете багатий вибір прикладів використання на всі можливі випадки.

## Переклад англійською
Для ChatGPT o1-mini:
> Перклади простою англійською мовою, тільки в посиланнях заміни README.ua.md на README.md. Результат видай у вигляді похідного .md коду, щоб можна було скопіювати