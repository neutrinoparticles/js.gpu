import { Context } from "./Context";
import { Effect } from "./Effect"

export class EffectsUpdateGroup
{
    public readonly context: Context;

    private effects: Set<Effect>;

    constructor(context: Context, effects?: Array<Effect>) {
        this.context = context;
        
        this.effects = new Set<Effect>();

        if (effects) {
            effects!.forEach((effect) => this.add(effect));
        }
    }

    public static updateOne(context: Context, effect: Effect, seconds: number) : void {
        EffectsUpdateGroup._beforeUpdate(context);
        effect.update(seconds);
        EffectsUpdateGroup._afterUpdate(context);
    }

    add(effect: Effect) : void {
        this.effects.add(effect);
    }

    remove(effect: Effect) : void {
        this.effects.delete(effect);
    }

    update(seconds: number) : void {
        if (this.effects.size == 0)
           return;

        EffectsUpdateGroup._beforeUpdate(this.context);
        this.effects.forEach((effect) => effect.update(seconds));
        EffectsUpdateGroup._afterUpdate(this.context);
    }

    private static _beforeUpdate(context: Context) : void {
        const gl = context.gl;
        
        // These two are not allowed when uploading from PBO (inside the library).
		// PIXI sets them if necessary for textures upload.
        // TODO: do we need to restore previous settings?
		gl.pixelStorei(gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, false);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, false);

        context.stateChanger.startGather();

		context.pushViewportSettings();
		context.gpu33Context.beginUpdate();
    }

    private static _afterUpdate(context: Context) : void {
        context.gpu33Context.endUpdate();
		context.popViewportSettings();

        context.stateChanger.end();
    }
}