import { Context } from './Context';
import { EffectModel } from './EffectModel';
import { Container, DisplayObject, Renderer, Transform } from 'pixi.js';
import * as NPGPU from "@neutrinoparticles/gpu.types";
import * as glMatrix from 'gl-matrix';

let _matrix1: NPGPU.Mat4 = glMatrix.mat4.create() as NPGPU.Mat4;
let _matrix2: NPGPU.Mat4 = glMatrix.mat4.create() as NPGPU.Mat4;
let _matrix3: NPGPU.Mat4 = glMatrix.mat4.create() as NPGPU.Mat4;
let _worldPosition: NPGPU.Vec3 = [0, 0, 0];
let _worldRotation: NPGPU.Quat = [0, 0, 0, 0];
const _identityTransform = new Transform();

export interface EffectOptions {
	worldDisplayObject?: DisplayObject
};

export interface EffectStartOptions {
	position?: NPGPU.Vec3,
	rotationRadians?: number,
}

export class Effect extends Container {
	public readonly context: Context;
	public readonly effectModel: EffectModel;
	public readonly GPU33: any;

	public positionZ: number = 0;
	public worldDisplayObject?: DisplayObject;

	private _npgpuEffect: NPGPU.EffectInst | null = null;
	get gpu33Effect() {
		return this._npgpuEffect;
	}

	get ready() {
		return this.gpu33Effect != null;
	}

	private lastUpdatePosition: NPGPU.Vec3 = [0, 0, 0];
	private lastUpdateRotation: number = 0;

	constructor(effectModel: EffectModel, options?: EffectOptions) {
		super();

		if (options) {
			this.worldDisplayObject = options.worldDisplayObject;
		}

		this.context = effectModel.context;
		this.effectModel = effectModel;
	}

	destroy() {
		if (this._npgpuEffect) {
			this._npgpuEffect!.delete();
		}
	}

	start(options?: EffectStartOptions) {
		if (options && options!.position) {
			this.position.set(options!.position![0], options!.position![1]);
			this.positionZ = options!.position![2];
		}

		if (options && options!.rotationRadians) {
			this.rotation = options!.rotationRadians;
		}

		if (this.effectModel.ready) {
			this._onEffectModelReady();
		} else {
			this.effectModel.once('ready', () => {
				this._onEffectModelReady();
			}, this);
		}
	}

	update(seconds: number) {
		if (!this.ready)
			return;

        this._npgpuEffect!.update(seconds, this._getPositionToUpdate(), 
			this._getRotationToUpdate());
	}

	preRender()
	{
		if (!this._npgpuEffect)
			return;

		if (this.worldDisplayObject) {
			this._getWorldTransform(_matrix1, this.worldDisplayObject);
		} else {
			glMatrix.mat4.identity(_matrix1);
		}

		this._npgpuEffect!.preRender(_matrix1);
	}

	_render(renderer: Renderer)
    {
        if (!this.ready)
			return;
		
		renderer.batch.setObjectRenderer(renderer.plugins.neutrinoGPU);
		renderer.plugins.neutrinoGPU.beforeEffectRendered();
		
        this.effectModel.setupTextures(renderer);
        this._npgpuEffect!.render();      
    }

	private _onEffectModelReady() {
		const npgpu = this.context.npgpu!;

		this.context.pushViewportSettings();

		this.context.stateChanger.startGather();

		Effect._updateWorldTransformTree(this);

		this._npgpuEffect = npgpu.EffectInst.create(this.effectModel.npgpuEffectModel!);
		this._npgpuEffect!.initialize(this._getPositionToUpdate(), 
			this._getRotationToUpdate(), npgpu.NANVEC3);
		
		this.context.stateChanger.end();

		this.context.popViewportSettings();

		this.emit('ready', this);
	}

	private _getPositionToUpdate() : NPGPU.Vec3 {
		const npgpu = this.context.npgpu!;
		this._getWorldPosition(_worldPosition);

		if (this.lastUpdatePosition[0] != _worldPosition[0] ||
			this.lastUpdatePosition[1] != _worldPosition[1] ||
			this.lastUpdatePosition[2] != _worldPosition[2]) 
		{
			glMatrix.vec3.copy(this.lastUpdatePosition, _worldPosition);
			return _worldPosition;
		}
		else {
			return npgpu.NANVEC3;
		}
	}

	private _getRotationToUpdate() : NPGPU.Quat {
		const npgpu = this.context.npgpu!;
		const worldRotation = Effect._getWorldRotation(this);
		
		if (this.lastUpdateRotation != worldRotation) 
		{
			this.lastUpdateRotation = worldRotation;
			Effect._rotationAroundZ(_worldRotation, worldRotation);
			return _worldRotation;
		}
		else {
			return npgpu.NANQUAT;
		}
	}

	private _getWorldPosition(outPosition: NPGPU.Vec3) {
		if (this.worldDisplayObject) {
			this._getWorldTransform(_matrix1, this.worldDisplayObject);
			this._getWorldTransform(_matrix2, this);

			glMatrix.mat4.invert(_matrix3, _matrix1); // _matrix3 = inverted worldDisplayObject matrix
			glMatrix.mat4.multiply(_matrix1, _matrix3, _matrix2); // _matrix1 = child matrix
			glMatrix.vec3.set(outPosition, _matrix1[12], _matrix1[13], _matrix1[14]);
		} else {
			glMatrix.vec3.set(outPosition, this.worldTransform.tx, this.worldTransform.ty, this.positionZ);
		}
	}

	private _getWorldTransform(outMatrix: NPGPU.Mat4, displayObject: DisplayObject) : void {
		const matrix = displayObject.worldTransform;
		const a = matrix.a;
		const b = matrix.b;
		const c = matrix.c;
		const d = matrix.d;
		const tx = matrix.tx;
		const ty = matrix.ty;

		glMatrix.mat4.set(outMatrix, 
			a, b, 0, 0, 
			c, d, 0, 0,
			0, 0, 1, 0,
			tx, ty, 0, 1
		);
	}

	private static _getWorldRotation(displayObject: DisplayObject): number {
		if (displayObject.parent) {
			return displayObject.rotation + Effect._getWorldRotation(displayObject.parent);
		}
		return displayObject.rotation;
	}

	private static _updateWorldTransformTree(displayObject: DisplayObject) {
		if (!displayObject.parent) {
			displayObject.transform.updateTransform(_identityTransform);
			return;
		}			

		Effect._updateWorldTransformTree(displayObject.parent);
		displayObject.transform.updateTransform(displayObject.parent.transform);
	}

	private static _rotationAroundZ(out: NPGPU.Quat, radians: number) {
		const halfAngle = radians * 0.5;
		glMatrix.vec4.set(out, Math.cos(halfAngle), 0, 0, Math.sin(halfAngle));
	}
}
