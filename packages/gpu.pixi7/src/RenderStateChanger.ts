import { BaseTexture, BLEND_MODES, Renderer, State } from "pixi.js";
import * as NPGPU from "@neutrinoparticles/gpu.types";
import { Context } from "./Context";

export class RenderStateChanger 
{
    public readonly context: Context;
    public readonly renderer: Renderer;
    private state: State;
    private affectedTextureSlots: Set<number> = new Set<number>();
    private dummyTexture: BaseTexture;
    private started: boolean = false;

    constructor(context: Context) {
        this.context = context;
        this.renderer = context.renderer;
        this.state = new State();
        this.dummyTexture = new BaseTexture();

        this._resetStateToGLDefaults();
    }

    enableDepthTest(value: boolean) {
        this.state.depthTest = value;
    }

    enableBlend(value: boolean) {
        this.state.blend = value;
    }

    enableProgramPointSize(_value: number) {
        // Never called for WebGL
    }

    blendFunc(value: NPGPU.BlendFunc) {
        const npgpu = this.context.npgpu!;

        switch (value) {
        //case GPU33.BlendFunc.ONE__ONE:
        default:
            this.state.blendMode = BLEND_MODES.ADD;
            break;

        case npgpu.BlendFunc.SRC_ALPHA__ONE_MINUS_SRC_ALPHA:
            this.state.blendMode = BLEND_MODES.NORMAL_NPM;
            break;

        case npgpu.BlendFunc.ONE__ONE_MINUS_SRC_ALPHA:
            this.state.blendMode = BLEND_MODES.NORMAL;
            break;

        case npgpu.BlendFunc.SRC_ALPHA__ONE:
            this.state.blendMode = BLEND_MODES.ADD_NPM;
            break;

        case npgpu.BlendFunc.ZERO__SRC_COLOR:
            this.state.blendMode = BLEND_MODES.MULTIPLY; // TODO: multiply cannot be mapped
            break;
        }
    }

    cullFace(value: NPGPU.CullFace) {
        const npgpu = this.context.npgpu!;

        switch (value) {
        //case GPU33.CullFace.FRONT_AND_BACK:
        default:
            this.state.culling = false;
            break;

        case npgpu.CullFace.BACK:
            this.state.culling = true;
            this.state.clockwiseFrontFace = false;
            break;
        }
    }

    depthFunc(value: NPGPU.DepthFunc) {
        const npgpu = this.context.npgpu!;
        const gl = this.renderer.gl;

        switch (value) {
        //case GPU33.DepthFunc.LESS:
        default:
            gl.depthFunc(gl.LESS);
            break;

        case npgpu.DepthFunc.GREATER:
            gl.depthFunc(gl.GREATER);
            break;
        }
    }

    activeTexture(index: number) {
        const gl = this.renderer.gl;
        gl.activeTexture(gl.TEXTURE0 + index);

        const textureSystem = this.renderer.texture;
        textureSystem.currentLocation = index;
    }

    notifyBeforeTextureBound(_type: NPGPU.TextureType, _id: number) {
        const textureSystem = this.renderer.texture;
        this.affectedTextureSlots.add(textureSystem.currentLocation);
    }

    notifyBeforeRender() {
        this.applyGatheredState();
    }

    public startGather() {
        if (this.started) {
            return;
        }

        this._resetStateToGLDefaults();
        this.affectedTextureSlots.clear();

        this.renderer.shader.reset();
        this.renderer.geometry.reset();

        this.started = true;
    }

    public applyGatheredState() {
        this.renderer.state.set(this.state);
    }

    public end() {
        if (!this.started) {
            return;
        }

        this.started = false;

        const gl = this.renderer.gl;
        const textureSystem = this.renderer.texture;

        this.affectedTextureSlots.forEach((index) => {
            gl.activeTexture(gl.TEXTURE0 + index);
            gl.bindTexture(gl.TEXTURE_2D, null);

            textureSystem.boundTextures[index] = this.dummyTexture;
            textureSystem.currentLocation = index;
        });

        this.affectedTextureSlots.clear();
    }

    private _resetStateToGLDefaults() {
        this.state.data = 0;
        this.state.depthMask = true;
    }
};