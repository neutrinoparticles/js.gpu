import { 
    DisplayObject,
    ExtensionType,
    ObjectRenderer,
    Renderer,
} from 'pixi.js';
import { Effect } from './Effect';
import { Context } from './Context';
import { RenderStateChanger } from './RenderStateChanger';

/**
 * Renderer plugin which knows how to batch and render particles geometry.
 * 
 * Basically, it is a modified PIXI.AbstractRendererPlugin. As a difference,
 * it has only one geometry buffer size (with maximum capacity). And all elements are
 * rendered right away without storing them to make postponed flush.
 */
export class RendererPlugin extends ObjectRenderer 
{
    public stateChanger?: RenderStateChanger;

    private started = false;

    constructor(renderer: Renderer) {
        super(renderer);

        this.renderer.on('prerender', this._onPreRender, this);
        renderer.runners.contextChange.add(this);
    }

    static get extension() { return {
        name: 'neutrinoGPU',
        type: ExtensionType.RendererPlugin,
    }}

    // At least once in the start of the application.
    contextChange() {
    }

    flush(): void {
    }

    destroy() {
        super.destroy();
    }

    start(): void {
    }

    beforeEffectRendered(): void {
        if (!Context.isInitialized() || !Context.instance().loaded || this.started) {
            return;
        }

        this.stateChanger!.startGather();
        Context.instance().gpu33Context.beginRender();
        this.started = true;
    }

    stop(): void {
        if (!Context.isInitialized() || !Context.instance().loaded || !this.started) {
            return;
        }

        Context.instance().gpu33Context.endRender();
        this.stateChanger!.end();
        this.started = false;
    }
 
    private _onPreRender(): void {
        if (!Context.isInitialized() || !Context.instance().loaded) {
            return;
        }

        if (this.started) {
            this.stop();
        }

        const context = Context.instance();

        const renderRoot = context.renderRoot;
        const camera = renderRoot.camera;
        const root = renderRoot.root;

        this.stateChanger!.startGather();

        context.pushViewportSettings();

        context.gpu33Context.beginPreRender(camera.cameraPos, camera.cameraDir,
            camera.cameraUp, camera.cameraRight, camera.viewProjMatrix,
            this.renderer.width, this.renderer.height);

        this._prerenderEffects(root);

        context.gpu33Context.endPreRender();

        context.popViewportSettings();

        this.stateChanger!.end();
    }

    private _prerenderEffects(root: DisplayObject)
    {
        if (root instanceof Effect) {
            (root as Effect).preRender();
        }
    
        if (root.children && root.children.length > 0) {
            root.children.forEach((child) => {
                this._prerenderEffects(child as DisplayObject);
            });
        }
    }
    }
