import { ApplicationPlugin } from './AplicationPlugin';
import { extensions } from 'pixi.js';
import { effectModelLoader } from './EffectModelLoader';
import { RendererPlugin } from './RendererPlugin';

export * from './AplicationPlugin';
export * from './Camera';
export * from './Camera2DPerspective';
export * from './Context';
export * from './Effect';
export * from './EffectModel';
export * from './EffectsUpdateGroup';
export * from './RendererPlugin';
export * from './RenderStateChanger';
export * from './TexturesLoader';
export * from './Bloom';

export function registerPlugins()
{
    extensions.add(ApplicationPlugin);
    extensions.add(effectModelLoader);
    extensions.add(RendererPlugin);
}

export function unregisterPlugins()
{
    extensions.remove(RendererPlugin);
    extensions.remove(effectModelLoader);
    extensions.remove(ApplicationPlugin);
}

registerPlugins();