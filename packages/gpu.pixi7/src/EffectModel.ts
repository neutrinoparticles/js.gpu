import { utils, BaseTexture, Texture, Renderer } from "pixi.js";
import { Context } from "./Context";
import { AbstractTexturesLoader } from "./TexturesLoader";
import * as NPGPU from "@neutrinoparticles/gpu.types";

export class EffectModel extends utils.EventEmitter
{
    public readonly gl: WebGL2RenderingContext;
    public readonly context: Context;
    public readonly textures: Array<BaseTexture>;
	private readonly reindexing: Array<number>;
	private readonly remapping: Array<Array<number>>;

    public npgpuEffectSource: NPGPU.EffectSource;
    public npgpuEffectModel: NPGPU.EffectModel | null = null;

    private _numTexturesToLoadLeft: number = 0;

    constructor(context: Context, effectSource: string, texturesLoader: AbstractTexturesLoader) {
        super();

        this.gl = context.gl;
        this.context = context;

		const npgpu = this.context.npgpu!;

        this.npgpuEffectSource = npgpu.EffectSource.create(effectSource)!;
        
        this._numTexturesToLoadLeft = this.npgpuEffectSource.textures.size();
        this.textures = new Array<BaseTexture>();
		this.reindexing = new Array<number>(this._numTexturesToLoadLeft);
		this.remapping = new Array<Array<number>>(this._numTexturesToLoadLeft);
        this._loadTextures(this._numTexturesToLoadLeft, texturesLoader);
    }

    destroy(): void {
        this.npgpuEffectModel?.delete();
        this.npgpuEffectSource?.delete();
    }

    get ready(): boolean {
		return this._numTexturesToLoadLeft === 0;
	}

    public setupTextures(renderer: Renderer): void {
        const textureSystem = renderer.texture;

        this.textures.forEach((baseTexture, index) => {
            textureSystem.bind(baseTexture, index);
        });
    }

    private _loadTextures(numTextures: number, texturesLoader: AbstractTexturesLoader): void
    {
		for (let texIndex = 0; texIndex < numTextures; ++texIndex) 
		{
			let texturePath = this.npgpuEffectSource.textures.get(texIndex);
			let texture = null;
			
			if (this.context.trimmedExtensionsLookupFirst) 
			{
				let trimmedTexturePath = texturePath.replace(/\.[^/.]+$/, ""); // https://stackoverflow.com/a/4250408
				texture = utils.TextureCache[trimmedTexturePath];
			}

			if (!texture)
				texture = utils.TextureCache[texturePath];

			if (texture)
			{
				if (texture.baseTexture.valid) 
				{
					this._onTextureLoaded(texIndex, texture);
				} else 
				{
					texture.once('update', function (self, imageIndex, texture) 
					{
						return function () 
						{
							self._onTextureLoaded(imageIndex, texture);
						}
					} (this, texIndex, texture));
				}
			}
			else
			{
				texturesLoader.load(this.context.texturesBasePath + texturePath).then(
					((self, imageIndex: number) => {
						return function(texture) 
						{
							self._onTextureLoaded(imageIndex, texture);
						}
					}) (this, texIndex));
			}			
		}

    }

    private _onTextureLoaded(index: number, texture: Texture) : void
	{
		const baseTexture = texture.baseTexture;
		const frame = texture.frame;
		const rotate = texture.rotate;

		let w = frame.width / baseTexture.width;
		let h = frame.height / baseTexture.height;
		let px = frame.x / baseTexture.width;
		let py = frame.y / baseTexture.height;

		switch (rotate)
		{
		default: this.remapping[index] = [w, 0, 0, -h, px, py + h]; break;
		case 2: this.remapping[index] = [0, h, w, 0, px, py]; break;
		case 4: this.remapping[index] = [-w, 0, 0, h, px + w, py]; break;
		case 6: this.remapping[index] = [0, -h, -w, 0, px + w, py + h]; break;
		// Flipped vertically
		case 8: this.remapping[index] = [w, 0, 0, h, px, py]; break;
		case 10: this.remapping[index] = [0, -h, w, 0, px, py + h]; break;
		case 12: this.remapping[index] = [-w, 0, 0, -h, px + w, py + h]; break;
		case 14: this.remapping[index] = [0, h, -w, 0, px + w, py]; break;
		}
		
		let texIndex = this.textures.indexOf(baseTexture);

		if (texIndex == -1)
		{
			texIndex = this.textures.length;
			this.textures.push(baseTexture);
		}

		this.reindexing[index] = texIndex;

		if (--this._numTexturesToLoadLeft === 0) {
			this._onAllTexturesLoaded();
		}
	}

	private _onAllTexturesLoaded() : void
	{
		const npgpu = this.context.npgpu!;

		this.context.stateChanger.startGather();

		this.npgpuEffectModel = npgpu.EffectModel.create(this.context.gpu33Context
            , this.npgpuEffectSource, this.remapping, this.textures.length, this.reindexing);

		this.context.stateChanger.end();
			
        if (!this.npgpuEffectModel) {
            throw new Error(npgpu.errorMessage());
		}

		this.emit('ready', this);
	}
}