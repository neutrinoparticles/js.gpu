import { Container, Renderer, RenderTexture } from "pixi.js";
import * as NPGPU from "@neutrinoparticles/gpu.types";
import { Context } from "./Context";

export class Bloom extends Container {
    public readonly context: Context;
    public readonly renderTargetToBloom: RenderTexture;
    
    private bloom: NPGPU.BloomFloatExternal | null;

    constructor(context: Context, renderTargetToBloom: RenderTexture, maxColor: number) {
        super();

        this.context = context;
        this.renderTargetToBloom = renderTargetToBloom;
        this.bloom = null;

        this.renderTargetToBloom.baseTexture.on('update', () => {
            if (!this.bloom) {
                const npgpu = this.context.npgpu!;
                const textureId = npgpu.GL.getNewId(npgpu.GL.textures);
                npgpu.GL.textures[textureId] = this.renderTargetToBloom.baseTexture
                    ._glTextures[this.context.renderer.CONTEXT_UID].texture;
                this.bloom = npgpu.BloomFloatExternal.create(textureId, true, maxColor);   
            }

            this.bloom!.resize(this.renderTargetToBloom.baseTexture.width, this.renderTargetToBloom.baseTexture.height);
        });
    }

    render(_renderer: Renderer) {
        if (!this.bloom) return;

        const npgpu = this.context.npgpu!;
        const stateChanger = this.context.stateChanger;

        this.context.pushViewportSettings();
        stateChanger.startGather();

        // Notify about texture bindings in processBloom(). Only the first slot.
        stateChanger.activeTexture(0);
        stateChanger.notifyBeforeTextureBound(npgpu.TextureType.TEXTURE_2D, 0);

        // Disable blend (like default OpenGL state has)
        stateChanger.enableBlend(false);
        stateChanger.applyGatheredState();

        this.bloom!.processBloom();

        this.context.popViewportSettings();

        // Notify about texture bindings in renderResult(). Five slots: basic texture + 4 blurred downsamples
        for (let i = 0; i < 5; ++i) {
            stateChanger.activeTexture(i);
            stateChanger.notifyBeforeTextureBound(npgpu.TextureType.TEXTURE_2D, 0);
        }

        this.bloom!.renderResult();

        stateChanger.end();
    }
}