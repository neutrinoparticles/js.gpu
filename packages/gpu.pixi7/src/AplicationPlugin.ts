import { Application, ExtensionType, Renderer } from "pixi.js";
import { Context, ContextOptions } from "./Context";

declare module 'pixi.js' {
    interface IApplicationOptions {
        neutrinoGPU?: ContextOptions;
    }

    interface Application {
        neutrinoGPU?: Context;
    }
}

/**
 * Create {@link Context} object
 * and assign it to PIXI.Application.neutrino property of the application.
 * 
 * @param {Object} options Options
 * @param {Object} options.neutrino Options object to pass to {@link Context} constructor.
 */
export class ApplicationPlugin
{
    static get extension():any { return {
        name: 'neutrino',
        type: ExtensionType.Application,
    }}

    static init(this: Application, options: any): void
    {
        const context = Context.initialize(this.renderer as Renderer, options.neutrinoGPU);
        context.pushRenderRoot(this.stage, context.camera);

        this.neutrinoGPU = context;
    }

    static destroy(this: Application): void
    {
        Context.shutdown();
        this.neutrinoGPU = undefined;
    }
}