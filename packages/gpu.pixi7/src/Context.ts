import * as NPGPU from "@neutrinoparticles/gpu.types";
import { DisplayObject, Renderer } from "pixi.js";
import { RenderStateChanger } from "./RenderStateChanger";
import { Camera2DPerspective } from "./Camera2DPerspective";
import { Camera } from "./Camera";

export interface ContextOptions {
    npgpuLoader: (options?: unknown) => Promise<NPGPU.MainModule>,
    texturesBasePath?: string;
    trimmedExtensionsLookupFirst?: boolean;
    camera?: Camera;
}

export interface ContextLoadData {
    __neutrinoContext__: Context;
}

interface RenderRoot {
    root: DisplayObject;
    camera: Camera;
}

export class Context
{
    public npgpu?: NPGPU.MainModule;
    public readonly gl: WebGL2RenderingContext;
    public readonly renderer: Renderer;
    public readonly stateChanger: RenderStateChanger;
    public loaded: boolean = false;
    public readonly loadedPromise: Promise<void>;

    public readonly texturesBasePath: string;
    public readonly trimmedExtensionsLookupFirst: boolean;
    public readonly loadData: ContextLoadData;

    private _camera: Camera;
    get camera() { return this._camera; }
    set camera(value: Camera) {
        this._camera = value;
        this._resizeCameraToRenderer();
    }

    private static _instance?: Context;

    private _viewportSettings: Array<NPGPU.ViewportSettings> = [];
    private _viewportIndex = 0;

    private gpu33Context_: NPGPU.Context | null = null;
    get gpu33Context(): NPGPU.Context {
        return this.gpu33Context_!;
    }

    private _renderRoot = new Array<RenderRoot>();
    get renderRoot() { return this._renderRoot[this._renderRoot.length  - 1]; }

    private constructor(renderer: Renderer, options: ContextOptions) {
        this.gl = renderer.gl;
        this.renderer = renderer;
        this.stateChanger = new RenderStateChanger(this);
        this.renderer.plugins.neutrinoGPU.stateChanger = this.stateChanger;
        this.renderer.on('resize', this._resizeCameraToRenderer, this);

        this.loadData = { __neutrinoContext__: this };

        this.texturesBasePath = (options && options.texturesBasePath) || "";
        this.trimmedExtensionsLookupFirst = (options && options.trimmedExtensionsLookupFirst) || true;
        
        this._camera = (options && options.camera) || new Camera2DPerspective(45);
        this._resizeCameraToRenderer();

        this.loadedPromise = new Promise<void>(async (resolve) => {

            const npgpu = await options.npgpuLoader();
            this.npgpu = npgpu;

            const emGLCtx = npgpu.GL.registerContext(this.gl, {});      
            this.npgpu.GL.makeContextCurrent(emGLCtx);

            const gpu33StateChanger = npgpu.RenderStateChanger.implement(this.stateChanger);

            this.pushViewportSettings();
            this.stateChanger.startGather();

            this.gpu33Context_ = npgpu.Context.createWithStateChanger(gpu33StateChanger, true);

            this.stateChanger.end();
            this.popViewportSettings();

            if (!this.gpu33Context_) {
                throw new Error(npgpu.errorMessage());
            }

            this.loaded = true;
            resolve();
        });
    }

    public static initialize(renderer: Renderer, options: ContextOptions) : Context {
        if (Context._instance)
            throw new Error("Context is already initialized");

        Context._instance = new Context(renderer, options);
        return Context._instance!;
    }

    public static isInitialized() : boolean {
        return !!Context._instance;
    }

    public static instance() : Context {
        return Context._instance!;
    }

    public static shutdown() : void {
        Context._instance!._shutdown();
        Context._instance = undefined;
    }

    public pushRenderRoot(root: DisplayObject, camera: Camera) : void {
        this._renderRoot.push({ root, camera });
    }

    public popRenderRoot() : void {
        if (this._renderRoot.length < 1)
            throw new Error("Context.popRenderRoot(): inconsistency of amount pushes and pops.");

        this._renderRoot.pop();
    }

    public pushViewportSettings() {
        const npgpu = this.npgpu!;

        if (this._viewportSettings.length <= this._viewportIndex) {
            this._viewportSettings.push(npgpu.ViewportSettings.create()!);
        }

        npgpu.saveViewportSettings(this._viewportSettings[this._viewportIndex]);
        this._viewportIndex++;
    }

    public popViewportSettings() {
        if (this._viewportIndex < 1)
            return;

        const npgpu = this.npgpu!;

        this._viewportIndex--;
        npgpu.restoreViewportSettings(this._viewportSettings[this._viewportIndex]);
    }

    private _shutdown() {
        this.gpu33Context_?.delete();
    }

    private _resizeCameraToRenderer() {
        this._camera.resize(this.renderer.width, this.renderer.height);
    }
}