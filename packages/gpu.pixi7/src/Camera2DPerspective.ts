import * as NPGPU from "@neutrinoparticles/gpu.types";
import { Camera } from './Camera';
import * as glMatrix from 'gl-matrix';

enum LookAt
{
    POSITIVE_Z,
    NEGATIVE_Z
}

const projMatrix: NPGPU.Mat4 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
const viewMatrix: NPGPU.Mat4 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
const viewTarget: NPGPU.Vec3 = [0, 0, 0];

function flippedYPerspective(out: NPGPU.Mat4, fovy: number, aspect: number, near: number, far: number) {
    const top = near * Math.tan(fovy / 2);
    const bottom = -top;
    const left = bottom * aspect;
    const right = top * aspect;

    glMatrix.mat4.frustum(out, left, right, top, bottom, near, far);

    return out;
}

export interface Camera2DPerspectiveOptions
{
    lookAt?: LookAt,
    nearPlanePercentage?: number,
    farPlanePercentage?: number,
    flippedY?: boolean,
}

export class Camera2DPerspective extends Camera
{
    private lookAt: LookAt;
    private width: number;
    private height: number;
    private angleDegree: number;
    private nearPlanePercentage: number;
    private farPlanePercentage: number;
    private flippedY: boolean;

    constructor(angleDegree: number, options?: Camera2DPerspectiveOptions) {
        super();

        this.lookAt = (options && options.lookAt) || LookAt.POSITIVE_Z;
        this.width = 1;
        this.height = 1;
        this.angleDegree = angleDegree;
        this.nearPlanePercentage = (options && options.nearPlanePercentage) || 0.01;
        this.farPlanePercentage = (options && options.farPlanePercentage) || 500;
        this.flippedY = (options && options.flippedY) || false;
        this._updateMatrices();
    }

    public resize(width: number, height: number) {
        if (this.width === width && this.height == height)
            return;

        this.width = width;
        this.height = height;
        this._updateMatrices();
    }

    private _updateMatrices() {
        const angleRad = Math.PI / 180.0 * this.angleDegree;
        const cameraZ = this.height / (2.0 * Math.tan(angleRad * 0.5));
        glMatrix.vec3.set(this._cameraPos,
            this.width * 0.5,
            this.height * 0.5,
            this.lookAt == LookAt.POSITIVE_Z ? -cameraZ : cameraZ
        );

        glMatrix.vec3.set(this._cameraDir, 0, 0, this.lookAt == LookAt.POSITIVE_Z ? 1 : -1);
        glMatrix.vec3.set(this._cameraUp, 0, -1, 0);
        glMatrix.vec3.set(this._cameraRight, 1, 0, 0);

        const cameraAspect = this.width / this.height;
        if (this.flippedY) {
            flippedYPerspective(projMatrix, angleRad, cameraAspect, cameraZ * this.nearPlanePercentage, 
                cameraZ * this.farPlanePercentage);
        } else {
            glMatrix.mat4.perspective(projMatrix, angleRad, cameraAspect, cameraZ * this.nearPlanePercentage, 
                cameraZ * this.farPlanePercentage);
        }
        
        glMatrix.mat4.lookAt(viewMatrix, this._cameraPos, 
            glMatrix.vec3.add(viewTarget, this._cameraPos, this._cameraDir), this._cameraUp);
        glMatrix.mat4.multiply(this._viewProjMatrix, projMatrix, viewMatrix);
    }
}