@echo off
npm test || (
    echo TESTS FAILED!
    exit /b 1
)

echo TESTS PASSED!
exit /b 0
