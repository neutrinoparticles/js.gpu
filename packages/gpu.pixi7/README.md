# neutrinoparticles.gpu.pixi7

This package is an integration of [NeutrinoParticles](https://neutrinoparticles.com/) effects into the graphics engine **`PIXI.js v7`**.


![Bunny in portal](./img/bunny_in_portal.gif)

[**NeutrinoParticles**](https://neutrinoparticles.com/) is a universal solution for creating and rendering particle effects for various platforms and engines. Effects are exported as source code in different programming languages. Then, this code is compiled, optimized, and bundled together with the application, ensuring maximum performance.

[**neutrinoparticles.gpu.(js|wasm)**](../gpu.js/README.md) is a renderer for effects exported in the special version of the `NeutrinoParticles Editor GPU`. In this editor, effects are exported as GLSL source code, which is used by the `neutrinoparticles.gpu.(js|wasm)` renderer to update and render the effects using the GPU resources, thus freeing up the CPU for other tasks.

This `neutrinoparticles.gpu.pixi7` package integrates [neutrinoparticles.gpu.(js|wasm)](../gpu.js/README.md) into PIXI.js engine version 7.

## What will I get?
Effects with full GPU acceleration in the PIXI.js graphics engine scene. Hundreds of thousands of particles in desktop browsers and from tens to hundreds of thousands in mobile browsers.

The ability to create fantastically complex and beautiful effects in the NeutrinoParticles editor and use them in PIXI.js games and applications.

## What is full GPU acceleration for effects?
Effects are not only rendered but also updated using the GPU (Graphics Processing Unit). This frees up the main processor (CPU - Central Processing Unit) for other game tasks and allows handling 100 times more particles in effects.

## What hardware is needed?
Any hardware that supports WebGL 2 in the browser.

> For the full-screen bloom effect [Bloom](../gpu.pixi7.samples/README.md#bloom), the browser needs to support [FLOAT textures](https://web3dsurvey.com/webgl2/extensions/EXT_color_buffer_float) (99.0% of devices) and [FLOAT Blend](https://web3dsurvey.com/webgl2/extensions/EXT_float_blend) (89.58% of devices).

## How to install?
You need two packages:
1. `@neutrinoparticles/gpu.pixi7` for PIXI integration.
2. `@neutrinoparticles/gpu.js` or `@neutrinoparticles/gpu.wasm` to choose the rendering library. The first one is pure JS, and the second is WebAssembly.

So, in the terminal, it will look something like this:
```bash
npm install @neutrinoparticles/gpu.pixi7 @neutrinoparticles/gpu.wasm
```

## Quick Example
```typescript
import { Effect, EffectsUpdateGroup } from '@neutrinoparticles/gpu.pixi7';

// Pick one of two following imports to choose between JS and WASM
//import { default as npgpuLoader } from '@neutrinoparticles/gpu.js';
import { default as npgpuLoader } from '@neutrinoparticles/gpu.wasm';

import * as PIXI from 'pixi.js';

// Creating PIXI Application
const app = new PIXI.Application({ background: '#1099bb', resizeTo: window,
    neutrinoGPU: { // Additional options for NeutrinoParticles Context
        npgpuLoader, // Mandatory. NeutrinoParticles GPU library loader
        texturesBasePath: 'textures/', // Base path for textures. Will be prefixed to exported texture paths in effects
    }
 });

// Wait for GPU library loaded
await app.neutrinoGPU!.loadedPromise;

document.body.appendChild(app.view as HTMLCanvasElement);

PIXI.Assets.addBundle('resources', [
    // Request loading effect in the bundle
    { 
        name: 'testEffect', // You choose a name of EffectModel in the bundle
        srcs: 'src/shaders/Test.shader', // Path to the effect
        data: app.neutrinoGPU!.loadData // Mark this resource as NeutrinoParticles effect
    },
]);

// Update group is for updating effects in a batch. It works faster.
const updateGroup = new EffectsUpdateGroup(app.neutrinoGPU!);

PIXI.Assets.loadBundle('resources').then((resources) => {

    // Bunny sprite on background
    {
        const bunny = PIXI.Sprite.from('https://pixijs.com/assets/bunny.png');
        bunny.anchor.set(0.5);
        bunny.x = app.screen.width / 2;
        bunny.y = app.screen.height / 2;
        bunny.scale = new PIXI.Point(20, 20);
        app.stage.addChild(bunny);
    }

    // Create effect
    {
        // Create instance of the effect
        const effect = new Effect(resources.testEffect);

        // Add the effect to the scene graph
        app.stage.addChild(effect);

        // Start the effect. Should be called after it is added to the scene graph
        // to calculate world position correctly.
        effect.start({
            position: [app.screen.width / 2, app.screen.height / 2, 0]
        })

        // Add it to the update group to update all effects at once
        updateGroup.add(effect);
    }

    // Listen for update
    app.ticker.add((_delta) =>
    {
        // Update effects. Better in a group.
        updateGroup.update(app.ticker.elapsedMS / 1000.0);
    });
});
```

## More Examples
In [**neutrinoparticles.gpu.pixi7.samples**](../gpu.pixi7.samples/README.md) you will find a rich selection of usage examples for all possible cases.
