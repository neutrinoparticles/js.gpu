/** @type {import('ts-jest').JestConfigWithTsJest} **/
module.exports = {
  testEnvironment: "node",
  transform: {
    "^.+.tsx?$": ["ts-jest",{ tsconfig: "tsconfig.test.json" }],
  },
  testTimeout: 300000,
  globalSetup: './tests/jest.global-setup.ts',
  globalTeardown: './tests/jest.global-teardown.ts',
};