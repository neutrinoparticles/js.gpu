import { defineConfig } from 'vite';

export default defineConfig({
  build: {
    lib: {
      entry: './neutrinoparticles.renderer.gpu.wasm.js',
      name: 'NPGPU',
      formats: ['umd'],
      fileName: 'neutrinoparticles.renderer.gpu.wasm',
    },
    rollupOptions: {
      output: {
        globals: {
        },
      },
    },
  },
});
